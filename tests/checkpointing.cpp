#include <catch.hpp>
#include <petscvec.h>
#include <oda/oda.h>
#include <dendrite/util.h>
#include <dendrite/DendroIO.h>
#include <dendrite/checkpoint.h>


TEST_CASE("write and reload octree", "[checkpointing]") {
  double problemSize[3] = {1.0, 1.0, 1.0};
  const unsigned int ndof = 1;
  const unsigned int refine_var_idx = 0;
  const unsigned int refine_lvl_base = 3;
  const unsigned int refine_lvl_interface = 6;
  const double refine_interface_tol = 0.15;
  const unsigned int max_depth = refine_lvl_interface + 1;
  const auto refine_func =  [&](double x, double y, double z) -> double {
    return 1.0 - sin(M_PI * x / problemSize[0]) * sin(M_PI * y / problemSize[1]) * sin(M_PI * z / problemSize[2]) * 2;
  };

	PetscRandom random;
	PetscRandomCreate(PETSC_COMM_WORLD, &random);

	system("rm -f treenodes.dat test_vals.dat");

  auto nodes_write = ot::function_to_TreeNodes(refine_func, refine_lvl_base, refine_lvl_interface, problemSize,
      PETSC_COMM_WORLD);
  write_octree("treenodes.dat", nodes_write, PETSC_COMM_WORLD);

  std::vector<ot::TreeNode> nodes_da = nodes_write;
  ot::DA* da = new ot::DA(nodes_da, PETSC_COMM_WORLD, PETSC_COMM_WORLD, 0.0);

  Vec vec1_write, vec2_write;
  da->createVector(vec1_write, false, false, 2);
	VecSetRandom(vec1_write, random);
  da->createVector(vec2_write, false, false, 3);
	VecSetRandom(vec2_write, random);

  Checkpointer::write_values("test_vals.dat", {vec1_write, vec2_write}, PETSC_COMM_WORLD);

  std::vector<ot::TreeNode> nodes_read = read_octree("treenodes.dat", PETSC_COMM_WORLD);
  //ot::DA* read_da = new ot::DA(nodes_read, PETSC_COMM_WORLD, PETSC_COMM_WORLD, 0.0);

  CHECK(nodes_write == nodes_read);

  Vec vec1_read, vec2_read;
  Checkpointer::read_values("test_vals.dat", {&vec1_read, &vec2_read}, PETSC_COMM_WORLD);
  PetscBool vecs_match = PETSC_FALSE;

  VecEqual (vec1_write, vec1_read, &vecs_match);
  CHECK (vecs_match == PETSC_TRUE);

	VecEqual (vec2_write, vec2_read, &vecs_match);
	CHECK (vecs_match == PETSC_TRUE);

  delete da;
  //delete read_da;
	PetscRandomDestroy(&random);
}
