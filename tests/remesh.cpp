#include <catch.hpp>
#include <petscvec.h>
#include <oda/oda.h>
#include <dendrite/util.h>
#include <dendrite/DendroIO.h>

std::vector<unsigned int> calc_refine(ot::DA* da, Vec vec, unsigned int ndof, unsigned int refine_var_idx,
                                      unsigned int refine_lvl_base,
                                      unsigned int refine_lvl_interface,
                                      double refine_interface_tol,
                                      const double* problemSize) {
  std::vector<unsigned int> refinement;
  da->createVector(refinement, true, true, 1);

  // make the data in vec accessible (via buff)
  PetscScalar* buff;
  da->vecGetBuffer(vec, buff, false, false, true, ndof);

  // read ghost node data into buff (requires communication)
  da->ReadFromGhostsBegin(buff, ndof);
  da->ReadFromGhostsEnd(buff);

  /*double octToPhysScale[3];
  const unsigned int maxD = da->getMaxDepth();
  octToPhysScale[0] = problemSize[0] / ((PetscScalar) (1 << (maxD - 1)));
  octToPhysScale[1] = problemSize[1] / ((PetscScalar) (1 << (maxD - 1)));
  octToPhysScale[2] = problemSize[2] / ((PetscScalar) (1 << (maxD - 1)));*/

  // loop over owned elements
  for (da->init<ot::DA_FLAGS::WRITABLE>();
       da->curr() < da->end<ot::DA_FLAGS::WRITABLE>();
       da->next<ot::DA_FLAGS::WRITABLE>()) {
    const unsigned int curr = da->curr();
    unsigned int node_idxes[8];
    da->getNodeIndices(node_idxes);

    /*const int lev = da->getLevel(da->curr());
    Point h(octToPhysScale[0] * (1u << (maxD - lev)),
            octToPhysScale[1] * (1u << (maxD - lev)),
            octToPhysScale[2] * (1u << (maxD - lev)));

    Point pt = da->getCurrentOffset();
    pt.x() *= octToPhysScale[0];
    pt.y() *= octToPhysScale[1];
    pt.z() *= octToPhysScale[2];*/

    // get the min phi value
    double min_val = 1e16;
    for (unsigned int n = 0; n < 8; n++) {
      double val = fabs(buff[node_idxes[n] * ndof + refine_var_idx]);
      min_val = std::min(val, min_val);
    }

    refinement[curr] = (min_val < refine_interface_tol) ? refine_lvl_interface : refine_lvl_base;
  }

  da->vecRestoreBuffer(vec, buff, false, false, true, ndof);

  return refinement;
}

TEST_CASE("remesh and interpolation", "[remesh]") {
  double problemSize[3] = {1.0, 1.0, 1.0};
  const unsigned int ndof = 1;
  const unsigned int refine_var_idx = 0;
  const unsigned int refine_lvl_base = 3;
  const unsigned int refine_lvl_interface = 6;
  const double refine_interface_tol = 0.15;
  const unsigned int max_depth = refine_lvl_interface + 1;
  const auto refine_func =  [&](double x, double y, double z, int dof) -> double {
    return 1.0 - sin(M_PI * x / problemSize[0]) * sin(M_PI * y / problemSize[1]) * sin(M_PI * z / problemSize[2]) * 2;
  };

  // ot::DA* base_da = ot::function_to_DA(refine_func, refine_lvl_base, refine_lvl_interface, problemSize, PETSC_COMM_WORLD);

  std::vector<ot::TreeNode> nodes;
  ot::createRegularOctree(nodes, refine_lvl_base, 3, max_depth, MPI_COMM_WORLD);
  ot::DA* base_da = new ot::DA(nodes, PETSC_COMM_WORLD, PETSC_COMM_WORLD, 0.3);
  nodes.clear();

  Vec vec;
  base_da->createVector(vec, false, false, ndof);
  setScalarByFunction(base_da, problemSize, vec, ndof, refine_func);

  std::vector<unsigned int> levels = calc_refine(base_da, vec, ndof, refine_var_idx,
      refine_lvl_base, refine_lvl_interface, refine_interface_tol, problemSize);
  ot::DA* refined_da = ot::remesh_DA(base_da, levels, problemSize, PETSC_COMM_WORLD);

  Vec interpolated_vec;
  refined_da->createVector(interpolated_vec, false, false, ndof);

  std::vector<double> pts;
  ot::getNodeCoordinates(refined_da, pts, problemSize);
  ot::interpolateData(base_da, vec, interpolated_vec, NULL, ndof, pts, problemSize);


  /*std::vector<unsigned int> levels2 = calc_refine(refined_da, interpolated_vec, ndof, refine_var_idx,
      refine_lvl_base, refine_lvl_interface, 0.1, problemSize);
  ot::DA* refined2_da = ot::remesh_DA(refined_da, levels2, problemSize, PETSC_COMM_WORLD);

  Vec interpolated_vec2;
  refined2_da->createVector(interpolated_vec2, false, false, ndof);

  std::vector<double> pts2;
  ot::getNodeCoordinates(refined2_da, pts2, problemSize);
  ot::interpolateData(refined_da, interpolated_vec, interpolated_vec2, NULL, ndof, pts2, problemSize);*/



  /*Vec levels_petsc;
  std_to_petsc_vec(base_da, levels, 1, true, true, false, &levels_petsc);
  octree2VTK(base_da, problemSize, levels_petsc, 1, true, "levels");*/

  octree2VTK(base_da, problemSize, vec, ndof, false, "base");
  octree2VTK(refined_da, problemSize, interpolated_vec, ndof, false, "refined");
  //octree2VTK(refined2_da, problemSize, interpolated_vec2, ndof, false, "refined2");

  VecDestroy(&vec);
  VecDestroy(&interpolated_vec);
  delete base_da;
  delete refined_da;
}