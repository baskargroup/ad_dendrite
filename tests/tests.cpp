#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include <petscsys.h>
#include <oda/oda.h>
#include <dendrite/dendrite.h>

int main(int argc, char* argv[]) {
  dendrite_init(argc, argv);

  const int result = Catch::Session().run(argc, argv);
  dendrite_finalize();
  return result;
}

