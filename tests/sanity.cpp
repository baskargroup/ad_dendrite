#include "catch.hpp"

#include <oda.h>
#include <odaUtils.h>
#include <set>
#include <talyfem/utils/macros.h>

TEST_CASE("WRITABLE contains no shared elements", "[daloops]") {
  ot::DA* da;
  double problemSize[3] = {1.0, 1.0, 1.0};

  // create DA
  {
    const double ctr[3] = {0.5, 0.5, 0.5};
    const double r = 0.1;
    auto fx_refine = [ctr, r](double x, double y, double z) -> double {
      return sqrt((x - ctr[0]) * (x - ctr[0]) + (y - ctr[1]) * (y - ctr[1]) + (z - ctr[2]) * (z - ctr[2])) - r - 1e-6;
    };
    da = ot::function_to_DA(fx_refine, 3, 5, problemSize, MPI_COMM_WORLD);
  }

  if (!da->computedLocalToGlobal())
    da->computeLocalToGlobalMappings();
  if (!da->computedLocalToGlobalElems())
    da->computeLocalToGlobalElemMappings();

  int mpiSize, mpiRank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &mpiRank);
  MPI_Comm_size(PETSC_COMM_WORLD, &mpiSize);

  // verify we loop over each element exactly once in parallel
  {
    const int* lclToGlbElem = da->getLocalToGlobalElemsMap();
    std::vector<PetscInt> visited;

    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
      unsigned int lclElemID = da->curr();
      int glbElemID = lclToGlbElem[lclElemID];
      visited.push_back(glbElemID);
    }
    std::sort(visited.begin(), visited.end());
    std::unique(visited.begin(), visited.end());

    PetscInt myCount = visited.size();
    std::vector<PetscInt> counts(mpiSize);
    MPI_Gather(&myCount, 1, MPI_TALYFEM_INT, counts.data(), 1, MPI_TALYFEM_INT, 0, PETSC_COMM_WORLD);

    if (!mpiRank) {
      std::vector<int> displs(mpiSize);
      for (unsigned int i = 1; i < displs.size(); i++) {
        displs[i] = displs[i - 1] + counts[i - 1];
      }

      std::vector<PetscInt> allVisited(displs[displs.size() - 1] + counts[displs.size() - 1]);

      MPI_Gatherv(visited.data(), myCount, MPI_TALYFEM_INT, allVisited.data(), counts.data(),
                  displs.data(), MPI_TALYFEM_INT, 0, PETSC_COMM_WORLD);

      std::set<PetscInt> seen;
      std::vector<PetscInt> duplicates;
      for (unsigned int i = 0; i < allVisited.size(); i++) {
        if (!seen.insert(allVisited[i]).second) {
          duplicates.push_back(allVisited[i]);
        }
      }

      REQUIRE(duplicates.empty());
      if (!duplicates.empty()) {
        std::cout << "Duplicates (" << duplicates.size() << "):\n";
      }
      for (int duplicate : duplicates) {
        std::cout << duplicate << "\n";
      }
    } else {
      MPI_Gatherv(visited.data(), myCount, MPI_TALYFEM_INT, NULL, NULL,
                  NULL, 0, 0, PETSC_COMM_WORLD);
    }
  }
}
