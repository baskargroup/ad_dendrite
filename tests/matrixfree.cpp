#include "catch.hpp"

#include <dendrite/TalyMat.h>
#include <dendrite/TalyEquation.h>
#include <dendrite/util.h>
#include <dendrite/BoundaryConditions.h>
#include <talyfem/fem/cequation.h>

struct TestNodeData {
  static const int ndof = 2;
  double u[ndof];

  inline double& value(int i) {
    assert (i >= 0 && i < ndof);
    return u[i];
  }

  inline double value(int i) const {
    assert (i >= 0 && i < ndof);
    return u[i];
  }

  static int valueno() {
    return ndof;
  }
};

class TestEquation : public TALYFEMLIB::CEquation<TestNodeData> {
 public:
  void Solve(double t, double dt) override {}

  void Integrands_Ae(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae) {
    static const int ndof = 2;

    // number unique to this integration point
    const double thisCall = 1 + fe.cur_itg_pt_num();
    const double val_at_gp[ndof] = {p_data_->valueFEM(fe, 0), p_data_->valueFEM(fe, 1)};

    for (int d1 = 0; d1 < ndof; d1++) {
      for (int d2 = 0; d2 < ndof; d2++) {
        for (int i = 0; i < fe.nbf(); i++) {
          for (int j = 0; j < fe.nbf(); j++) {
            double N = 0.0;
            for (int k = 0; k < fe.nsd(); k++) {
              N += fe.dN(i, k) * fe.dN(j, k) * thisCall * fe.detJxW();
            }
            Ae(i * ndof + d1, j * ndof + d2) += N * val_at_gp[d1] * (d1 + 1) * val_at_gp[d2] * (ndof + d2 + 1);
          }
        }
      }
    }

    // guarantee matrix not symmetric
    bool is_asym = false;
    for (unsigned int j = 0; j < Ae.ny(); j++) {
      for (unsigned int i = 0; i < Ae.nx() / 2; i++) {
        is_asym = is_asym || (std::fabs(Ae(i, j) - Ae(Ae.nx() - i - 1, j)) > 1e-7);
      }
    }
    assert(is_asym);

    // Ae.fill(1.0);
    /*for (unsigned int i = 0; i < Ae.nx(); i++) {
      Ae(i, i) = 1.0;
    }*/
  }

  // not used by these tests, but required for things to compile
  void Integrands_be(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZEROARRAY<double>& be) {
    assert(false);
  }
};

void MatMultCallback(Mat mat, Vec in, Vec out) {
  std::function<void(Mat, Vec, Vec)>* cb;
  MatShellGetContext(mat, &cb);
  (*cb)(mat, in, out);
}

TEST_CASE("matrix-free matches assembled matrix multiplication", "[matrix-free, TalyMat]") {
  int ierr;

  double problemSize[3]{1.0, 1.0, 1.0};
  ot::DA* octDA = nullptr;

  {
    const double ctr[3] = {0.5, 0.5, 0.5};
    const double r = 0.1;
    auto fx_refine = [ctr, r](double x, double y, double z) -> double {
      return sqrt((x - ctr[0]) * (x - ctr[0]) + (y - ctr[1]) * (y - ctr[1]) + (z - ctr[2]) * (z - ctr[2])) - r - 1e-6;
    };
    octDA = ot::function_to_DA(fx_refine, 3, 5, problemSize, MPI_COMM_WORLD);
  }

  /*{
    std::vector<ot::TreeNode> nodes;
    ot::createRegularOctree(nodes, 4, 3, 5, MPI_COMM_WORLD);
    octDA = new ot::DA(nodes, PETSC_COMM_WORLD, PETSC_COMM_WORLD, 1e-3);
  }*/

  const int ndof = 2;
  Vec vec;
  octDA->createVector(vec, false, false, ndof);
  VecSetRandom(vec, NULL);

  if (!octDA->computeLocalToGlobalMappings())
    octDA->computeLocalToGlobalMappings();

  TalyEquation<TestEquation, TestNodeData>* talyEq = new TalyEquation<TestEquation, TestNodeData>(true, ndof);
  TalyMatrix<TestEquation, TestNodeData>* talyMat = talyEq->mat;
  talyMat->setDA(octDA, problemSize);
  talyMat->setVectors({
    VecInfo(vec, ndof, 0, true)  // copy vec into TestNodeData's u[0] field
  });

  BoundaryConditions bc;
  bc.addByNodalFunction(octDA, problemSize, ndof, [&](double x, double y, double z, unsigned int nodeID) -> Boundary {
    Boundary b;
    // dirichlet BC on all walls
    if (fabs(x) < 1e-9 || fabs(x - problemSize[0]) < 1e-9)
      b.addDirichlet(0, 0.0);
    if (fabs(y) < 1e-9 || fabs(y - problemSize[1]) < 1e-9)
      b.addDirichlet(0, 0.0);
    if (fabs(z) < 1e-9 || fabs(z - problemSize[2]) < 1e-9)
      b.addDirichlet(0, 0.0);

    return b;
  });
  REQUIRE(bc.rows().size() > 0);

  int nRows;
  ierr = VecGetLocalSize(vec, &nRows);
  CHKERRXX(ierr);

  Vec matfreeResult;
  VecDuplicate(vec, &matfreeResult);
  {
    std::function<void(Mat, Vec, Vec)> mulCb = [&](Mat mat, Vec in, Vec out) {
      VecZeroEntries(out);
      talyMat->MatVec(in, out);
    };

    Mat mat;
    ierr = MatCreateShell(PETSC_COMM_WORLD, nRows, nRows, PETSC_DETERMINE, PETSC_DETERMINE, &mulCb,
                          &mat);
    CHKERRXX(ierr);

    ierr = MatShellSetOperation(mat, MATOP_MULT, (void (*)()) (MatMultCallback));
    CHKERRXX(ierr);

    // avoids new nonzero errors when boundary conditions are used
    ierr = MatSetOption(mat, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    CHKERRXX(ierr);

    // matfreeResult = mat * vec
    ierr = MatMult(mat, vec, matfreeResult);
    CHKERRXX(ierr);

    MatDestroy(&mat);

    bc.applyMatrixFreeBC(octDA, vec, matfreeResult);
  }

  Vec assemblyResult;
  VecDuplicate(vec, &assemblyResult);
  {
    Mat mat;
    octDA->createMatrix(mat, MATAIJ, ndof);

    ierr = MatZeroEntries(mat);
    CHKERRXX(ierr);
    talyMat->GetAssembledMatrix(&mat, 0);
    ierr = MatAssemblyBegin(mat, MAT_FINAL_ASSEMBLY);
    CHKERRXX(ierr);
    ierr = MatAssemblyEnd(mat, MAT_FINAL_ASSEMBLY);
    CHKERRXX(ierr);

    bc.applyMatBC(octDA, mat);

    write_mat_matlab(mat, "A", 0.0);  // for debug

    ierr = MatMult(mat, vec, assemblyResult);
    CHKERRXX(ierr);

    MatDestroy(&mat);
  }

  // for debugging
  /*write_vec_matlab(vec, "x", 0.0);
  write_vec_matlab(matfreeResult, "y_matfree", 0.0);
  write_vec_matlab(assemblyResult, "y_assembly", 0.0);*/

  // diff = assemblyResult - matfreeResult
  // (actually: diff = assemblyResult; diff = -1.0 * matfreeResult + diff)
  Vec diff;
  ierr = VecDuplicate(assemblyResult, &diff);
  CHKERRXX(ierr);
  ierr = VecCopy(assemblyResult, diff);
  CHKERRXX(ierr);
  ierr = VecAXPY(diff, -1.0, matfreeResult);
  CHKERRXX(ierr);

  // check if vectors are equal by making sure the max value in diff (infinity norm) is very small
  PetscScalar norm;
  ierr = VecNorm(diff, NORM_INFINITY, &norm);
  CHKERRXX(ierr);
  REQUIRE(norm < 1e-8);
}
