#!/usr/bin/env bash

# ******************************************************************
build_dir='../../../build/examples/Adv_Diff_Linear'
fexec=${build_dir}/'ad_ar'

# ******************************************************************
ADAP=1
MAXREF=2
REFINE_LVL=3
Etol=1e-5
# ******************************************************************
# if [ -f $fexec ]; then
#    rm $fexec
# fi
# cp ${build_dir}/${fexec} .

# ******************************************************************
${fexec} -adap ${ADAP} -maxref ${MAXREF} -refine_lvl ${REFINE_LVL} -etol ${Etol} > output_${REFINE_LVL}.txt 2>&1 & echo $! > process_${REFINE_LVL}.txt
