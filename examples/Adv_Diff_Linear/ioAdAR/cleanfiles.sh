#!/usr/bin/env bash

rm *.vtu
rm *.vtk
rm err*.txt
rm output*.txt
rm process*.txt
