#pragma once

/**
 *
 * @param da
 * @param problem_size
 * @param talyEq
 * @param ti
 */
template<typename T, typename U>
void setMatrixAndVector(ot::DA *da, double *problem_size,
                        TalyEquation<T, U> *talyEq, TimeInfo &ti) {
    auto talyMat = talyEq->mat;
    auto talyVec = talyEq->vec;
    talyMat->setDA(da, problem_size);
    talyVec->setDA(da, problem_size);
    talyMat->setTimeInfo(&ti);
    talyVec->setTimeInfo(&ti);
}

/**
 *
 * @param da
 * @param ts
 * @param problem_size
 * @param talyEq
 * @param ndof
 */
template<typename T, typename U>
void setLinearSolver(ot::DA *da, LinearSolver *ts, double const *problem_size,
                     TalyEquation<T, U> *talyEq, int ndof,
                     Boundary (*bfunc)(double, double, double, unsigned int)) {

    ts->cleanup();
    auto talyMat = talyEq->mat;
    auto talyVec = talyEq->vec;
    ts->setDA(da, problem_size);
    ts->setMatrix(talyMat);
    ts->setVector(talyVec);
    ts->setDof(ndof);
    ts->init();

    ts->setBoundaryCondition(bfunc);
}

/**
 *
 * @tparam T
 * @param baseDA
 * @param problemSize
 * @param ndof
 * @param solution
 * @param tol
 * @param equation
 * @param estimator_domain
 * @param ifSurfErrCalc
 * @return
 */
template<typename T>
ot::DA *refine(ot::DA *baseDA, double problemSize[3], int ndof, Vec &solution,
               double tol, T *const equation, Vec &estimator_vec, unsigned int num_err, std::string estimator_file,
               double *overallEstimator, bool ifSurfErrCalc) {
    std::vector<unsigned int> refinement;
    baseDA->createVector(refinement, true, true, 1);

    const auto ElementError = [&equation](const TALYFEMLIB::FEMElm &fe, const double *ValueFEM,
                                          const double *ValueDerivativeFEM) {

        ZEROPTV pt = fe.position();
        double f = equation->CalcForce(pt, equation->CalcLinearDiffusivity(pt, 0));
        double dudt = ValueDerivativeFEM[2];
        ZEROPTV v_adv = equation->CalcAdvectionVelocity(pt);
        double a_dot_delV = 0;
        for (int ii = 0; ii < 2; ii++) {
            a_dot_delV += v_adv[ii] * ValueDerivativeFEM[ii];
        }

        /// FORMULA for CALCULATING THE DOMAIN RESIDUAL
        double residual = (f - dudt - a_dot_delV);

        /// Return the integrand (i.e., the square of the residual)
        return (residual * residual);
    };

    /**
     * Calculates the surface residual at a single Gauss point
     * All the values in ValueFEM and the other similar data structures
     * pertain to the ordering in fe and not the order in fe_neigh
     *
     * fe_neigh is not actually needed in this function, it's NOT to be used
     *
     */
    const auto SurfaceError = [&equation](const TALYFEMLIB::FEMElm &fe, const TALYFEMLIB::FEMElm &fe_neigh,
                                          const double *ValueFEM, const double *ValueFEM_neigh,
                                          const double *ValueDerivativeFEM, const double *ValueDerivativeFEM_neigh,
                                          const TALYFEMLIB::ZEROPTV &normal, const TALYFEMLIB::ZEROPTV &normal_neigh,
                                          const int surf_id) {

        //PrintInfo("PositionCurrent = ", fe.position());
        /*PrintInfo("valueCurr = ", ValueFEM[0]);
        PrintInfo("valueNeig = ", ValueFEM_neigh[0]);

        PrintInfo("valDer0 = ", ValueDerivativeFEM[0], " ", ValueDerivativeFEM[1], " ", ValueDerivativeFEM[2]);
        PrintInfo("valDer1 = ", ValueDerivativeFEM_neigh[0], " ", ValueDerivativeFEM_neigh[1], " ",
                  ValueDerivativeFEM_neigh[2]);*/

        ZEROPTV pt = fe.position();
        double k = equation->CalcLinearDiffusivity(pt);

        double du_dot_n0 = 0, du_dot_n1 = 0;
        for (int i = 0; i < 2; i++) {
            du_dot_n0 += ValueDerivativeFEM[i] * normal[i];
            du_dot_n1 += ValueDerivativeFEM_neigh[i] * normal_neigh[i];
        }

        /// FORMULA for CALCULATING THE EDGE RESIDUAL (i.e., the jump)
        double jump = k * (du_dot_n0 + du_dot_n1);

        /// Return the integrand (i.e., the square of the jump)
        return (jump * jump);
    };

    const auto refinefunc = [tol](double residual_domain, double residual_edge, int ref_level) {
        double error = residual_domain * residual_domain + residual_edge * residual_edge;
        error = sqrt(error);

        int refine_lvl = ref_level - 1;
        if (error > tol) {
            refine_lvl = ref_level;
        }
        return (refine_lvl);
    };

    /// calc_error function calculates the errors and returns the refinement vector
    /// that contains the elements that are to be refined next
    calc_error(baseDA, problemSize, ndof, solution, refinement, ElementError, SurfaceError, refinefunc, estimator_vec,
               num_err, overallEstimator, ifSurfErrCalc);

    /// Write a file with the element error estimators
    // code for writing a scalar in cell value format
    /*octree2VTK(baseDA, problemSize, {
            VecOutInfo(estimator_vec, 1, {Attribute::scalar("eta", 0)}, true)
    }, estimator_file, false);*/

    // code for writing multiple values in cell value format
    std::vector<Attribute> attrs(3);
    attrs[0] = Attribute::scalar("eta_domain", 0);
    attrs[1] = Attribute::scalar("eta_edge", 1);
    attrs[2] = Attribute::scalar("eta_total", 2);
    octree2VTK(baseDA, problemSize, {
            VecOutInfo(estimator_vec, num_err, attrs, true)
    }, estimator_file, false);


    /// Create the new DA (mesh)
    ot::DA *newDA =
            ot::remesh_DA(baseDA, refinement, problemSize, PETSC_COMM_WORLD);

    /// container for the previous solution
    Vec prev_solution;
    newDA->createVector(prev_solution, false, false, ndof);

    std::vector<double> pts;
    ot::getNodeCoordinates(newDA, pts, problemSize);
    ot::interpolateData(baseDA, solution, prev_solution, NULL, ndof, pts, problemSize);

    VecDestroy(&solution);

    newDA->createVector(solution, false, false, ndof);
    VecCopy(prev_solution, solution);

    VecDestroy(&prev_solution);
    delete baseDA;

    pts.clear();

    return newDA;
}// end of refine



/// Merge utility for different
/**
 *
 * @param file_prefix
 * @param num_procs
 * @param time_step
 */
void write_merge_vtk(const std::string file_prefix, const int num_procs, const int time_step) {

    std::string filename = "merged" + std::to_string(time_step) + ".pvtu";
    std::ofstream file(filename);
    file << "<?xml version=\"1.0\"?> " << std::endl;
    file << "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" >" << std::endl;
    file << "<PUnstructuredGrid GhostLevel=\"0\">\n";
    file << "<PPoints>\n";
    file << "<PDataArray type=\"Float64\" NumberOfComponents=\"3\"/>\n";
    file << "</PPoints>\n";
    file << "<PPointData Scalars=\"u\">\n";
    file << "<PDataArray type=\"Float64\" Name=\"u\" />\n";
    file << "</PPointData>\n";
    for (int i = 0; i < num_procs; i++) {
        char fname[PATH_MAX];
        snprintf(fname, sizeof(fname), "%s_%05d.vtu", file_prefix.c_str(), i);
        file << "<Piece Source=\"" << fname << "\" />\n";
    }
    file << "</PUnstructuredGrid>\n";
    file << "</VTKFile>\n";
    file.close();
}