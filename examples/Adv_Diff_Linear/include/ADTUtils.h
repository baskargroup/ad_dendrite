#pragma once
/**
     * Calculates the dot product between the derivative of a gridfield value and
     * the derivatives of a shape function (indexed by indexfe)
     * @param fe FEMElm object
     * @param indexfe the index of the shape function
     * @param v the Zeroptv object containing the derivatives of the gridfield value
     * @return the dot product between the two
     */

inline double feDerivativeDotProduct(FEMElm const &fe, int const indexfe, ZEROPTV const &v, int nSpatialDim) {
    double scalar = 0;
    for (int k = 0; k < nSpatialDim; k++) {
        scalar += v(k) * fe.dN(indexfe, k);
    }
    return scalar;

}

/**
 * Calculates the dot product between the derivatives of a shape function
 * @param fe FEMElm object
 * @param ind1 first shape function index
 * @param ind2 second shape funciton index
 * @return the dot product between the two
 */

inline double feDerivativeDotProduct(FEMElm const &fe, int const ind1, int const ind2, int nSpatialDim) {
    double scalar = 0;
    for (int k = 0; k < nSpatialDim; k++) {
        scalar += fe.dN(ind1, k) * fe.dN(ind2, k);
    }
    return scalar;
}

/**
 * Calculates the dot product between two ZEROPTV variables upto the dimension specified
 * @param v1
 * @param v2
 * @param nSpatialDim
 * @return
 */
inline double DotProductWithDimension(ZEROPTV const &v1, ZEROPTV const &v2, int nSpatialDim) {
    double scalar = 0;
    for (int k = 0; k < nSpatialDim; k++) {
        scalar += v1(k) * v2(k);
    }
    return scalar;
}

/**
    * Calculates the dot product between two ZEROPTV variables upto the dimension specified
    * @param v1
    * @param v2
    * @param nSpatialDim
    * @return
    */
inline double SpatialNorm(ZEROPTV const &v1, int nSpatialDim) {
    double scalar = 0;
    for (int k = 0; k < nSpatialDim; k++) {
        scalar += v1(k) * v1(k);
    }
    return sqrt(scalar);
}

/**
* Calculates the dot product between two ZEROPTV variables upto the dimension specified
* @param v1
* @param v2
* @param nSpatialDim
* @return
*/
inline double SpatialDotProduct(ZEROPTV const &v1, ZEROPTV const &v2, int nSpatialDim) {
    double scalar = 0;
    for (int k = 0; k < nSpatialDim; k++) {
        scalar += v1(k) * v2(k);
    }
    return scalar;
}