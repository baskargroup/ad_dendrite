#pragma once

#include <map>
#include <string>

enum ManufacturedCases : int {
    AD_Bicosine_Cone_2D = 1,
    D_Rotating_Source = 2,
};

class ADTInputData : public InputData {
public:
    /// Unclassified
    std::string config_file;
    int iCase_;

    /// PDE specific data
    double Pe_mesh;
    double kdif;

    /// FEM specific Flags
    int ifStabilize;
    int ifStabTime;
    int ifStabSpace;
    int useTalyfemSUPG;
    int ifStabTypeSUPG;

    /// Refinement related parameters
    int ifSurfErrCalc;
    int maxAdapRefLevel;
    double refineErrTol;

    ADTInputData()
            : InputData(),
              config_file("config.txt") {

        /// Initialize the cfg object (that reads the input config file)
        try {
            cfg.setAutoConvert(true);  // deprecated
            // this is the correct autoconvert call for the current libconfig version:
            // cfg.setOptions(libconfig::Config::OptionAutoConvert);
            cfg.readFile(config_file.c_str());
        } catch (const libconfig::FileIOException &fioex) {
            throw TALYException() << "I/O error while reading " << config_file << ": " << fioex.what();
        } catch (const libconfig::ParseException &pex) {
            throw TALYException() << "Parse error at " << pex.getFile() << ":" <<
                                  pex.getLine() << " - " << pex.getError();
        }

        /// Initialize other things
        ifSurfErrCalc = false;
        maxAdapRefLevel = 5;
        refineErrTol = 1e-7;
    }

    void ReadConfigFile() {
        std::string config_file = "config.txt";
        libconfig::Config cfg;
        cfg.readFile(config_file.c_str());
        cfg.lookupValue("Case", iCase_);
    }

    bool ReadFromFile(const std::string &filename = std::string("config.txt")) {
        // Read config file and initialize basic fields
        PrintInfo("***************** INPUTS *************************");
        if (ReadValue("Case", iCase_)) {}
        if (ReadValue("Kdif", kdif)) {}

        if (ReadValue("useTalyfemSUPG", useTalyfemSUPG)) {}
        if (ReadValue("ifStabTypeSUPG", ifStabTypeSUPG)) {}

        if (ReadValue("ifStabSpace", ifStabSpace)) {}
        if (ReadValue("ifStabTime", ifStabTime)) {}

        if (ReadValue("ifSurfErrCalc", ifSurfErrCalc)) {}
        if (ReadValue("maxAdapRefLevel", maxAdapRefLevel)) {}
        if (ReadValue("refineErrTol", refineErrTol)) {}

        // not being read right now, not being used
        if (ReadValue("ifStabilize", ifStabilize)) {}
        if (ReadValue("Pe_mesh", Pe_mesh)) {}
        PrintInfo("**************************************************");
        return true;
    }
};
