#pragma once

#include <talyfem/stabilizer/tezduyar_upwind.h>

class Eq_AD_SharpIC : public TALYFEMLIB::CEquation<ADTNodeData> {
private:
    ADTInputData *const inputData_;
    int useTalyfemSUPG;
    int ifStabTypeSUPG;
    TezduyarUpwindFE upfe_s_, upfe_t_;
    double gls_s_, gls_t_;

public:
    explicit Eq_AD_SharpIC(ADTInputData *const cdata) : inputData_(cdata) {
        useTalyfemSUPG = inputData_->useTalyfemSUPG;
        ifStabTypeSUPG = static_cast<bool>(useTalyfemSUPG) ? 1 : inputData_->ifStabTypeSUPG;
    }

    void Solve(double dt, double t) override {
        assert(false);
    }

    void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
                    TALYFEMLIB::ZEROARRAY<double> &be) override {
        assert(false);
    }

    /// Interface function for the boundary conditions
    /// The function pointer will be handed over to the calcL2Error function
    static double CalcAnalyticSol(double x, double y, double z) {
        return 0.0;
    }

    static double CalcInitialCondition(double x, double y, double z) {
        double val;
        double x0 = 1.0 / 3.0;
        double sigma = 0.1;
        double len = pow((x - x0) / sigma, 2) + pow((y - x0) / sigma, 2);
        if (len <= 1) {
            val = 1.0;
        } else {
            val = 0;
        }
        return val;
    }

    static double CalcBoundaryCondition(double x, double y, double z) {
        return 0.0;
    }

    ZEROPTV CalcAdvectionVelocity(ZEROPTV const &pt = {}) const {
        double mult = 2 * M_PI;
        double vx = -mult * (pt.y() - 0.5);
        double vy = mult * (pt.x() - 0.5);
        return {vx, vy, 0.0};
    }

    double CalcLinearDiffusivity(ZEROPTV const &pt = {}, double const h = 0.0) const {
        return inputData_->kdif;
    }

    double CalcForce(ZEROPTV const &pt, double const kdif) {
        return 0.0;
    }

    /// Interface function for the boundary conditions
    /// The function pointer will be handed over to the LinearSolver object
    static Boundary BoundaryFunction(double x, double y, double z, unsigned int nodeID) {
        Boundary b;
        static constexpr double eps = 1e-14;
        double xmax = 1.0;
        double ymax = 1.0;

        bool onTimeBoundary = fabs(z) < eps;
        bool onSpatialBoundary = fabs(x) < eps || fabs(y) < eps || fabs(x - xmax) < eps || fabs(y - ymax) < eps;

        if (onTimeBoundary || onSpatialBoundary) {
            double val = 0;

            //Initial condition
            if (onTimeBoundary) {
                val = CalcInitialCondition(x, y, z);
            }

            // Boundary condition
            if (onSpatialBoundary) {
                val = CalcBoundaryCondition(x, y, z);
            }

            // Add this value as a Dirichlet value
            b.addDirichlet(0, val);
        }
        return b;
    }

    double CalcGLSParameter(const FEMElm &fe, const ZEROPTV &vel, double k, double h) {

        int const nSpatialDim = fe.nsd() - 1;

        /// delta_gls calculation treating time as separate

        double vel_norm = SpatialNorm(vel, nSpatialDim);

        double vel_term = 2.0 * vel_norm / h;

        double diffusion_term = 4 * k / (h * h);

        double dt = h;
        double dt_term = 1.0 / (0.5 * dt);

        double temp = vel_term + diffusion_term;
        double delta_gls = 1.0 / temp;

        /// delta_gls calculation treating time alongwith spatial advection
        double vel_norm_enhanced = sqrt(
                vel_norm * vel_norm + 1 * 1); // taking time derivative as an advection component

        double vel_term_enhanced = 2.0 * vel_norm_enhanced / h;

        double temp_enhanced = vel_term_enhanced + diffusion_term;

        double delta_gls_enhanced = 1.0 / temp_enhanced;

        //return delta_gls;
        return delta_gls_enhanced;
    }

    double CalcGLSTimeParameter(FEMElm const &fe, double h_time) {
        double delta_gls_t = h_time / 2.0;
        //PrintInfo("delta_gls_t = ", delta_gls_t);
        return delta_gls_t;
    }

    void CalcAllStabParameters(const FEMElm &fe, const ZEROPTV &adv_s, const ZEROPTV &adv_t, double kdif, double h) {
        double stab_parameter = CalcGLSParameter(fe, adv_s, kdif, h);
        //gls_t_ = CalcGLSTimeParameter(fe, h);

        gls_s_ = stab_parameter * static_cast<double>(inputData_->ifStabSpace);
        gls_t_ = stab_parameter * static_cast<double>(inputData_->ifStabTime);

        upfe_s_.calcSUPGWeightingFunction(fe, adv_s, kdif);
        upfe_t_.calcSUPGWeightingFunction(fe, adv_t, 0);
    }


    void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be) {
        ///  Set the indices right
        int const nSpatialDim = fe.nsd() - 1;
        int const tIndex = fe.nsd() - 1; //index used to denote the time variable in below calculations

        /// Get the mesh size
        const double detJ = fe.jacc();
        const double h = pow(detJ * 8.0, 1.0 / 3.0);

        /// Calculate the required-data
        ZEROPTV pt = fe.position();
        ZEROPTV adv_s = CalcAdvectionVelocity(pt);
        ZEROPTV adv_t = ZEROPTV(0, 0, 1);
        double kdif = CalcLinearDiffusivity(pt);
        double force = CalcForce(pt, kdif);

        /// Calculate the stabilization parameters
        CalcAllStabParameters(fe, adv_s, adv_t, kdif, h);

        /// Get in loop
        /// a is for test function (v). And b is for trial function (u)
        for (int a = 0; a < fe.nbf(); a++) {
            double vel_dot_dNa = feDerivativeDotProduct(fe, a, adv_s, nSpatialDim);

            double time_corr = static_cast<double>(inputData_->ifStabTime) *
                               (useTalyfemSUPG ? upfe_t_.SUPG(a) : gls_t_ * fe.dN(a, tIndex));
            double spatial_corr = static_cast<double>(inputData_->ifStabSpace) *
                                  (useTalyfemSUPG ? upfe_s_.SUPG(a) : gls_s_ * vel_dot_dNa);
            be(a) += (fe.N(a) + time_corr + spatial_corr) * force * fe.detJxW();
        }
    }

    void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae) {
        ///  Set the indices right
        int const nSpatialDim = fe.nsd() - 1;
        int const tIndex = fe.nsd() - 1; //index used to denote the time variable in below calculations

        /// Get the mesh size
        const double detJ = fe.jacc();
        const double h = pow(detJ * 8.0, 1.0 / 3.0);

        /// Calculate the required-data
        ZEROPTV pt = fe.position();
        ZEROPTV adv_s = CalcAdvectionVelocity(pt);
        ZEROPTV adv_t = ZEROPTV(0, 0, 1);
        double kdif = CalcLinearDiffusivity(pt);
        double force = CalcForce(pt, kdif);

        /// Calculate the stabilization parameters
        CalcAllStabParameters(fe, adv_s, adv_t, kdif, h);

        /// Get in loop
        /// a is for test function (v). And b is for trial function (u)
        for (int a = 0; a < fe.nbf(); a++) {
            double vel_dot_dNa = feDerivativeDotProduct(fe, a, adv_s, nSpatialDim);

            double time_corr = static_cast<double>(inputData_->ifStabTime) *
                               (useTalyfemSUPG ? upfe_t_.SUPG(a) : gls_t_ * fe.dN(a, tIndex));
            double spatial_corr = static_cast<double>(inputData_->ifStabSpace) *
                                  (useTalyfemSUPG ? upfe_s_.SUPG(a) : gls_s_ * vel_dot_dNa);

            for (int b = 0; b < fe.nbf(); b++) {
                double vel_dot_dNb = feDerivativeDotProduct(fe, b, adv_s, nSpatialDim);

                // the u_t term
                double time_term = (fe.N(a) + time_corr + spatial_corr) * fe.dN(b, tIndex);
                // the advection (a \dot (\nabla u)) term
                double adv_term = (fe.N(a) + time_corr + spatial_corr) * vel_dot_dNb;
                // the diffusion term
                double diff_term = 0;
                for (int i = 0; i < nSpatialDim; i++) {
                    // the original diffusion term
                    diff_term += fe.dN(a, i) * fe.dN(b, i) * kdif;
                    // the cross of (\Laplacian u) and v_t
                    diff_term += gls_t_ * fe.d2N(a, i, tIndex) * fe.dN(b, i) * kdif;
                    // the cross of u_t and (\Laplacian v)
                    diff_term += not(ifStabTypeSUPG) * gls_s_ * fe.dN(a, i) * fe.d2N(b, i, tIndex) * kdif;
                }
                Ae(a, b) += (time_term + adv_term + diff_term) * fe.detJxW();
            }
        }
    }
};
