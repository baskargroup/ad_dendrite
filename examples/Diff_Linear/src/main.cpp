#include <iostream>
#include <fstream>
#include <vector>
#include <functional>
#include <iomanip>

#include <dendrite/solvers/LinearSolver.h>
#include <dendrite/DendroIO.h>
#include <dendrite/TalyEquation.h>
#include <dendrite/dendrite.h>
#include <dendrite/util.h>
#include <dendrite/analytic.h>

#include <talyfem/talyfem.h>
#include <ADTUtils.h>
#include <ADTNodeData.h>
#include <ADTInputData.h>

#include <Eq_AD_MMS.h>
#include <Eq_AD_SmoothIC.h>
#include <Eq_AD_SharpIC.h>
#include <Eq_Diff_ExpDecay.h>
#include <Eq_Diff_RotatingSource.h>
#include <Eq_Diff_StationarySource.h>
#include <Eq_Diff_StationaryGaussian.h>
#include <Eq_AD_MMS2.h>

#include <OtherFunctions.h>

/**
 * Icase is just a dummy for ICASE (which is being
 * passed through cmake). But Icase is actually being
 * used for the conditional compilations below.
 * ICASE is needed for the compilation of the code
 * It NECESSARILY has to be passed as an option to cmake
 * in the command line as follows.
 * For example, for case 1, do
 * cmake .. -DICASE=1
 */
#define ICase ICASE

/**
 * @param argc
 * @param argv
 * @return
 */

int main(int argc, char **argv) {
    dendrite_init(argc, argv);

    int comm_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    int comm_size;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

    const int ndof = 1;

    // *********************************************************
    // TEMPLATED CODE INFORMATION
    // *********************************************************
    std::string code_name;
    if (ICASE == 1) {
        code_name = "DIFFUSION-Stationary_Heat_Source";
    } else if (ICASE == 2) {
        code_name = "ADVECTION-DIFFUSION-Sharp Initial Condition";
    }
    if (comm_rank == 0) {
        std::cout << "================== CASE " << ICase << " : ADAPTIVE REFINEMENT FOR " << code_name
                  << " =================="
                  << "\n";
        std::cout << "MPI Size = " << comm_size << "\n";
    }

    // *********************************************************
    // Command Line Options Setup
    // *********************************************************
    PetscBool mf = PETSC_FALSE;
    PetscOptionsGetBool(NULL, 0, "-mfree", &mf, 0);
    int refine_lvl = 1;
    CHKERRQ (PetscOptionsGetInt(NULL, 0, "-refine_lvl", &refine_lvl, 0));
    PetscBool adap;
    CHKERRQ (PetscOptionsGetBool(NULL, 0, "-adap", &adap, 0));
    PetscInt iCaseRead = 0;
    CHKERRQ (PetscOptionsGetInt(NULL, 0, "-icase", &iCaseRead, 0));
    PetscReal tempErrTol = 1E-10;
    PetscBool setErrTol;
    CHKERRQ (PetscOptionsGetReal(NULL, 0, "-etol", &tempErrTol, &setErrTol));
    PetscInt tempMaxRefLvl = 0;
    PetscBool setMaxAdref;
    CHKERRQ (PetscOptionsGetInt(NULL, 0, "-maxref", &tempMaxRefLvl, &setMaxAdref));

    bool mfree = (mf == PETSC_TRUE);
    if (comm_rank == 0) {
        std::cout << "Matrix-free: " << (mfree ? "true" : "false") << "\n";
    }

    // *********************************************************
    // Input and Case data setup
    // *********************************************************
    /// Read config file again
    /// And set the function pointers
    ADTInputData idata;
    idata.ReadFromFile();

    bool ifSurfErrCalc = static_cast<bool>(idata.ifSurfErrCalc);

    if (setErrTol) {
        idata.refineErrTol = tempErrTol;
    }
    if (setMaxAdref) {
        idata.maxAdapRefLevel = tempMaxRefLvl;
    }
    double refineErrTol = idata.refineErrTol;
    int maxAdapRefLevel = idata.maxAdapRefLevel;

    double frac = 1000; // a very large arbitrary number

    // *********************************************************
    // Filename strings
    // *********************************************************
    std::string data_file_prefix("data");
    std::string errorfilename = std::string("error_data_") + std::to_string(refine_lvl) + std::string(".txt");
    std::ofstream error_file(errorfilename, std::ios::out);
    error_file << std::left
               << std::setw(12) << "nodes" << " "
               << std::setw(12) << "L2_Err" << " "
               << std::setw(12) << "eta" << " "
               << std::setw(12) << "Solve_time" << " "
               << std::setw(12) << "Refine_time" << "\n";
    std::string estimator_file_prefix("errorEstimators");

    // *********************************************************
    // Octree Creation
    // *********************************************************
    double problemSize[3] = {1.0, 1.0, 1.0};
    ot::DA *baseDA = NULL;

    // use a regular octree
    std::vector<ot::TreeNode> oct;
    const int max_depth = refine_lvl + 20;
    assert(max_depth < 32);
    createRegularOctree(oct, refine_lvl, 3, max_depth, MPI_COMM_WORLD);
    baseDA = new ot::DA(oct, PETSC_COMM_WORLD, PETSC_COMM_WORLD, 0.3);
    PrintInfo("Created Base DA");
    MPI_Barrier(PETSC_COMM_WORLD);

    // *********************************************************
    // Equation & solver Setup (along with function pointers
    // *********************************************************
    /**
     * Three functions, namely,
     * setMatrixAndVector, setLinearSolver and refine
     * are templated for the Equation and the NodeData class
     * They have to be instantiated at compile time
     * The relevant case is selected by passing the ICASE variable
     * See above where Icase is defined
     *
     */
#if (ICase == 1)
    auto talyEquation = new TalyEquation<Eq_Diff_StationaryGaussian, ADTNodeData>(true, ndof, &idata);
    /// Set the relevant function pointers (static functions)
    auto bfunc = Eq_Diff_StationaryGaussian::BoundaryFunction;
    auto analytic_sol = Eq_Diff_StationaryGaussian::CalcAnalyticSol;
#pragma message ("This code is for Diff-Stationary_Heat_Source.")
#elif (ICase == 2)
    auto talyEquation = new TalyEquation<Eq_AD_SharpIC, ADTNodeData>(true, ndof, &idata);
    /// Set the relevant function pointers (static functions)
    auto bfunc = Eq_AD_SharpIC::BoundaryFunction;
    auto analytic_sol = Eq_AD_SharpIC::CalcAnalyticSol;
#pragma message ("This code is for AD-SharpIC.")
#endif

    /// Set mat and vec
    auto talyMat = talyEquation->mat;
    auto talyVec = talyEquation->vec;
    // set solver...
    LinearSolver *solver = new LinearSolver();
    Vec prev_solution;

    /// SOLVE and REFINIEMENT loop below
    PrintInfo("Starting solve.");
    TimeInfo ti(0, 0);
    for (int refine_counter = 0; refine_counter < maxAdapRefLevel + 1; refine_counter++) {
        // *********************************************************
        // Pre-solve setup
        // *********************************************************
        setMatrixAndVector(baseDA, problemSize, talyEquation, ti);
        setLinearSolver(baseDA, solver, problemSize, talyEquation, ndof, bfunc);
        solver->setMatrixFree(mfree);

        // Before the first iteration the prev_solution and solver->getCurrentSolution()
        // are null vectors. If the destroy or getCurrentSolution() is called, it gives error.
        if (refine_counter > 0) {
            VecCopy(prev_solution, solver->getCurrentSolution());
            VecDestroy(&prev_solution);
        }
        baseDA->createVector(prev_solution, false, false, ndof);
        talyMat->setVectors({VecInfo(prev_solution, ndof, 0, true)});
        talyVec->setVectors({VecInfo(prev_solution, ndof, 0, true)});

        // *********************************************************
        // Solve
        // *********************************************************
        double stime = MPI_Wtime();
        solver->solve();  // uses talyMat to assemble matrix, talyVec to assemble RHS, then calculates solution
        double etime = MPI_Wtime();
        PrintInfo("Solve finishes.");
        PrintInfo("Total time for solve: ", etime - stime);

        // *********************************************************
        // L2 Error
        // *********************************************************
        MPI_Barrier(PETSC_COMM_WORLD);
        double err = calcL2Error(baseDA, solver->getProblemSize(), solver->getCurrentSolution(),
                                 solver->getDof(), 0, analytic_sol);
        PrintInfo("Calculating L2 error");
        PrintInfo("L2_error = ", err);

        //**********************************************************
        // Writing solution to files
        //**********************************************************
        std::string filename = data_file_prefix + std::to_string(refine_counter);
        // save result as data_final_xxxx.vtk (where xxxx is the process rank)
        octree2VTU(baseDA, problemSize, {
                VecOutInfo(solver->getCurrentSolution(), 1, {Attribute::scalar("u", 0)}, false)
        }, filename, false);
        MPI_Barrier(PETSC_COMM_WORLD);
        // merge data from different processors (if running on multiprocess)
        if (comm_size > 1 && comm_rank == 0) {
            write_merge_vtk(filename, comm_size, refine_counter);
        }

        /// Break out of the refinement loop if the fractional
        /// change in the problem size is less than a cutoff value
        if (frac < 1.01) {
            PrintInfo("Fractional change (Less than 1.01)", frac);
            break;
        }

        //**********************************************************
        // Adaptive refinement section
        //**********************************************************
        if (adap == PETSC_TRUE) {
            /// Vector for the error estimators
            unsigned int num_err_quantities = 3;
            Vec estimator_vec;
            baseDA->createVector(estimator_vec, true, false, num_err_quantities);
            VecZeroEntries(estimator_vec);

            VecCopy(solver->getCurrentSolution(), prev_solution);

            int nodes_current = baseDA->getNodeSize();
            double estimator_domain, estimator_edge, overallEstimator;

            /// Refinement routine is called and timed
            std::string estimator_file = estimator_file_prefix + std::to_string(refine_counter);

            double start_refine_time = MPI_Wtime();
            baseDA = refine(baseDA, problemSize, ndof, prev_solution, refineErrTol, talyEquation->equation(),
                            estimator_vec, num_err_quantities, estimator_file, &overallEstimator, ifSurfErrCalc);
            double end_refine_time = MPI_Wtime();

            overallEstimator = sqrt(overallEstimator);

            /// Write another file with the values of L2-errors, overall_estimators
            /// and the time required for the refinement process
            error_file << std::left
                       << std::setw(12) << nodes_current << " "
                       << std::setw(12) << err << " "
                       << std::setw(12) << overallEstimator << " "
                       << std::setw(12) << etime - stime << " "
                       << std::setw(12) << end_refine_time - start_refine_time
                       << "\n";
            PrintInfo("eta_L2 = ", overallEstimator);

            /// Calculate the ratio of the node numbers in
            /// the current and the next cycle
            int nodes_next = baseDA->getNodeSize();
            frac = ((double) nodes_next / ((double) nodes_current));
            PrintInfo("Fractional change in size (next/current) = ", frac);

            VecDestroy(&estimator_vec);
        } else {
            error_file << err << " " << baseDA->getNodeSize() << " " << etime - stime << "\n";
            break;
        }
        MPI_Barrier(PETSC_COMM_WORLD);
    }
    error_file.close();
    PrintInfo("Error File has been written");
    PrintInfo("Program ends here.");

    delete solver;
    dendrite_finalize();

}
