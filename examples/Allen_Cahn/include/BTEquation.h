#pragma once

#include <talyfem/fem/cequation.h>
#include "BTNodeData.h"
#include <talyfem/stabilizer/tezduyar_upwind.h>

class BTEquation : public TALYFEMLIB::CEquation<BTNodeData> {
 public:
  void Solve(double delta_t, double current_time) override {
    assert(false);
  }
  void Integrands(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae,
                  TALYFEMLIB::ZEROARRAY<double>& be) override {
    assert(false);
  }

  void Integrands_Ae(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae);
  void Integrands_be(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZEROARRAY<double>& be);
  double get_val(double u);
  double get_deriv_val(double u);
  inline double get_norm_factor();
};

// note: BC on x- and x+ walls at xl = -0.4 and xr = 0.4
// dirichlet 0 (because nonlinear), IC -0.668371 for left and 3.017089 for right

void BTEquation::Integrands_Ae(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae) {
  using namespace TALYFEMLIB;

   const int n_dimensions = fe.nsd();      // # of dimensions: 1D, 2D, or 3D
    const int n_basis_functions = fe.nbf();  // # of basis functions
    const double detJxW = fe.detJxW();      // (determinant of J) cross W
    const double u  = p_data_->valueFEM(fe, 0);
    const double D = 1;
    const double Cn = 0.1;
    const double detJ = fe.jacc();
    const double dt = pow(detJ*8.0,1.0/3.0);
    const double theta = 0.1;


    double t0 = get_norm_factor();
    for (int a = 0; a < n_basis_functions; a++) {
      for (int b = 0; b < n_basis_functions; b++) {
        double N = 0;
        double M = (fe.N(a)*fe.dN(b,2) + theta*dt*fe.dN(a,2)*fe.dN(b,2))*detJxW;
        for (int k = 0; k < 2; k++) {
            N += D*Cn*Cn*(fe.dN(a, k) * fe.dN(b, k) + theta*dt*fe.d2N(a,2,k)*fe.dN(b,k))* detJxW;
        }
        double M1 = D*(fe.N(a)*fe.N(b) + theta*dt*fe.dN(a,2)*fe.N(b))*get_deriv_val(u)*detJxW;
        Ae(a, b) += M + M1;
        Ae(a, b) += N;
      }
      
    }
  }

void BTEquation::Integrands_be(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZEROARRAY<double>& be) {
  using namespace TALYFEMLIB;

  const int n_basis_functions = fe.nbf();  // # of basis functions
    const double detJxW = fe.detJxW();      // (determinant of J) cross W

    const ZEROPTV pt = fe.position();
    double ux[3];
    const double D =  1;
    const double Cn = 0.1;
    double t0 = get_norm_factor();
    const double detJ = fe.jacc();
    const double dt = pow(detJ*8.0,1.0/3.0);
    const double theta = 0.1;

    ux[0] = this->p_data_->valueDerivativeFEM(fe, 0, 0);
    ux[1] = this->p_data_->valueDerivativeFEM(fe, 0, 1);
    ux[2] = this->p_data_->valueDerivativeFEM(fe, 0, 2);

//    std::cout << pt << " " << ux[0] << " " << ux[1] << " " << ux[2] << "\n";
    const double u  = p_data_->valueFEM(fe, 0);

    for (int a = 0; a < n_basis_functions; a++) {
        double N = 0;
        for (int k = 0; k < 2; k++) {
            N += D*Cn*Cn*(fe.dN(a,k)*ux[k] + theta*dt*fe.d2N(a,2,k)*ux[k])* detJxW;
        }
        be(a) += (fe.N(a) + theta*dt*fe.dN(a,2))*ux[2]*detJxW*t0 +
                (fe.N(a) + theta*dt*fe.dN(a,2))*get_val(u)*detJxW*t0;
        be(a) += N;
    }

  }
  
double BTEquation::get_val(double u){
    double t0 = get_norm_factor();
    return(10*(u - 3*u*u*t0 + 2*u*u*u*t0*t0) -1.0 );

}

double BTEquation::get_deriv_val(double u){
      double t0 = get_norm_factor();
        return(10*(1 -6*u*t0 + 6*u*u*t0*t0));
}

inline double BTEquation::get_norm_factor(){
    return (1.0);
}
