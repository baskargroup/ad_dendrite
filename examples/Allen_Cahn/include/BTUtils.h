//
// Created by maksbh on 5/14/19.
//


#include <iostream>
#include <fstream>
#include <vector>
#include <functional>

#include <dendrite/dendrite.h>

#include <dendrite/solvers/NonlinearSolver.h>
#include <dendrite/DendroIO.h>
#include <dendrite/TalyEquation.h>
#include <dendrite/util.h>  // for setScalarByValue


#ifndef DENDRITE_BTUTILS_H
#define DENDRITE_BTUTILS_H
/**
 *
 * @tparam T
 * @param baseDA
 * @param problemSize
 * @param ndof
 * @param solution
 * @param tol
 * @param equation
 * @param estimator_domain
 * @param ifSurfErrCalc
 * @return
 */
template<typename T>
ot::DA *refine(ot::DA *baseDA, double problemSize[3], int ndof, Vec &solution,
               double tol, T *const equation, Vec &estimator_vec, unsigned int num_err, std::string estimator_file,
               double *overallEstimator, bool ifSurfErrCalc = false) {
    using namespace TALYFEMLIB;
    std::vector<unsigned int> refinement;



    const auto ElementError = [&equation](const TALYFEMLIB::FEMElm &fe, const double *ValueFEM,
                                          const double *ValueDerivativeFEM) {

        if((ValueFEM[0] > 0.2 ) and (ValueFEM[0] < 0.6)){
            return 1.0;
        }
        else{
            return 0.0;
        }

    };
    /**
     * Calculates the surface residual at a single Gauss point
     * All the values in ValueFEM and the other similar data structures
     * pertain to the ordering in fe and not the order in fe_neigh
     *
     * fe_neigh is not actually needed in this function, it's NOT to be used
     *
     */
    const auto SurfaceError = [&equation](const TALYFEMLIB::FEMElm &fe, const TALYFEMLIB::FEMElm &fe_neigh,
                                          const double *ValueFEM, const double *ValueFEM_neigh,
                                          const double *ValueDerivativeFEM, const double *ValueDerivativeFEM_neigh,
                                          const TALYFEMLIB::ZEROPTV &normal, const TALYFEMLIB::ZEROPTV &normal_neigh,
                                          const int surf_id) {


        return (0);
    };

    const auto refinefunc = [tol](double residual_domain, double residual_edge, int ref_level) {
        int refine_lvl = ref_level ;

        if(residual_domain > 0){
            refine_lvl = refine_lvl + 1;

        }
        else{
            refine_lvl = refine_lvl - 1;

        }
        return (refine_lvl);
    };

    /// calc_error function calculates the errors and returns the refinement vector
    /// that contains the elements that are to be refined next
    calc_error(baseDA, problemSize, ndof, solution, refinement, ElementError, SurfaceError, refinefunc, estimator_vec,
               num_err, overallEstimator, ifSurfErrCalc);



    /// Create the new DA (mesh)
    ot::DA *newDA =
            ot::remesh_DA(baseDA, refinement, problemSize, PETSC_COMM_WORLD);
    /// container for the previous solution
    Vec prev_solution;
    newDA->createVector(prev_solution, false, false, ndof);

    std::vector<double> pts;
    ot::getNodeCoordinates(newDA, pts, problemSize);
    ot::interpolateData(baseDA, solution, prev_solution, NULL, ndof, pts, problemSize);

    VecDestroy(&solution);


    newDA->createVector(solution, false, false, ndof);
    VecCopy(prev_solution, solution);

    VecDestroy(&prev_solution);
    delete baseDA;

    pts.clear();

    return newDA;
}// end of refine


#endif //DENDRITE_BTUTILS_H
