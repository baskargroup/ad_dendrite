

#include <iostream>
#include <fstream>
#include <vector>
#include <functional>

#include <dendrite/dendrite.h>

#include <dendrite/solvers/NonlinearSolver.h>
#include <dendrite/DendroIO.h>
#include <dendrite/TalyEquation.h>
#include <dendrite/util.h>  // for setScalarByValue
#include <dendrite/analytic.h>
#include <cmath>
#include "BTEquation.h"
#include "BTUtils.h"

using TALYFEMLIB::PrintInfo;

void write_merge_vtk(const std::string file_prefix, const int num_procs, const int time_step) {

    std::string filename = "Results/merged" + std::to_string(time_step) + ".pvtu";
    std::ofstream file(filename);
    file << "<?xml version=\"1.0\"?> " << std::endl;
    file << "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" >" << std::endl;
    file << "<PUnstructuredGrid GhostLevel=\"0\">\n";
    file << "<PPoints>\n";
    file << "<PDataArray type=\"Float64\" NumberOfComponents=\"3\"/>\n";
    file << "</PPoints>\n";
    file << "<PPointData Scalars=\"u\">\n";
    file << "<PDataArray type=\"Float64\" Name=\"u\" />\n";
    file << "</PPointData>\n";
    for (int i = 0; i < num_procs; i++) {
        char fname[PATH_MAX];
        snprintf(fname, sizeof(fname), "%s_%05d.vtu", file_prefix.c_str(), i);
        file << "<Piece Source=\"" << fname << "\" />\n";
    }
    file << "</PUnstructuredGrid>\n";
    file << "</VTKFile>\n";
    file.close();
}

void setMatrixAndVector(ot::DA *da, double problem_size[3], TalyEquation<BTEquation, BTNodeData> *talyEq) {
    auto talyMat = talyEq->mat;
    auto talyVec = talyEq->vec;
    talyMat->setDA(da, problem_size);
    talyVec->setDA(da, problem_size);

}

void setNonLinearSolver(ot::DA *da, NonlinearSolver *ts, double problem_size[3],
                        TalyEquation<BTEquation, BTNodeData> *talyEq, int ndof) {
    ts->cleanup();
    auto talyMat = talyEq->mat;
    auto talyVec = talyEq->vec;

    ts->setDA(da, problem_size);
    ts->setMatrix(talyMat);
    ts->setVector(talyVec);
    ts->setDof(ndof);

    ts->init();
}


int main(int argc, char **argv) {
    dendrite_init(argc, argv);

    PetscLogStagePush(createStage);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    const int ndof = 1;
    double problemSize[3]{1.0, 1.0, 1.0};

    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    bool mfree;
    {
        PetscBool mf = PETSC_FALSE;
        PetscOptionsGetBool(NULL, NULL, "-mfree", &mf, NULL);
        mfree = (mf == PETSC_TRUE);
    }

    int refine_lvl = 4;
    PetscOptionsGetInt(NULL, NULL, "-refine_lvl", &refine_lvl, NULL);

    ot::DA *baseDA = NULL;
    {
        assert(refine_lvl < 31);
        std::vector<ot::TreeNode> nodes;
        ot::createRegularOctree(nodes, refine_lvl, 3, refine_lvl + 10, MPI_COMM_WORLD);
        baseDA = new ot::DA(nodes, PETSC_COMM_WORLD, PETSC_COMM_WORLD, 1e-3);
    }


    PrintStatus("Created DA.");
    MPI_Barrier(PETSC_COMM_WORLD);

    std::vector<unsigned int> refinement;
    baseDA->createVector(refinement, true, true, 1);

    auto talyEq = new TalyEquation<BTEquation, BTNodeData>(true, ndof);
    NonlinearSolver *ts = new NonlinearSolver();
    Vec prev_solution;
    unsigned int num_err_quantities = 3;
    Vec estimator_vec;
    double overallEstimator;
    std::string estimator_file("errorEstimators");

    for(int time = 0; time < 3; time++){
        setMatrixAndVector(baseDA, problemSize, talyEq);
        auto talyMat = talyEq->mat;
        auto talyVec = talyEq->vec;

        setNonLinearSolver(baseDA, ts, problemSize, talyEq, ndof);
        ts->setMatrixFree(mfree);
        if(time > 0){
            VecCopy(prev_solution,ts->getCurrentSolution());
            VecDestroy(&prev_solution);
        }
        // sync the SNES guess with BTNodeData value index 0 for both Ae and be
        baseDA->createVector(prev_solution, false, false, ndof);
        talyMat->setVectors({VecInfo(PLACEHOLDER_GUESS, 1, 0, true)});
        talyVec->setVectors({VecInfo(PLACEHOLDER_GUESS, 1, 0, true)});
        baseDA->createVector(estimator_vec, true, false, num_err_quantities);

        ts->setBoundaryCondition([&](double x, double y, double z, int nodeID) -> Boundary {
            Boundary b;
            double eps = 1E-9;
            bool wall = (fabs(z) < eps);
            if (wall) {
                double dist = sqrt(x * x + y * y);
                double val = 0.5 + 0.5*tanh((dist - 0.5)/(sqrt(2/10.0)*0.1));
                b.addDirichlet(0, val);

            }
            return b;
        });
        std::cout << "Solve start\n";
        ts->solve();
        std::string filename = "Results/data" + std::to_string(time);
        octree2VTU(baseDA, problemSize, {
                VecOutInfo(ts->getCurrentSolution(), ts->getDof(), {Attribute::scalar("u", 0)})
        }, filename.c_str(), false);
        write_merge_vtk("data"+std::to_string(time), size, time);
        VecCopy(ts->getCurrentSolution(), prev_solution);
	if(time <= 2){
		baseDA = refine(baseDA, problemSize, ndof, prev_solution, 0.01, talyEq->equation(),
               estimator_vec, num_err_quantities, estimator_file, &overallEstimator, false);
	}

    }











    // set boundary conditions - dirichlet w/ analytic solution on the walls

    // use default of 0.0 as initial guess (note that BC will be automatically applied to the guess during solve)




    PetscLogStagePop();
    PetscLogStagePush(postprocessStage);
    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "Out\n";



    PetscLogStagePop();

    delete ts;
    dendrite_finalize();
}
