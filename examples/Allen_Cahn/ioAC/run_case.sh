#!/usr/bin/env bash

# ******************************************************************
build_dir='../../../build/examples/Allen_Cahn'
fexec=${build_dir}/'ac'

# ******************************************************************
ADAP=1
REFINE_LVL=4
# ******************************************************************
# if [ -f $fexec ]; then
#    rm $fexec
# fi
# cp ${build_dir}/${fexec} .

# ******************************************************************
${fexec} -adap ${ADAP} -refine_lvl ${REFINE_LVL}> output_${REFINE_LVL}.txt 2>&1 & echo $! > process_${REFINE_LVL}.txt
