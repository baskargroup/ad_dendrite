#!/usr/bin/env bash

rm Results/*.vtu
rm Results/*.pvtu
rm Results/*.vtk
rm err*.txt
rm output*.txt
rm process*.txt
