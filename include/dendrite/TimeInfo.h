#pragma once

class TimeInfo {
 public:
  inline TimeInfo(double t, double dt) : currentstep((unsigned int) (t / dt)), current(t), step(dt) {}

  unsigned int currentstep;
  double current;
  double step;

  inline void increment() {
    currentstep++;
    current += step;
  }
};
