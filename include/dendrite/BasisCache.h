//
// Created by lofquist on 3/15/18.
//
#pragma once

#include <talyfem/grid/femelm.h>
#include <Point.h>
#include "Globals.h"

/**
 * Not thread-safe.
 */
class BasisCache {
protected:
    int relOrder;
    unsigned int bf ;
    unsigned int N_GAUSS_POINTS ;
    static const int MAX_LEVELS = 32;

public:
    BasisCache();
    virtual ~BasisCache();

    /**
     * Pre-calculate FEMElms for every element shape from level = 0..MAX_LEVELS at all gauss points 0..N_GAUSS_POINTS.
     * FEMElms are calculated for an element with its first node ("bottom-left corner") at 0, 0, 0.
     * fe.position() is adjusted on-the-fly in get().
     * @param problem_size used to calculate the octree coordinate -> physical scaling factors
     * @param max_depth used to calculate the octree coordinate -> physical scaling factors
     * @param grid TalyFEM grid to use to calculate FEMElms (can be temporary)
     * @param basis_flags whether or not to calculate second derivatives, etc.
     * @param rel_order use more or fewer gauss points, currently only 0 is supported (because N_GAUSS_POINTS is fixed)
     */
    void init(const PetscScalar* problem_size, unsigned int max_depth, TALYFEMLIB::GRID* grid,
              unsigned int basis_flags = TALYFEMLIB::BASIS_ALL, int rel_order = 0);

    /**
     * @param level level of octant, typically da->getLevel(da->curr())
     * @param gp_idx gauss point index, 0 <= gp_idx < n_itg_pts()
     * @param offset offset in physical space to add to fe.position(), typically da->getCurrentOffset() scaled by oct2phys
     * @return FEMElm for the shape at the given level and gauss point, with position adjusted by offset
     */
    const TALYFEMLIB::FEMElm* get(unsigned char level, unsigned int gp_idx, const TALYFEMLIB::ZEROPTV& offset);

   unsigned int n_itg_pts() {
        return N_GAUSS_POINTS;
    }

protected:
    PetscScalar oct_to_phys_[3];

    // pointers because FEMElm doesn't have a default constructor
    std::vector<std::vector<TALYFEMLIB::FEMElm*> > cache_;
//          TALYFEMLIB::FEMElm  * cache_[MAX_LEVELS][N_GAUSS_POINTS];
    std::vector<std::vector<TALYFEMLIB::ZEROPTV> > positions_;
    // pointers because FEMElm doesn't have a default constructor
//    TALYFEMLIB::FEMElm* cache_[MAX_LEVELS][N_GAUSS_POINTS];
//    TALYFEMLIB::ZEROPTV positions_[MAX_LEVELS][N_GAUSS_POINTS];
};