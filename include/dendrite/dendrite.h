#pragma once

#include "dendro.h"

/**
 * Initializes everything needed to use Dendrite, including MPI and PETSc.
 */
void dendrite_init(int argc, char** argv);

/**
 * Call at shutdown to ensure all memory is freed.
 * Automatically calls PetscFinalize().
 */
void dendrite_finalize();
