#pragma once

#include "feVector.h"
#include "VecInfo.h"
#include "interp.h"  // for taly-dendro map

// TalyFEM includes
#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>
#include <talyfem/grid/elem.h>
#include <talyfem/grid/elem_types/elem3dhexahedral.h>
#include <talyfem/grid/femelm.h>
#include <talyfem/data_structures/zeroarray.h>
#include <talyfem/data_structures/zeromatrix.h>
#include <talyfem/utils/macros.h>  // for DEFINE_HAS_SIGNATURE

// used to detect if an equation requires surface integration
DEFINE_HAS_MEMBER(has_integrands4side_be, Integrands4side_be);

template <typename Equation, typename NodeData>
class TalyVector : public feVector<TalyVector<Equation, NodeData> > {
  typedef feVector<TalyVector<Equation, NodeData> > Parent;
 public:

  TalyVector(feVec::daType da, Equation *eq, TALYFEMLIB::GRID *grid, TALYFEMLIB::GridField<NodeData> *gf)
      : Parent(da), taly_eq_(eq), taly_grid_(grid), taly_gf_(gf),
        taly_fe_(grid, TALYFEMLIB::BASIS_ALL), out_temp_(NULL)
  {
    taly_elem_ = taly_grid_->elm_array_[0];
  }

  void setDof(unsigned int dof) override {
    Parent::setDof(dof);
    be_.redim(8 * m_uiDof);
  }

  inline bool initStencils() { return true; }

  inline Equation *getEq() {
    return taly_eq_;
  }

  inline bool ElementalAddVec(int i, int j, int k, PetscScalar ***in, double scale) {
    assert(false);
    return false;
  }

  inline bool ElementalAddVec(unsigned int elmID, PetscScalar *out, double scale) {

    const unsigned int n_nodes = 8;

    taly_elem_->set_elm_id(elmID);
    sync_.syncCoordsAndNodeData(taly_grid_, taly_gf_);

    // sum integrands over all gauss points, filling be_
    be_.fill(0.0);

    /*taly_fe_.refill(taly_elem_, TALYFEMLIB::BASIS_LINEAR, 0);
    while (taly_fe_.next_itg_pt()) {
      taly_eq_->Integrands_be(taly_fe_, be_);
    }*/
    for (unsigned int i = 0; i < basis_cache_.n_itg_pts(); i++) {
      const TALYFEMLIB::FEMElm* fe = basis_cache_.get(m_octDA->getLevel(m_octDA->curr()), i,
                                                      taly_grid_->GetNode(0)->location());
      taly_eq_->Integrands_be(*fe, be_);
    }
    // do surface integration
    surface_integration(taly_fe_, be_);

    // remap back to Dendro format (local_out -> node_data_temp)
    taly_to_dendro(out_temp_, be_.data(), sizeof(PetscScalar) * m_uiDof);

    // fill dendro vector (out)
    interp_local_to_global(out_temp_, out, m_octDA, m_uiDof);

    return true;
  }

  inline bool ElementalAddVec(int elmID, PetscScalar *in_local, PetscScalar *in_local_prev, PetscScalar *out_local,
                              PetscScalar *coords, double scale) {
    assert(false);
    return false;
  }

  bool preAddVec() {
    // set t and dt on equation
    if (m_time) {
      taly_eq_->set_t(m_time->current);
      taly_eq_->set_dt(m_time->step);
    }

    sync_.getBuffers(m_octDA, m_dLx, m_dLy, m_dLz);

    assert(out_temp_ == NULL);
    out_temp_ = new PetscScalar[8*m_uiDof];

    const double problemSize[3] { m_dLx, m_dLy, m_dLz };
    basis_cache_.init(problemSize, m_octDA->getMaxDepth(), taly_grid_);
    return true;
  }

  bool postAddVec() {
    sync_.releaseBuffers();

    delete[] out_temp_;
    out_temp_ = NULL;
    return true;
  }

  void setVectors(const std::vector<VecInfo>& vecs) {
    sync_.setVectors(vecs);
  }

  void setPlaceholder(VecPlaceholder p, Vec v) override {
    sync_.setPlaceholder(p, v);
  }

 protected:
  using Parent::m_uiDof;  // dof for the input vector
  using Parent::m_time;
  using Parent::m_octDA;
  using Parent::m_dLx;
  using Parent::m_dLy;
  using Parent::m_dLz;

  template <bool Exists = has_integrands4side_be<Equation>::value>
  typename std::enable_if<!Exists>::type surface_integration(TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZEROARRAY<double>& be) const {
    // do nothing, just need to provide an implementation
  }

  template <bool Exists = has_integrands4side_be<Equation>::value>
  typename std::enable_if<Exists>::type surface_integration(TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZEROARRAY<double>& be) const {
    static const int n_nodes = 8;

    // determines which side(s) of the domain p lies on (may be multiple at corners)
    const auto get_surf_flags = [&] (const TALYFEMLIB::ZEROPTV& p) -> unsigned int {
      static const double tol = 1e-8;
      unsigned int flags = 0;
      if (p.x() < tol)
        flags |= (1u << 1u);
      if (fabs(p.x() - m_dLx) < tol)
        flags |= (1u << 2u);
      if (p.y() < tol)
        flags |= (1u << 3u);
      if (fabs(p.y() - m_dLy) < tol)
        flags |= (1u << 4u);
      if (p.z() < tol)
        flags |= (1u << 5u);
      if (fabs(p.z() - m_dLz) < tol)
        flags |= (1u << 6u);
      return flags;
    };

    // these are all per-element-type/basis function constants that could be cached since we have a uniform mesh
    const int* surf_arr = taly_elem_->GetSurfaceCheckArray();
    const int surf_row_len = taly_elem_->GetSurfaceCheckArrayRowLength();
    const int nodes_per_surf = taly_elem_->GetNodesPerSurface();

    for (int i = 0; i < taly_elem_->GetSurfaceCount(); i++) {
      int surf_id = surf_arr[i * surf_row_len];  // id determines which nodes on the surface

      // note: loop assumes elements have 8 nodes and use an identity map, so we skip using elem->ElemToLocalNodeID
      unsigned int flags = ~0u;  // initialize to all true
      for (unsigned int n = 0; n < nodes_per_surf && flags != 0; n++) {
        int elem_node_id = surf_arr[i * surf_row_len + 1 + n];
        // remove any surface flags that all nodes do not have in common
        // skipping elem->ElemToLocalNodeID because identity map!
        flags = flags & get_surf_flags(taly_grid_->GetNode(elem_node_id)->location());
      }

      // loop over set surface flags
      if (flags != 0) {
        for (unsigned int side_idx = 1; side_idx <= 6; side_idx++) {
          if (flags & (1u << side_idx)) {
            // TODO set up basis cache for surfaces (should also cache normals...)
            TALYFEMLIB::SurfaceIndicator surf(surf_id);
            surf.set_normal(taly_elem_->CalculateNormal(taly_grid_, surf_id));
            fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_LINEAR, 0);

            // loop over surface gauss points
            while (fe.next_itg_pt()) {
              taly_eq_->Integrands4side_be(taly_fe_, side_idx, be);
            }
          }
        }
      }
    }
  }

 private:
  TALYFEMLIB::GRID *taly_grid_;
  TALYFEMLIB::GridField<NodeData> *taly_gf_;
  Equation *taly_eq_;

  TALYFEMLIB::ELEM *taly_elem_;
  TALYFEMLIB::FEMElm taly_fe_;
  TALYFEMLIB::ZEROARRAY<double> be_;

  PetscScalar* out_temp_;  // scratch space for reordering be to morton order

  TalyDendroSync sync_;
  BasisCache basis_cache_;
};
