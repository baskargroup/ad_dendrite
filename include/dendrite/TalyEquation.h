#pragma once

#include "TalyMat.h"
#include "TalyVec.h"

template <typename NodeData>
class TalyMesh {
 public:
  TalyMesh() {
    // 8 nodes, 1 element
    grid.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
      grid.node_array_[i] = new TALYFEMLIB::NODE();
    }

    TALYFEMLIB::ELEM *taly_elem = new TALYFEMLIB::ELEM3dHexahedral();
    grid.elm_array_[0] = taly_elem;

    int node_id_array[8] = {
        0, 1, 2, 3, 4, 5, 6, 7
    };
    taly_elem->redim(8, node_id_array);

    field.redimGrid(&grid);
    field.redimNodeData();
  }

  TALYFEMLIB::GRID grid;
  TALYFEMLIB::GridField<NodeData> field;
};

template <typename Equation, typename NodeData>
class TalyEquation {
 public:
  TalyMatrix<Equation, NodeData> *mat;
  TalyVector<Equation, NodeData> *vec;

  inline Equation* equation() { return &taly_eq_; }

  /**
   * Arguments past the first two are passed to the Equation constructor.
   * @param useOctree true if octDA, false for PETSc da
   * @param ndof number of degrees of freedom, automatically passed to mat & vec with setDof
   * @param args passed to Equation constructor
   */
  template <typename... Args>
  TalyEquation(bool useOctree, int ndof, Args... args)
      : taly_eq_(args...) {
    mesh_ = new TalyMesh<NodeData>();
    owned_mesh_ = true;

    taly_eq_.p_grid_ = grid();
    taly_eq_.p_data_ = field();

    mat = new TalyMatrix<Equation, NodeData>(useOctree ? feMat::OCT : feMat::PETSC, &taly_eq_, grid(), field());
    mat->setDof(ndof);
    vec = new TalyVector<Equation, NodeData>(useOctree ? feVec::OCT : feVec::PETSC, &taly_eq_, grid(), field());
    vec->setDof(ndof);



  }

  /**
   * Same as the other constructor, but using an existing TalyMesh instead of creating a new one.
   * Used for coupled equations.
   * @param mesh Grid/GF to use for mat/vec - not managed by the TalyEquation (i.e. it is up to you to delete it).
   * @param useOctree true if octDA, false for PETSc da
   * @param ndof number of degrees of freedom, automatically passed to mat & vec with setDof
   * @param args passed to Equation constructor
   */
  template <typename... Args>
  TalyEquation(TalyMesh<NodeData>* mesh, bool useOctree, int ndof, Args... args)
      : taly_eq_(args...) {
    mesh_ = mesh;
    owned_mesh_ = false;

    taly_eq_.p_grid_ = grid();
    taly_eq_.p_data_ = field();

    mat = new TalyMatrix<Equation, NodeData>(useOctree ? feMat::OCT : feMat::PETSC, &taly_eq_, grid(), field());
    mat->setDof(ndof);
    vec = new TalyVector<Equation, NodeData>(useOctree ? feVec::OCT : feVec::PETSC, &taly_eq_, grid(), field());
    vec->setDof(ndof);
  }

  virtual ~TalyEquation() {
    delete mat;
    delete vec;

    if (owned_mesh_)
      delete mesh_;
  }

  TALYFEMLIB::GRID* grid() {
    return &mesh_->grid;
  }

  TALYFEMLIB::GridField<NodeData>* field() {
    return &mesh_->field;
  }

 protected:
  TalyMesh<NodeData>* mesh_;
  bool owned_mesh_;

  Equation taly_eq_;
};
