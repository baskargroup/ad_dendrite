//
// Created by maksbh on 11/19/18.
//
#pragma  once
//#ifndef DENDRITE_GLOBALS_H
//#define DENDRITE_GLOBALS_H

class Globals{
    int relOrder_;
    unsigned int bf_;
    unsigned int nsd_;
    bool set_;
public:
    Globals();
    void setGlobals();
    const int getrelOrder();
    const unsigned int getBasisFunction();
    const unsigned int nsd();
    void setrelOrder( const int relOrder);
    void setBasisFunction( const int bf);
    const unsigned int ndof();

};
//
//Globals::Globals(){
//    relOrder_ = 0;
//    bf_ = 1;
//    set = false;
//}
//const  int Globals::getrelOrder(){
//    return relOrder_;
//
//}

//const unsigned int Globals::getBasisFunction(){
//    return bf_;
//
//}
//
//void Globals::setBasisFunction( int relOrder){
//    relOrder_ = relOrder;
//
//}
//void Globals::setrelOrder( int bf){
//    bf_ = bf;
//}
//#endif //DENDRITE_GLOBALS_H
