#pragma once

#include <talyfem/grid/femelm.h>
#include "parUtils.h"
#include "octUtils.h"
#include "dendro.h"
#include "Globals.h"


void interp_global_to_local(PetscScalar *glo, PetscScalar * /* __restrict*/ loc, ot::DA *m_octDA, int dof);

void interp_local_to_global(PetscScalar * /*__restrict*/ loc, PetscScalar *glo, ot::DA *da, int dof);

void interp_local_to_global_matrix(PetscScalar *Ke, std::vector<ot::MatRecord> &out, ot::DA *da, int dof);

void interp_local_to_global_still_local(PetscScalar * /*__restrict*/ loc, PetscScalar *loc_new, ot::DA *da, int m_uiDof);

// morton -> taly ordering
void dendro_to_taly(void *dest, void *src, size_t bytes);

// taly -> morton ordering
void taly_to_dendro(void *dest, void *src, size_t bytes);

void build_taly_coordinates(double *coords /*out*/, const Point &pt, const Point &h);

// map from dendro order to taly order (node_data_temp_ -> NodeData);
constexpr int morton_to_taly_map[8] = {
  0, 1, 3, 2, 4, 5, 7, 6
};


void dendro_to_taly_surface(double *dest, double *src, size_t bytes, int surf_id);


void copy(double *dest, double *src, size_t bytes, const int * morton_to_taly_map) ;



