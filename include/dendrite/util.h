#pragma once

#include <petscmat.h>
#include <oda/oda.h>

int write_mat_matlab(Mat mat, const char *name, double ts);
int write_vec_matlab(Vec vec, const char *name, double ts);

void setScalarByFunction(ot::DA *da, const double *size, Vec vec, int dof,
                         const std::function<double(double, double, double, int)>& f);
int setScalarByFunction(DM da, int Ns, Vec vec, int dof,
                        const std::function<double(double, double, double, int)>& f);

/**
 * Converts an std::vector-based distributed Dendro vector (created by ot::DA::createVector)
 * to a PETSc Vec. This is used for testing to print temporary vectors to VTK files, as the
 * VTK code currently only works with PETSc vectors.
 */
template <typename T>
void std_to_petsc_vec(ot::DA* da, std::vector<T>& vec, unsigned int ndof, bool elemental,
                      bool vec_is_ghosted, bool out_is_ghosted, Vec* out) {
  da->createVector(*out, elemental, false, ndof);

  PetscScalar* out_buff = NULL;
  da->vecGetBuffer(*out, out_buff, elemental, out_is_ghosted, false, 1);

  T* tmp_from = NULL;
  da->vecGetBuffer(vec, tmp_from, elemental, vec_is_ghosted, true, 1);
  da->ReadFromGhostsBegin(tmp_from, ndof);
  da->ReadFromGhostsEnd(tmp_from);

  for (da->init<ot::DA_FLAGS::ALL>();
       da->curr() < da->end<ot::DA_FLAGS::ALL>();
       da->next<ot::DA_FLAGS::ALL>()) {
    for (unsigned int dof = 0; dof < ndof; dof++) {
      if (elemental) {
        size_t idx = da->curr() * ndof + dof;
        out_buff[idx] = (PetscScalar) tmp_from[idx];
      } else {
        unsigned int node_idxes[8];
        da->getNodeIndices(node_idxes);
        for (unsigned int n = 0; n < 8; n++) {
          size_t idx = node_idxes[n] * ndof + dof;
          out_buff[idx] = (PetscScalar) tmp_from[idx];
        }
      }
    }
  }
  da->vecRestoreBuffer(vec, tmp_from, elemental, vec_is_ghosted, true, ndof);
  da->vecRestoreBuffer(*out, out_buff, elemental, out_is_ghosted, false, ndof);
}

/**
 * Prints approximate work-per-process by several measures. Used for weak scaling studies.
 * @param da domain
 */
void printApproximateWork(ot::DA* da);

/**
 * Prints a message to stdout about the peak memory usage (in kb)
 * @param msg message to print with the number
 */
void printMemoryUsage(const std::string& msg);

/**
 * Sets vec[i]->node to a TreeNode calculated based on the physical coordinates in vec[i]->values.
 * This should be used to build TreeNodes from physical coordinates for use with alignPoints.
 * @param[inout] vec vec[i].values must be filled with the physical coordinates,
 *            vec[i].label is ignored (not used or changed), vec[i].nodes will be set by this function
 */
void fill_nodes_by_values(ot::DA* da, const double* problemSize, std::vector< ot::NodeAndValues<PetscScalar, 3> >& vec);
