#pragma once

#include <petscvec.h>

enum VecPlaceholder {
  PLACEHOLDER_NONE = 0,
  PLACEHOLDER_GUESS  // "guess" solution given by SNES (FormFunction)
};

struct VecInfo {
  Vec vec;
  unsigned int ndof;
  int nodeDataIndex;  // vec[i * ndof + dof] maps to GetNodeData(i).value(nodeDataIndex + dof)
  bool isReadOnly;
  VecPlaceholder placeholder;

  VecInfo() : vec(NULL), ndof(0), nodeDataIndex(-1), isReadOnly(true), placeholder(PLACEHOLDER_NONE) {}
  VecInfo(Vec v, unsigned int _ndof, int nodeDataIdx, bool readOnly)
      : vec(v), ndof(_ndof), nodeDataIndex(nodeDataIdx), isReadOnly(readOnly), placeholder(PLACEHOLDER_NONE) {}
  VecInfo(VecPlaceholder p, unsigned int _ndof, int nodeDataIdx, bool readOnly)
      : vec(NULL), ndof(_ndof), nodeDataIndex(nodeDataIdx), isReadOnly(readOnly), placeholder(p) {}
};