#pragma once

#include <sstream>

#include <petscksp.h>

#include <dendrite/solvers/Solver.h>
#include <dendrite/DendroIO.h>

class LinearSolver : public Solver
{
 public:
  virtual ~LinearSolver();

  PetscErrorCode init() override;
  PetscErrorCode solve() override;
  PetscErrorCode cleanup();

  // matrix-free shell operations
  PetscErrorCode jacobianMatMult(Vec In, Vec Out) override;
  // PetscErrorCode jacobianGetDiagonal(Vec diag) override { assert(false); }
  // PetscErrorCode mgjacobianMatMult(DM da, Vec In, Vec Out) override { assert(false); }

  inline void setMatrixFree(bool matrixFree) {
    m_matrixFree = matrixFree;
  }

 protected:
  KSP m_ksp = NULL;
  bool m_matrixFree = false;
};
