#pragma once

#include <sstream>

#include "Solver.h"
#include <dendrite/DendroIO.h>
#include <talyfem/utils/utils.h>
#include <dendrite/util.h>

using TALYFEMLIB::PrintStatus;

class NonlinearSolver : public Solver {
 public:
  ~NonlinearSolver() override;

  virtual void surfaceIntegralJac(Mat *jac, Vec sol) {}
  virtual void surfaceIntegralJacMfree(Vec mfree_in, Vec mfree_out, Vec sol) {}
  virtual void surfaceIntegralFuc(Vec in, Vec out) {}

  PetscErrorCode init() override;
  PetscErrorCode solve() override;
  virtual PetscErrorCode cleanup();

  PetscErrorCode jacobianMatMult(Vec in, Vec out) override;
  // PetscErrorCode jacobianGetDiagonal(Vec diag) override;
  // PetscErrorCode mgjacobianMatMult(DM da, Vec In, Vec Out) override;

  inline void setMatrixFree(bool matrixFree) {
    m_matrixFree = matrixFree;
  }

  inline SNES snes() {
    return m_snes;
  }

 protected:
  static PetscErrorCode FormJacobian(SNES snes, Vec sol, Mat jac, Mat precond_matrix, void *ctx);
  static PetscErrorCode FormFunction(SNES snes, Vec in, Vec out, void *ctx);

  SNES m_snes = NULL;
  Vec m_guess = NULL;  // last guess passed into FormJacobian
  bool m_matrixFree = false;
};

