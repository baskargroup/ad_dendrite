#pragma once

#include <vector>
#include <string>

#include "petscdmda.h"
#include "petscts.h"
#include <dendrite/feMat.h>
#include <dendrite/feVec.h>
#include <dendrite/TimeInfo.h>
#include <dendrite/BoundaryConditions.h>

class Solver {
 public:
  Solver();
  virtual ~Solver() {}

  /**
   * Initializes solver data structures (working vectors, SNES/KSP objects).
   * Should be called once after construction or a call to cleanup().
   * @return
   */
  virtual PetscErrorCode init() = 0;

  /**
   * Solves once (i.e., does not iterate for timestepping).
   * @return 0 on success, PETSc error code on error
   */
  virtual PetscErrorCode solve() = 0;

  inline PetscErrorCode solve_until(TimeInfo& ti, double end_time) {
    while (ti.current < end_time - 1e-16) {
      int ierr = solve(); CHKERRQ(ierr);
      ti.increment();
    }
    return 0;
  }

  void setMatrix(feMat *mat);
  void setVector(feVec *vec);

  /**
   * Specify a function that calculates boundary conditions at each node.
   * @param f This function takes four arguments: the x, y, and z coordinates of the node, and the (local) node ID
   *          and returns a Boundary object. If there are no boundary conditions on this node, you should
   *          return an "empty" Boundary object. If there is a dirichlet boundary, call boundary.addDirichlet(dof, val).
   *          NOTE: For nonlinear problems, val is actually the residual delta for this node
   *                (which should almost always be 0.0). TODO make it make sense for nonlinear problems
   */
  void setBoundaryCondition(const std::function<Boundary(double, double, double, unsigned int)> &f);

  inline void setDof(int dof) { m_uiDof = dof; }

  inline unsigned int getDof() const { return (unsigned int) m_uiDof; }

  /**
   * Guaranteed to always return the same vector after init() has been called.
   * @return current solution vector
   */
  inline Vec getCurrentSolution() const {
    assert(m_vecSolution != NULL);  // only valid after init()
    return m_vecSolution;
  }

  virtual PetscErrorCode jacobianMatMult(Vec _in, Vec _out) { throw std::runtime_error("Not implemented"); };

  virtual PetscErrorCode jacobianGetDiagonal(Vec diag) { throw std::runtime_error("Not implemented"); };

  virtual PetscErrorCode mgjacobianMatMult(DM _da, Vec _in, Vec _out) { throw std::runtime_error("Not implemented"); };

  // set the number of levels for multigrid
  inline void setLevels(int levels) {
    m_inlevels = levels;
  }

  /**
	*	@brief The Jacobian Matmult operation done a matrix free fashion
	*  @param _jac PETSC Matrix which is of shell type used in the time stepping
	*  @param _in  PETSC Vector which is the input vector
	*  @param _out PETSC Vector which is the output vector _jac*_in
	*  @return bool true if successful, false otherwise
	*
	*  See feMatrix.h for similar implementation
	**/
  static PetscErrorCode ShellMatMult(Mat M, Vec In, Vec Out) {
    Solver *contxt;
    MatShellGetContext(M, &contxt);
    return contxt->jacobianMatMult(In, Out);
  }

  static PetscErrorCode ShellMatGetDiagonal(Mat M, Vec diag) {
    Solver *contxt;
    MatShellGetContext(M, &contxt);
    return contxt->jacobianGetDiagonal(diag);
  }

/*  static PetscErrorCode MGMatMult(Mat M, Vec In, Vec Out){
	 stsDMMG *contxt;
	 MatShellGetContext(M,(void**)&contxt);
	 DM da = (DM)(((stsDMMG)contxt)->dm);
	 return ((Solver*)(((stsDMMG)contxt)->user))->mgjacobianMatMult(da,In,Out);
  }*/

  virtual void updateBoundaries(ot::DA *da);

  virtual void updateBoundaries(DM da);

  void setProblemSize(const double *size) {
    m_problemSize[0] = size[0];
    m_problemSize[1] = size[1];
    m_problemSize[2] = size[2];
  }

  inline const double* getProblemSize() const {
    return m_problemSize;
  }

  inline BoundaryConditions& getBC() {
    return boundary_conditions_;
  }

  inline void setDA(ot::DA* da, const double* problemSize) {
    assert(m_da == NULL);
    m_octDA = da;
    setProblemSize(problemSize);
  }

  inline void setDA(DM da, const double* problemSize) {
    assert(m_octDA == NULL);
    m_da = da;
    setProblemSize(problemSize);
  }

 protected:
  ot::DA* m_octDA;
  DM m_da;

  feMat *m_Mat;
  feVec *m_Vec;

  // Matrix
  Mat m_matJacobian;

  // Working vectors
  Vec m_vecRHS;
  Vec m_vecSolution;

  // stsDMMG Multigrid
  //stsDMMG      *m_dmmg;
  int m_inlevels;

  double m_problemSize[3];
  int m_uiDof;

  BoundaryConditions boundary_conditions_;

  // for generating dirichlet bc
  std::function<Boundary(double, double, double, unsigned int)> m_boundaryCondition;

};
