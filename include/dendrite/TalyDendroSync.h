#pragma once

#include <petscsys.h>
#include <talyfem/grid/grid_types/grid.h>
#include <oda/oda.h>
#include <talyfem/grid/gridfield.h>
#include "VecInfo.h"
#include "interp.h"
#include "PetscLogging.h"  // for profiling

class TalyDendroSync {
 public:
  TalyDendroSync();
  virtual ~TalyDendroSync();

  TalyDendroSync(const TalyDendroSync& rhs) = delete;
  TalyDendroSync& operator=(const TalyDendroSync& rhs) = delete;

  // setup
  void setVectors(const std::vector<VecInfo>& vecs);
  void setPlaceholder(VecPlaceholder p, Vec v);

  // use
  void getBuffers(ot::DA *octDA, double m_dLx, double m_dLy, double m_dLz);
  void releaseBuffers();

  template <typename T>
  void syncCoordsAndNodeData(TALYFEMLIB::GRID* grid, TALYFEMLIB::GridField<T>* gf) {
    PetscLogEventBegin(syncCoordsAndNodeDataEvent, 0, 0, 0, 0);
    assert(haveBuffers());
    Globals globals;
    const int n_nodes = globals.ndof();
    const unsigned int maxD = m_octDA->getMaxDepth();
    const int lev = m_octDA->getLevel(m_octDA->curr());

    // calculate element size and position of bottom left node
    Point h(octToPhysScale[0] * (1 << (maxD - lev)),
            octToPhysScale[1] * (1 << (maxD - lev)),
            octToPhysScale[2] * (1 << (maxD - lev)));

    Point pt = m_octDA->getCurrentOffset();
    pt.x() *= octToPhysScale[0];
    pt.y() *= octToPhysScale[1];
    pt.z() *= octToPhysScale[2];

    // build node coordinates (fill coords)
    double coords[n_nodes*3];
    build_taly_coordinates(coords, pt, h);

    // update node coordinates on the taly side
    for (unsigned int i = 0; i < n_nodes; i++) {
      grid->node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
    }

    // find the values at the nodes and update taly structures with them
    for (unsigned int v = 0; v < vecs_.size(); v++) {
      if (vec_buff_[v] == NULL)
        continue;

      // vec_buff[i] -> node_data_temp_
      interp_global_to_local(vec_buff_[v], node_data_temp_, m_octDA, vecs_[v].ndof);
      for (unsigned int i = 0; i < n_nodes; i++) {
        for (unsigned int dof = 0; dof < vecs_[v].ndof; dof++) {
          auto& talyNodeData = gf->GetNodeData(morton_to_taly_map[i]);
          talyNodeData.value(vecs_[v].nodeDataIndex + dof) = node_data_temp_[i * vecs_[v].ndof + dof];
        }
      }
    }

    PetscLogEventEnd(syncCoordsAndNodeDataEvent, 0, 0, 0, 0);
  }

  inline bool haveBuffers() const {
    return (vec_buff_ != NULL);
  }

 private:
  ot::DA* m_octDA;
  std::vector<VecInfo> vecs_;
  PetscScalar** vec_buff_;
  PetscScalar* node_data_temp_;  // scratch space for reordering node data
  PetscScalar octToPhysScale[3];
};
