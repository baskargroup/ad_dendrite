#pragma once

#include "feMatrix.h"
#include "VecInfo.h"
#include "interp.h"  // for taly-dendro map
#include "TalyDendroSync.h"
#include "PetscLogging.h"  // for profiling
#include "BasisCache.h"

// TalyFEM includes
#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>
#include <talyfem/grid/elem.h>
#include <talyfem/grid/elem_types/elem3dhexahedral.h>
#include <talyfem/grid/femelm.h>
#include <talyfem/data_structures/zeroarray.h>
#include <talyfem/data_structures/zeromatrix.h>
#include <talyfem/utils/macros.h>  // for DEFINE_HAS_SIGNATURE

// used to detect if an equation requires surface integration
DEFINE_HAS_MEMBER(has_integrands4side_ae, Integrands4side_Ae);

template <typename Equation, typename NodeData>
class TalyMatrix : public feMatrix<TalyMatrix<Equation, NodeData> > {
  typedef feMatrix<TalyMatrix<Equation, NodeData> > Parent;
 public:

  TalyMatrix(feMat::daType da, Equation *eq, TALYFEMLIB::GRID *grid, TALYFEMLIB::GridField<NodeData> *gf)
      : Parent(da), taly_eq_(eq), taly_grid_(grid), taly_gf_(gf),
        taly_fe_(grid, TALYFEMLIB::BASIS_DEFAULT),
        Ke_temp_(NULL), ae_column_(NULL), ae_column_interpolated_(NULL)
  {
    taly_elem_ = taly_grid_->elm_array_[0];
  }

  void setDof(unsigned int dof) override {
    Parent::setDof(dof);
    Ae_.redim(8 * m_uiDof, 8 * m_uiDof);
  }

  inline Equation *getEq() {
    return &taly_eq_;
  }

  inline bool GetElementalMatrix(int i, int j, int k, PetscScalar *mat) {
    assert(false);
  }

  inline bool GetElementalMatrix(unsigned int lclElmID, std::vector<ot::MatRecord> &records) {
    static const int n_nodes = 8;

    taly_elem_->set_elm_id(lclElmID);
    sync_.syncCoordsAndNodeData(taly_grid_, taly_gf_);

    // calculate values
    // sum integrands over all gauss points
    Ae_.fill(0.0);

    /*taly_fe_.refill(taly_elem_, TALYFEMLIB::BASIS_LINEAR, 0);
    while (taly_fe_.next_itg_pt()) {
      taly_eq_->Integrands_Ae(taly_fe_, Ae_);
    }*/
    for (unsigned int i = 0; i < basis_cache_.n_itg_pts(); i++) {
      const TALYFEMLIB::FEMElm* fe = basis_cache_.get(m_octDA->getLevel(m_octDA->curr()), i,
                                                      taly_grid_->GetNode(0)->location());
      taly_eq_->Integrands_Ae(*fe, Ae_);
    }

    // do surface integration (if Integrands4side_Ae exists)
    surface_integration(taly_fe_, Ae_);

    // remap back to Dendro order
    //PetscLogEventBegin(elemStiffnessMatReorderEvent, 0, 0, 0, 0);
    const int size = n_nodes * m_uiDof;
    for (int k = 0; k < size; k++) {
      for (int j = 0; j < size; j++) {
        Ke_temp_[size * k + j] = Ae_(k, j);
      }
    }

    for (int k = 0; k < n_nodes; k++) {
      for (int j = 0; j < n_nodes; j++) {
        for (int d1 = 0; d1 < m_uiDof; d1++) {
          for (int d2 = 0; d2 < m_uiDof; d2++)
            Ae_.data_ptr()[size * (k * m_uiDof + d1) + j * m_uiDof + d2] =
                Ke_temp_[size * (morton_to_taly_map[k] * m_uiDof + d1) + morton_to_taly_map[j] * m_uiDof + d2];
        }
      }
    }
    //PetscLogEventEnd(elemStiffnessMatReorderEvent, 0, 0, 0, 0);

    interp_local_to_global_matrix(Ae_.data_ptr(), records, m_octDA, m_uiDof);
    return true;
  }

  inline bool GetElementalMatrix(int elmID, PetscScalar *in_local, PetscScalar *in_local_prev, PetscScalar *coords,
                                 PetscScalar *mat) {
    assert(false);
    return false;
  }

  inline bool initStencils() { return true; }

  inline bool ElementalMatVec(int i, int j, int k, PetscScalar ***in, PetscScalar ***out, double scale) {
    assert(false);
    return false;
  }

  inline bool ElementalMatVec(unsigned int lclElmID, PetscScalar *in, PetscScalar *out, double scale) {
    static const int n_nodes = 8;

    taly_elem_->set_elm_id(lclElmID);
    sync_.syncCoordsAndNodeData(taly_grid_, taly_gf_);

    // calculate values
    // sum integrands over all gauss points
    Ae_.fill(0.0);

    /*taly_fe_.refill(taly_elem_, TALYFEMLIB::BASIS_LINEAR, 0);
    while (taly_fe_.next_itg_pt()) {
      taly_eq_->Integrands_Ae(taly_fe_, Ae_);
    }*/
    for (unsigned int i = 0; i < basis_cache_.n_itg_pts(); i++) {
      const TALYFEMLIB::FEMElm* fe = basis_cache_.get(m_octDA->getLevel(m_octDA->curr()), i,
                                                      taly_grid_->GetNode(0)->location());
      taly_eq_->Integrands_Ae(*fe, Ae_);
    }

    // do surface integration (if Integrands4side_Ae exists)
    surface_integration(taly_fe_, Ae_);

    // remap back to Dendro order
    //PetscLogEventBegin(elemStiffnessMatReorderEvent, 0, 0, 0, 0);
    const int size = n_nodes * m_uiDof;
    for (int k = 0; k < size; k++) {
      for (int j = 0; j < size; j++) {
        Ke_temp_[size * k + j] = Ae_(k, j);
      }
    }

    for (int k = 0; k < n_nodes; k++) {
      for (int j = 0; j < n_nodes; j++) {
        for (int d1 = 0; d1 < m_uiDof; d1++) {
          for (int d2 = 0; d2 < m_uiDof; d2++) {
            Ae_.data_ptr()[size * (k * m_uiDof + d1) + j * m_uiDof + d2] =
                Ke_temp_[size * (morton_to_taly_map[k] * m_uiDof + d1) + morton_to_taly_map[j] * m_uiDof + d2];
          }
        }
      }
    }
    //PetscLogEventEnd(elemStiffnessMatReorderEvent, 0, 0, 0, 0);

    unsigned int nodeIdxes[8];
    m_octDA->getNodeIndices(nodeIdxes);

    for (int k = 0; k < size; k++) {  // k = column
      for (int j = 0; j < size; j++) {  // j = row
        ae_column_[j] = Ae_(j, k);
        ae_column_interpolated_[j] = 0.0;
      }
      interp_local_to_global_still_local(ae_column_, ae_column_interpolated_, m_octDA, m_uiDof);

      for (int n = 0; n < 8; n++) {
        for (int d = 0; d < m_uiDof; d++) {
          const int row = nodeIdxes[n] * m_uiDof + d;
          const int col = nodeIdxes[k / m_uiDof] * m_uiDof + (k % m_uiDof);
          out[row] += ae_column_interpolated_[n * m_uiDof + d] * in[col];
        }
      }
    }

    return true;
  }

  inline bool ElementalMatVec(PetscScalar *in_local, PetscScalar *out_local, PetscScalar *coords, double scale) {
    assert(false);
    return false;
  }

  bool preMatVec() {
    // set t and dt on equation
    if (m_time) {
      taly_eq_->set_t(m_time->current);
      taly_eq_->set_dt(m_time->step);
    }

    sync_.getBuffers(m_octDA, m_dLx, m_dLy, m_dLz);

    assert(Ke_temp_ == NULL);
    assert(ae_column_ == NULL);
    assert(ae_column_interpolated_ == NULL);
    const unsigned int size = 8*m_uiDof ;
    Ke_temp_ = new PetscScalar[size*size];
    ae_column_ = new PetscScalar[size];
    ae_column_interpolated_ = new PetscScalar[size];

    const double problemSize[3] { m_dLx, m_dLy, m_dLz };
    // assert(false);
    basis_cache_.init(problemSize, m_octDA->getMaxDepth(), taly_grid_);
    return true;
  }

  bool postMatVec() {
    sync_.releaseBuffers();

    delete[] Ke_temp_;
    delete[] ae_column_;
    delete[] ae_column_interpolated_;
    Ke_temp_ = NULL;
    ae_column_ = NULL;
    ae_column_interpolated_ = NULL;
    return true;
  }

  void setVectors(const std::vector<VecInfo>& vecs) {
    sync_.setVectors(vecs);
  }

  void setPlaceholder(VecPlaceholder p, Vec v) override {
    sync_.setPlaceholder(p, v);
  }

 protected:
  using Parent::m_uiDof;  // dof for the input vector
  using Parent::m_time;
  using Parent::m_octDA;
  using Parent::m_dLx;
  using Parent::m_dLy;
  using Parent::m_dLz;

  template <bool Exists = has_integrands4side_ae<Equation>::value>
  typename std::enable_if<!Exists>::type surface_integration(TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae) const {
    // do nothing, just need to provide an implementation
  }

  template <bool Exists = has_integrands4side_ae<Equation>::value>
  typename std::enable_if<Exists>::type surface_integration(TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae) const {
    static const int n_nodes = 8;

    // determines which side(s) of the domain p lies on (may be multiple at corners)
    const auto get_surf_flags = [&] (const TALYFEMLIB::ZEROPTV& p) -> unsigned int {
      static const double tol = 1e-8;
      unsigned int flags = 0;
      if (p.x() < tol)
        flags |= (1u << 1u);
      if (fabs(p.x() - m_dLx) < tol)
        flags |= (1u << 2u);
      if (p.y() < tol)
        flags |= (1u << 3u);
      if (fabs(p.y() - m_dLy) < tol)
        flags |= (1u << 4u);
      if (p.z() < tol)
        flags |= (1u << 5u);
      if (fabs(p.z() - m_dLz) < tol)
        flags |= (1u << 6u);
      return flags;
    };

    // these are all per-element-type/basis function constants that could be cached since we have a uniform mesh
    const int* surf_arr = taly_elem_->GetSurfaceCheckArray();
    const int surf_row_len = taly_elem_->GetSurfaceCheckArrayRowLength();
    const int nodes_per_surf = taly_elem_->GetNodesPerSurface();

    for (int i = 0; i < taly_elem_->GetSurfaceCount(); i++) {
      int surf_id = surf_arr[i * surf_row_len];  // id determines which nodes on the surface

      // note: loop assumes elements have 8 nodes and use an identity map, so we skip using elem->ElemToLocalNodeID
      unsigned int flags = ~0u;  // initialize to all true
      for (unsigned int n = 0; n < nodes_per_surf && flags != 0; n++) {
        int elem_node_id = surf_arr[i * surf_row_len + 1 + n];
        // remove any surface flags that all nodes do not have in common
        // skipping elem->ElemToLocalNodeID because identity map!
        flags = flags & get_surf_flags(taly_grid_->GetNode(elem_node_id)->location());
      }

      // loop over set surface flags
      if (flags != 0) {
        for (unsigned int side_idx = 1; side_idx <= 6; side_idx++) {
          if (flags & (1u << side_idx)) {
            // TODO set up basis cache for surfaces (should also cache normals...)
            TALYFEMLIB::SurfaceIndicator surf(surf_id);
            surf.set_normal(taly_elem_->CalculateNormal(taly_grid_, surf_id));
            fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_LINEAR, 0);

            // loop over surface gauss points
            while (fe.next_itg_pt()) {
              taly_eq_->Integrands4side_Ae(taly_fe_, side_idx, Ae);
            }
          }
        }
      }
    }
  }

 private:
  TALYFEMLIB::GRID *taly_grid_;
  TALYFEMLIB::GridField<NodeData> *taly_gf_;
  Equation *taly_eq_;
  TALYFEMLIB::ELEM *taly_elem_;
  TALYFEMLIB::FEMElm taly_fe_;
  TALYFEMLIB::ZeroMatrix<double> Ae_;

  PetscScalar* Ke_temp_;  // scratch space for reordering Ae to morton order
  PetscScalar* ae_column_;  // scratch space for contiguous column of Ae (to use for interpolation)
  PetscScalar* ae_column_interpolated_;  // scratch space for local-to-global interpolated Ae column

  TalyDendroSync sync_;
  BasisCache basis_cache_;
};
