#pragma once

#include <vector>
#include <functional>
#include <talyfem/grid/femelm.h>
#include <oda/oda.h>


#define  LEFT 0
#define  RIGHT 1
#define  TOP 2
#define  BOTTOM 3
#define  FRONT 4
#define  BACK 5
#define  TEST 0

#define EQUALS(x, y) fabs((x) - (y)) < 1E-10 ? true : false


struct octInfo {
    double coords[24];
    double node_val[8];
    unsigned int curr_id;
    unsigned int neighCase = -1;

};

typedef std::function<double(double x, double y, double z)> AnalyticFunction;
//typedef std::function<int (TALYFEMLIB::FEMElm fe, double * local_taly, int lev)> RefineFunction;
typedef std::function<double(const TALYFEMLIB::FEMElm &fe, const double *ValueFEM,
                             const double *ValueDerivativeFEM)> ElementError;
typedef std::function<double(const TALYFEMLIB::FEMElm &fe, const TALYFEMLIB::FEMElm &fe_neigh, const double *ValueFEM,
                             const double *ValueFEM_neigh,
                             const double *ValueDerivativeFEM, const double *ValueDerivativeFEM_neigh,
                             const TALYFEMLIB::ZEROPTV &normal, const TALYFEMLIB::ZEROPTV &normal_neigh,
                             const int surf_id)> SurfaceError;
typedef std::function<unsigned int(double, double, int)> RefineFunction;


// void addAnalyticalSolution(ot::DA* da, const double* problemSize, Vec u_vec, Vec* output_vec_out, int ndof, double ts);

/**
 * Efficiently calculates L2 errors for all degrees of freedom in a vector (via integration w/ linear basis functions).
 * @param u vector to do comparisons with
 * @param ndof number of degrees of freedom in u
 * @param fs vector of analytic equation functions, where fs[i](x,y,z) returns the analytic solution for dof i at x/y/z
 *        if you don't want to supply an equation for some dof, just use nullptr for that entry (it will be skipped)
 *        skipped dofs will have an l2 error of 0.0 in the result
 * @return a vector result, where result[i] is the l2 error of dof i
 */
std::vector<double>
calcL2Errors(ot::DA *da, const double *problemSize, Vec u, int ndof, const std::vector<AnalyticFunction> &fs);

/**
 * Helper for calcL2Errors for single-dof solutions. Can still be used with a multi-dof vector.
 * @param u vector to compare with
 * @param ndof number of degrees of freedom in u
 * @param compareDof which degree of freedom f corresponds to (should be 0 for single-dof vectors)
 * @param f function of (double x, double y, double z) that returns the analytic solution at x/y/z
 * @return l2 error of dof compareDof versus f (integrated across all elements)
 */
double calcL2Error(ot::DA *da, const double *problemSize, Vec u, int ndof, int compareDof, const AnalyticFunction &f);
//void calcRefinementfunctionHeat(ot::DA* da, const double * problemSize, int ndof, Vec u,
//                                std::vector<unsigned int> & refinement, int initial_refinement, int solution_refinement,
//                                int boundary_refinement, double val, double eps,   const RefineFunction& f);

void
calcEnergy(ot::DA *da, const double *problemSize, int ndof, Vec u, double &free_energy, double &interfacial_energy);

void calcRefinementfunction(ot::DA *da, const double *problemSize, int ndof, Vec u,
                            std::vector<unsigned int> &refinement, int initial_refinement, int solution_refinement,
                            int boundary_refinement, double val, double eps);

void calcRefinementfunctionHeat(ot::DA *da, const double *problemSize, int ndof, Vec u,
                                std::vector<unsigned int> &refinement, const ElementError &f);

void get_surface(ot::DA *da, const double *problemSize, int ndof, Vec u);

void calc_error(ot::DA *da, const double *problemSize, int ndof, Vec u, std::vector<unsigned int> &refinement,
                const ElementError &fe_Error, const SurfaceError &surf_Error, const RefineFunction &fs,
                Vec estimator_vec, unsigned int num_err, double *overallEstimator, bool ifSurfErrCalc = false);

double integrate(ot::DA *da, const double *problemSize, Vec u);

void get_neighbours(ot::DA *da, double *problemSize);

void createGrid(const double *coords, TALYFEMLIB::GRID &grid, TALYFEMLIB::ELEM *elem);

void get_left(ot::DA *da, const double *problemSize, octInfo &oct, std::vector<std::vector<octInfo> > &neighbour_list,
              double *u_vec_data, unsigned int ndof);

void writeMatlabNeighbourList(octInfo &oct, std::vector<std::vector<octInfo> > &neighbour_list);

void interpolateNeighbours(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords, double *val,
                           unsigned int i, const unsigned int ndof);

void interpolateXFromLowerLevel(ot::DA *da, octInfo &neighbours, octInfo &curr_oct, double *coords, double *val,
                                unsigned int side, const unsigned int ndof);

void interpolateYFromLowerLevel(ot::DA *da, octInfo &neighbours, octInfo &curr_oct, double *coords, double *val,
                                unsigned int side, const unsigned int ndof);

void interpolateZFromLowerLevel(ot::DA *da, octInfo &neighbours, octInfo &curr_oct, double *coords, double *val,
                                unsigned int side, const unsigned int ndof);

void interpolateXFromHigherLevel(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords,
                                 double *val, unsigned int side, const unsigned int ndof);

void interpolateYFromHigherLevel(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords,
                                 double *val, unsigned int side, const unsigned int ndof);

void interpolateZFromHigherLevel(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords,
                                 double *val, unsigned int side, const unsigned int ndof);

void
valueFEM(TALYFEMLIB::FEMElm &fe, int ndof, double *value, double *val_c/*, std::vector<TALYFEMLIB::ZEROPTV> & coords*/);

void valueDerivativeFEM(TALYFEMLIB::FEMElm &fe, int ndof, const double *value, double **val_d);

#if TEST
bool isequal(TALYFEMLIB::ZEROPTV & coord1, TALYFEMLIB::ZEROPTV & coord2);
void check_left(TALYFEMLIB::FEMElm & fe_curr, TALYFEMLIB::FEMElm & fe_left, double * curr, double * left);
void check_right(TALYFEMLIB::FEMElm & fe_curr, TALYFEMLIB::FEMElm & fe_right, double * curr, double * right);
void check_top(TALYFEMLIB::FEMElm & fe_curr, TALYFEMLIB::FEMElm & fe_top, double * curr, double * top);
void check_bottom(TALYFEMLIB::FEMElm & fe_curr, TALYFEMLIB::FEMElm & fe_bottom, double * curr, double * bottom);

bool check_front(TALYFEMLIB::FEMElm & fe_curr, TALYFEMLIB::FEMElm & fe_front, double * curr, double * front);
void check_back(TALYFEMLIB::FEMElm & fe_curr, TALYFEMLIB::FEMElm & fe_back, double * curr, double * back);




#endif