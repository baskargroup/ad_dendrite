#pragma once

#ifdef PETSC_LOGGING_IMPL
#define Event int
#define Stage int
#else
#define Event extern int
#define Stage extern int
#endif

Stage createStage;
Stage firstSolveStage;
Stage solveStage;
Stage postprocessStage;

Event functionToDAEvent;
Event assembleVolMatNSEvent;
Event assembleSurfMatNSEvent;
Event assembleVolVecNSEvent;
Event assembleSurfVecNSEvent;
Event applyMatBCEvent;
Event applyVecBCEvent;
Event applyResidualVecBCEvent;
Event writeVTKEvent;
Event elemStiffnessMatReorderEvent;
Event syncCoordsAndNodeDataEvent;
Event remeshEvent;
Event solverInitEvent;
Event solverSolveEvent;
Event solverJacobianAssemblyEvent;
Event solverVectorAssemblyEvent;
Event totalEvent;

// ns-ibm-moving specific code (really should move this out...)
Event geoLoadEvent;

Event onBeforeSolveEvent;
Event updateBoundariesEvent;
Event onAfterSolveEvent;
Event inOutEvent;
Event findBackgroundElemEvent;
Event modifyInNodesEvent;
Event getGlobalInNodesEvent;

Event getFreshlyClearedEvent;
Event modifyFreshlyClearedEvent;
Event getGlobalFreshlyClearedEvent;
Event computeFreshlyClearedEvent;

Event ltsrSolveEvent;
Event ltsrCalcForceEvent;
Event ltsrComputeSurfaceValueEvent;
Event ltsrComputeSurfaceValueLocalEvent;

Event geoUpdateOverall;
Event geoUpdateVel;

Event getNodeCoordinatesEvent;
Event interpolateDataEvent;

void talydendro_register_events();

#ifdef PETSC_LOGGING_IMPL
void talydendro_register_events()
{
  PetscClassId cls;
  PetscClassIdRegister("dendrite", &cls);

  PetscLogStageRegister("create", &createStage);
  PetscLogStageRegister("firstSolve", &firstSolveStage);
  PetscLogStageRegister("solve", &solveStage);
  PetscLogStageRegister("postprocessStage", &postprocessStage);

  PetscLogEventRegister("functionToDA", cls, &functionToDAEvent);
  PetscLogEventRegister("assembleVolMatNS", cls, &assembleVolMatNSEvent);
  PetscLogEventRegister("assembleSurfMatNS", cls, &assembleSurfMatNSEvent);
  PetscLogEventRegister("assembleVolVecNS", cls, &assembleVolVecNSEvent);
  PetscLogEventRegister("assembleSurfVecNS", cls, &assembleSurfVecNSEvent);
  PetscLogEventRegister("applyMatBC", cls, &applyMatBCEvent);
  PetscLogEventRegister("applyVecBC", cls, &applyVecBCEvent);
  PetscLogEventRegister("applyResidualVecBC", cls, &applyResidualVecBCEvent);
  PetscLogEventRegister("writeVTK", cls, &writeVTKEvent);
  PetscLogEventRegister("elemStiffnessMatReorder", cls, &elemStiffnessMatReorderEvent);
  PetscLogEventRegister("syncElement", cls, &syncCoordsAndNodeDataEvent);
  PetscLogEventRegister("remesh", cls, &remeshEvent);
  PetscLogEventRegister("solverInit", cls, &solverInitEvent);
  PetscLogEventRegister("solverSolve", cls, &solverSolveEvent);
  PetscLogEventRegister("solverJacobianAssembly", cls, &solverJacobianAssemblyEvent);
  PetscLogEventRegister("solverVectorAssembly", cls, &solverVectorAssemblyEvent);
  PetscLogEventRegister("total", cls, &totalEvent);

  // ns-ibm-moving specific
  PetscLogEventRegister("geoLoadEvent", cls, &geoLoadEvent);

  PetscLogEventRegister("onBeforeSolve", cls, &onBeforeSolveEvent);
  PetscLogEventRegister("updateBoundaries", cls, &updateBoundariesEvent);
  PetscLogEventRegister("inOut", cls, &inOutEvent);
  PetscLogEventRegister("findBackgroundElem", cls, &findBackgroundElemEvent);
  PetscLogEventRegister("modifyInNodes", cls, &modifyInNodesEvent);
  PetscLogEventRegister("getGlobalInNodes", cls, &getGlobalInNodesEvent);

  PetscLogEventRegister("getFreshlyCleared", cls, &getFreshlyClearedEvent);
  PetscLogEventRegister("modifyFreshlyCleared", cls, &modifyFreshlyClearedEvent);
  PetscLogEventRegister("getGlobalFreshlyCleared", cls, &getGlobalFreshlyClearedEvent);
  PetscLogEventRegister("computeFreshlyCleared", cls, &computeFreshlyClearedEvent);

  PetscLogEventRegister("ltsrSolve", cls, &ltsrSolveEvent);
  PetscLogEventRegister("ltsrCalcForce", cls, &ltsrCalcForceEvent);
  PetscLogEventRegister("ltsrComputeSurfaceValue", cls, &ltsrComputeSurfaceValueEvent);
  PetscLogEventRegister("ltsrComputeSurfaceValueLocal", cls, &ltsrComputeSurfaceValueLocalEvent);

  PetscLogEventRegister("onAfterSolve", cls, &onAfterSolveEvent);
  PetscLogEventRegister("geoUpdateOverall", cls, &geoUpdateOverall);
  PetscLogEventRegister("geoUpdateVel", cls, &geoUpdateVel);

  PetscLogEventRegister("getNodeCoordinates", cls, &getNodeCoordinatesEvent);
  PetscLogEventRegister("interpolateData", cls, &interpolateDataEvent);
}
#endif

#undef Stage
#undef Event
