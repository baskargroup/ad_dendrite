#pragma once

#include <string>
#include <TreeNode.h>
#include <petscvec.h>
#include <oda/oda.h>

class TimeInfo;

class Checkpointer {
 public:
	/**
	 * How often calls to checkpoint() should actually write data.
	 * Default is 10 (every 10 calls, write a checkpoint file).
	 * @param freq how often to write checkpoint data
	 */
  inline void set_checkpoint_frequency(int freq) { frequency_ = freq; }

  /**
   * Number of backup checkpoint files to keep.
   * Backups are named as prefix.bak1, prefix.bak2, etc. (higher number = further into the past).
   * You should keep at least one backup, as checkpointing is not atomic (i.e. if you are
   * using no backups and checkpoint() call is interrupted, the old data has already been partially overwritten).
   * The default is 1.
   * @param n_backups number of backups to keep
   */
  inline void set_n_backups(int n_backups) { n_backups_ = n_backups; }
  inline void set_filename_prefix(const std::string& prefix) { file_prefix_ = prefix; }

  /**
   * Reload data from a checkpoint.
   * @param da_out new DA will be created here; this should point to either NULL or be left uninitialized
   * @param vecs_out vectors to load; these should be NULL or uninitialized, as they will be VecCreate'd by this
   *                 function
   * @param ti_out timeinfo (optional)
   */
  void load(ot::DA** da_out, std::vector<Vec*> vecs_out, TimeInfo* ti_out);

  /**
   * Save the octree shape and some accompanying data vectors.
   * Only actually writes checkpoint data every frequency calls.
   * @param tree_nodes from your DA constructor call
   *                   (NOTE: *DO NOT* call this function after the DA has been constructed;
   *                   the DA constructor modifies the treenodes in place!)
   * @param vecs data vectors (optional, may be empty)
   * @param ti time info to record (optional, may be NULL)
   */
  void checkpoint(const std::vector<ot::TreeNode>& tree_nodes, const std::vector<Vec>& vecs,
                  const TimeInfo* ti);

  /**
   * Write a PETSc Vec to a file. Unlike PETSc's built-in VecView routines, the per-process row counts are also
   * included in the file to ensure the structure exactly matches when loaded. This also means that this file can
   * only be read by the same number of MPI tasks that created it.
   * @param filename file to write to (will be overwritten if it exists)
   * @param vecs list of vecs to save
   * @param comm MPI communicator (usually PETSC_COMM_WORLD)
   */
	static void write_values(const std::string& filename, const std::vector<Vec>& vecs, MPI_Comm comm);

	/**
	 * Read vecs from a file (created by write_values).
	 * Note that the number of MPI tasks must exactly match what the file was written with. If it does not, this
	 * function throws an exception.
	 * @param filename file to read from
	 * @param vecs_out list of pointers to Vecs to load; these vectors will be automatically VecCreate'd by this routine,
	 *                 so they should be either NULL or uninitialized pointers
	 * @param comm MPI communicator (usually PETSC_COMM_WORLD)
	 */
	static void read_values(const std::string& filename, const std::vector<Vec*>& vecs_out, MPI_Comm comm);

 protected:
	static void write_cfg(const std::string& filename, const TimeInfo* ti, MPI_Comm comm);
	static void read_cfg(const std::string& filename, TimeInfo* ti, MPI_Comm comm);

  /**
   * Rename files for backups to ensure atomic updates.
   * Currently uses system("mv file1 file2") on rank 0.
   */
  void shift_backups();

  /**
   * Get the names of the files for backup i.
   * i = 0 means the "current" file set (i.e. not a backup).
   * @param i 0 for current ("checkpoint_*.dat"), 1+ for history ("checkpoint_*.dat.bak1"...)
   */
  std::vector<std::string> get_backup_names(int i) const;

  MPI_Comm comm_ = PETSC_COMM_WORLD;
  int n_backups_ = 1;
  int frequency_ = 10;
  int current_checkpoint_ = 0;
  std::string file_prefix_ = "checkpoint";
};

