#pragma once

#include <vector>
#include <string>

#include <oda.h>
#include <TreeNode.h>

struct Attribute {
  std::string name;
  std::string type;
  unsigned int start_idx;
  unsigned int len;

  static Attribute scalar(const std::string& attr_name, unsigned int dof);

  static Attribute vector(const std::string& attr_name, unsigned int start_dof);

  std::string vtk_header() const;

    std::string vtu_header() const;
};

struct VecOutInfo {
  Vec vec;
  std::vector<Attribute> attributes;
  unsigned int ndof;
  bool is_elemental;

  inline VecOutInfo(Vec data, unsigned int dof, const std::vector<Attribute>& attrs, bool elemental = false)
      : vec(data), attributes(attrs), ndof(dof), is_elemental(elemental) {}
};

/**
 * This is the most powerful VTK output function, capable of printing multiple vectors and typed data (scalars/vectors),
 * data associated with nodes, and data associated with elements (cells).
 *
 * Example usage:
 *
 *   octree2VTK(da, problemSize, {
 *     VecOutInfo(prev_sol_ns, ndof_ns, {
 *       Attribute::vector("velocity", 0), Attribute::scalar("pressure", 3)
 *     }),
 *     VecOutInfo(prev_sol_ch, ndof_ch, {
 *       Attribute::scalar("phi", 0), Attribute::scalar("mu", 1)
 *     })
 *   }, "my_data");
 *
 *  This will cause each process to write a file named "my_data_xxxxx.vtk", where xxxxx is the MPI process rank.
 *  Each file will contain 4 nodal variables: velocity (a 3D vector made of DOFs 0, 1, and 2 in prev_sol_ns),
 *  pressure (a scalar from DOF 3 in prev_sol_ns), phi (scalar from DOF 0 in prev_sol_ch), and mu (DOF 1 in ch).
 *
 *  Any attributes that are not listed will not be saved (i.e. if you don't want pressure, just remove it
 *  from the VecOutInfo constructor).
 *
 *  Attribute names must not contain whitespace, as the VTK format does not allow it.
 *
 * @param da octree
 * @param problemSize length 3 array for problem size scale (e.g. {1.0, 1.0, 1.0})
 * @param out output vector info, as described above
 * @param file_prefix prefix for the file (files are written as [file_prefix]_xxxxx.vtk, where xxxxx is the process ID)
 */
void octree2VTK(ot::DA *da, const double* problemSize, const std::vector<VecOutInfo>& out,
                const std::string& file_prefix, bool binary = true);

// legacy
inline void octree2VTK(ot::DA *da, const double* problemSize, Vec u, unsigned int ndof, bool is_elemental,
                       const std::string& file_prefix) {
  // name each dof "data0", "data1", etc
  std::vector<Attribute> attrs(ndof);
  for (unsigned int i = 0; i < ndof; i++) {
    attrs[i] = Attribute::scalar("data" + std::to_string(i), i);
  }

  octree2VTK(da, problemSize, {VecOutInfo(u, ndof, attrs, is_elemental)}, file_prefix);
}

// short for is_elemental = false
inline void octree2VTK(ot::DA *da, Vec u, unsigned int ndof, const double* problemSize, const std::string& file_prefix) {
  octree2VTK(da, problemSize, u, ndof, false, file_prefix);
}

void octree2VTU(ot::DA *da, const double* problemSize, const std::vector<VecOutInfo>& out,
                const std::string& file_prefix, bool binary = true);

// legacy
inline void octree2VTU(ot::DA *da, const double* problemSize, Vec u, unsigned int ndof, bool is_elemental,
                       const std::string& file_prefix) {
    // name each dof "data0", "data1", etc
    std::vector<Attribute> attrs(ndof);
    for (unsigned int i = 0; i < ndof; i++) {
        attrs[i] = Attribute::scalar("data" + std::to_string(i), i);
    }

    octree2VTU(da, problemSize, {VecOutInfo(u, ndof, attrs, is_elemental)}, file_prefix);
}

// short for is_elemental = false
inline void octree2VTU(ot::DA *da, Vec u, unsigned int ndof, const double* problemSize, const std::string& file_prefix) {
    octree2VTK(da, problemSize, u, ndof, false, file_prefix);
}


// for checkpointing
void write_octree(const std::string& filename, const std::vector<ot::TreeNode>& nodes, MPI_Comm comm);
std::vector<ot::TreeNode> read_octree(const std::string& filename, MPI_Comm comm);