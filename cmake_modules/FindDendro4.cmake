find_path(DENDRO4_BUILD_DIR  # dendro.h is a generated file, usually in build folder
	NAMES dendro.h
	HINTS ${DENDRO_DIR} $ENV{DENDRO_DIR}
	PATH_SUFFIXES include build cmake-build-debug cmake-build-release
)
find_path(DENDRO4_INCLUDE_DIR
	NAMES Point.h
	HINTS ${DENDRO_DIR} $ENV{DENDRO_DIR}
	PATH_SUFFIXES include
)

find_library(DENDRO4_LIBRARY
	NAMES dendro
	HINTS ${DENDRO_DIR} $ENV{DENDRO_DIR}
	PATH_SUFFIXES lib build cmake-build-debug cmake-build-release
)

find_library(DENDRO4_DA_LIBRARY
	NAMES dendroDA
	HINTS ${DENDRO_DIR} $ENV{DENDRO_DIR}
	PATH_SUFFIXES lib build cmake-build-debug cmake-build-release
)

find_library(DENDRO4_MG_LIBRARY
	NAMES dendroMG
	HINTS ${DENDRO_DIR} $ENV{DENDRO_DIR}
	PATH_SUFFIXES lib build cmake-build-debug cmake-build-release
)

set(DENDRO4_INCLUDES "${DENDRO4_INCLUDE_DIR}" "${DENDRO4_INCLUDE_DIR}/fem" "${DENDRO4_INCLUDE_DIR}/oda" "${DENDRO4_INCLUDE_DIR}/omg" "${DENDRO4_BUILD_DIR}")
set(DENDRO4_LIBRARIES "${DENDRO4_DA_LIBRARY}" "${DENDRO4_LIBRARY}" "${DENDRO4_MG_LIBRARY}")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Dendro4
  REQUIRED_VARS DENDRO4_INCLUDES DENDRO4_LIBRARIES DENDRO4_INCLUDE_DIR DENDRO4_BUILD_DIR DENDRO4_LIBRARY
)
