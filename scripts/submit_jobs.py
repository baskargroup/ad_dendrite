import os
import subprocess

def submit_jobs(campaign_dir, submit_cmd='sbatch'):
    orig_cwd = os.getcwd()
    job_dirs = [os.path.join(campaign_dir, x) for x in os.listdir(campaign_dir)]
    for idx, job_dir in enumerate(job_dirs):
        if os.path.exists(os.path.join(job_dir, '.submitted')):
            print("Skipping '{}', as it was previously submitted.".format(job_dir))
            continue

        print("Submitting {}... ({} / {})".format(job_dir, idx + 1, len(job_dirs)))
        os.chdir(job_dir)

        with open('.submitted', 'w') as f:
            subprocess.check_call([submit_cmd, 'job.sh'], stdout=f, stderr=subprocess.STDOUT)

        # open('.submitted', 'a').close()
        os.chdir(orig_cwd)
