import os
import pickle


def get_results(campaign_dir, gatherers):
    results = []

    job_dirs = [os.path.join(campaign_dir, x) for x in os.listdir(campaign_dir)]
    for job_dir in job_dirs:
        casefile_dir = os.path.join(job_dir, 'case.pkl')
        if not os.path.exists(casefile_dir):
            print("Skipping '{}', as it does not contain a case.pkl file.".format(job_dir))
            continue

        case = None
        with open(casefile_dir, 'rb') as f:
            case = pickle.load(f)

        result = {'case': case}
        for gatherer in gatherers:
            result = {**result, **gatherer(case, job_dir)}

        results.append(result)

    return results
