import os
import re


def PetscResults(petsc_log_name, labels=[]):
    def gather_petsc(case, job_dir):
        result = {}
        petsc_log_path = os.path.join(job_dir, petsc_log_name)
        if os.path.exists(petsc_log_path):
            reducedLogPath = petsc_log_path + '.reduced'

            # simplify log file so it can import in a reasonable amount of time
            if not os.path.exists(reducedLogPath):
                ignorePattern = re.compile(r'"count" : 0')
                with open(petsc_log_path, 'r') as f:
                    with open(reducedLogPath, 'w') as reducedLog:
                        for line in f:
                            if not ignorePattern.search(line):
                                reducedLog.write(line)

            import imp
            petsclog = imp.load_source('petsclog', reducedLogPath)

            # from petsclog import Stages, numProcs, LocalTimes
            Stages = petsclog.Stages
            # numProcs = petsclog.numProcs
            # LocalTimes = petsclog.LocalTimes

            times = {}
            stage = Stages['solve']
            for eventName, event in stage.items():
                if eventName in labels or len(labels) == 0:
                    avg_event_times = [v['time'] for v in event.values()]
                    if any(avg_event_times):
                        avg_event_time = sum(avg_event_times) / len(avg_event_times)
                        assert eventName not in times
                        times[eventName] = avg_event_time
            result['times'] = times
        return result

    return gather_petsc
