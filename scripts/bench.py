import sys
from typing import List

if sys.version_info[0] < 3:
    print("This script must be run with Python 3.")
    sys.exit(1)

from gen_folders import gen_folders, get_varied_keys
from submit_jobs import submit_jobs
from get_results import get_results

from file_generator import FileGenerator, FileCopyGenerator, FileLinkGenerator
from gather_petsc import PetscResults

import argparse
import json
import csv
import shutil

try:
    import matplotlib.pyplot as plt
    import numpy as np
except ImportError:
    plt = None


class Benchmark:
    def __init__(self, campaign_name: str):
        self.campaign_name = campaign_name
        self.cases = []
        self.generators = []
        self.gatherers = []
        self.param_output_blacklist = ['runner', 'executable', 'job_script']

    def add_cases(self, base_cfg, cases):
        if len(cases) == 0:
            cases = [{}]

        for case in cases:
            case = {**base_cfg, **case}
            for key, val in case.items():
                if callable(val):
                    case[key] = val(case)
            self.cases.append(case)

    def add_file_generator(self, filename: str, template: str):
        self.generators.append(FileGenerator(filename, template))

    def add_file_copy(self, filename: str, src: str):
        self.generators.append(FileCopyGenerator(filename, src))

    def add_file_link(self, filename: str, src: str):
        self.generators.append(FileLinkGenerator(filename, src))

    def add_result_gatherer(self, gatherer):
        self.gatherers.append(gatherer)

    def add_petsc_parser(self, petsc_log_name: str, labels: List[str] = list()):
        self.add_result_gatherer(PetscResults(petsc_log_name, labels))

    def run_main(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--generate', action='store_true', default=False, help="Generate campaign directory")
        parser.add_argument('--submit', action='store_true', default=False, help="Submit jobs to the batch scheduler.")
        parser.add_argument('--full-cases', action='store_true', default=False, help="Include full case data, not just varied keys in results.")
        parser.add_argument('--keys', type=str, nargs='+', help="List of keys to include in results (NOTE: currently only applies to --results-csv output).")
        parser.add_argument('--results-json', type=argparse.FileType('w'), help="Gather result data into a JSON file. Use - to write to stdout.")
        parser.add_argument('--results-csv', type=argparse.FileType('w'), help="Gather result data into a CSV file. Use - to write to stdout.")
        parser.add_argument('--runner', type=str, default='mpirun', help="Specify an alternative to mpirun.")
        parser.add_argument('--tasks-per-node', type=int, help="Specify the number of MPI tasks to use per node (default 16).")
        parser.add_argument('--results-graph', type=str, nargs=2, help="Create a line plot of some result values. Specify --results-graph [xkey] [ykey].")
        parser.add_argument('--results-scatter', type=str, nargs=2, help="Create a scatter plot of some result values. Specify [xkey] [ykey].")
        parser.add_argument('--filter', type=str, nargs='+', help="Python expression to filter by for results-graph.")
        args = parser.parse_args()

        if len(sys.argv) < 2:
            parser.print_help()
            return

        # override runner in base cfg
        param_overrides = {}
        if shutil.which(args.runner) is None:
            print("Warning: runner '{}' does not seem to exist.".format(args.runner))
        param_overrides['runner'] = args.runner

        if args.tasks_per_node:
            param_overrides['tasks_per_node'] = args.tasks_per_node

        # apply overrides to cases
        warned = set()
        for case in self.cases:
            for key, val in param_overrides.items():
                if key not in warned and key in case:
                    print("Warning: overriding case key '{}' with value '{}'.".format(key, val))
                    warned.add(key)
                case[key] = val

        if args.generate:
            gen_folders(self.campaign_name, self.cases, self.generators)

        if args.submit:
          submit_jobs(self.campaign_name)

        results_raw = None
        results = None
        results_varied = None
        need_results = args.results_json or args.results_graph or args.results_csv or args.results_scatter
        if need_results:
            results_raw = get_results(self.campaign_name, self.gatherers)

            # filter out keys in param_output_blacklist
            results = [dict(x) for x in results_raw]
            for result in results:
                result['case'] = dict(filter(lambda p: p[0] not in self.param_output_blacklist, result['case'].items()))

            # apply user filter
            if args.filter:
                condition_expr = ' '.join(args.filter) if args.filter else None
                def check_expr(r):
                    try:
                        return eval(condition_expr, globals(), {**r['case'], **r.get('errors', {}), **r.get('submission', {})})
                    except:
                        return None
                results = [r for r in results if check_expr(r)]

            # filter out non-varying keys
            varied_keys = get_varied_keys([x.get('case') for x in results])
            results_varied = [dict(x) for x in results]
            for result in results_varied:
                result['case'] = dict(filter(lambda p: p[0] in varied_keys, result['case'].items()))

        if not args.full_cases:
            results = results_varied

        if args.results_json:
            json.dump(results, args.results_json)

        if args.results_csv:
            w = csv.writer(args.results_csv)

            keys = set()
            for result in results:
                for category, data in result.items():
                    for key in data.keys():
                        if key not in self.param_output_blacklist and (args.keys is None or key in args.keys):
                            keys.add((category, key))
            keys = sorted(list(keys))

            # true if we need to include category to disambiguate keys, or if we can simplify to k[1]
            category_required = len(set([k[1] for k in keys])) != len(list([k[1] for k in keys]))

            # header row
            w.writerow([str(k[1]) if not category_required else str(k[0]) + '/' + str(k[1]) for k in keys])
            for result in results:
                row = []
                for key in keys:
                    val = result.get(key[0], {}).get(key[1], '-')
                    row.append(val)
                w.writerow(row)

        if args.results_graph or args.results_scatter:
            graph_variables = args.results_graph if args.results_graph else args.results_scatter
            xname = graph_variables[0]
            yname = graph_variables[1]
            xkey = xname.split('/')
            ykey = yname.split('/')
            if not len(xkey) == 2 or not len(ykey) == 2:
                print("Graph keys should be in the form of category/name, e.g. 'case/ntasks' or 'times/KSPSolve'.")
                sys.exit(1)

            assert len(xkey) == 2 and len(ykey) == 2

            #  gather data for plot
            xs = []
            ys = []
            for result in results:
                x = result.get(xkey[0], {}).get(xkey[1], None)
                y = result.get(ykey[0], {}).get(ykey[1], None)
                if x and y:
                    xs.append(x)
                    ys.append(y)

            assert len(xs) == len(ys)
            if len(xs) > 0:
                xs, ys = zip(*sorted(zip(xs, ys)))

                if args.results_graph:
                    plt.plot(xs, ys, marker='.')
                else:
                    plt.scatter(xs, ys, marker='.')
                    m, b = np.polyfit(xs, ys, 1)  # plot regression
                    plt.plot(xs, m * np.array(xs) + b, '--')
                plt.ylim(ymin=0)
                plt.xlabel(xkey[1])
                plt.ylabel(ykey[1] + (' (seconds)' if ykey[0] == 'times' else ''))
                plt.title(str(xkey[1]) + ' vs ' + str(ykey[1]))
                plt.show()
            else:
                print("Could not find any results for X = {}, Y = {}.".format(xname, yname))
