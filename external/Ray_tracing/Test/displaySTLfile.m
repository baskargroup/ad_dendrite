function varout = displaySTLfile(varargin)
% Parse the input

if isstruct(varargin{1}) 
    if ~all(isfield(varargin{1},{'vertices','faces'}))
        error( 'Structure FV must have "faces" and "vertices" fields' );
    end
    fv = varargin{1};
    varargin(1) = [];
end
if (~isempty(varargin))
    options = varargin{1};
    varargin(1) = [];
    ifDisplayNormal = options.showNormals;
else
    ifDisplayNormal = false;
end


if (ifDisplayNormal)
    facets = fv.vertices';
    facets = permute(reshape(facets(:,fv.faces'), 3, 3, []),[2 1 3]);
    edgeVecs = facets([2 3 1],:,:) - facets(:,:,:);
    allFacNorms = bsxfun(@times, edgeVecs(1,[2 3 1],:), edgeVecs(2,[3 1 2],:)) - ...
      bsxfun(@times, edgeVecs(2,[2 3 1],:), edgeVecs(1,[3 1 2],:));
    allFacNorms = bsxfun(@rdivide, allFacNorms, sqrt(sum(allFacNorms.^2,2)));
    facNorms = num2cell(squeeze(allFacNorms)',1);
    facCents = num2cell(squeeze(mean(facets,1))',1);
    facEdgeSize = mean(reshape(sqrt(sum(edgeVecs.^2,2)),[],1,1));
    
    patch(fv, 'FaceColor', 'g', 'FaceAlpha', 0.2);
    hold on;
    quiver3(facCents{:}, facNorms{:}, facEdgeSize * 20);
    % Add a camera light, and tone down the specular highlighting
    camlight('headlight');
    material('dull');
    % Fix the axes scaling, and set a nice view angle
    axis('image');
    view([45 45]);
    % view default
    xlabel('x')
    ylabel('y')
    zlabel('z')
%     close
else
    patch(fv,'FaceColor',       [0.8, 0.8, 1.0], ...
         'EdgeColor',       [0.1, 0.1, 0.1],        ...
         'FaceLighting',    'gouraud',     ...
         'AmbientStrength', 0.15);
    % Add a camera light, and tone down the specular highlighting
    camlight('headlight');
    material('dull');
    % Fix the axes scaling, and set a nice view angle
    axis('image');
    view([45 45]);
    % view default
    xlabel('x')
    ylabel('y')
    zlabel('z')
end