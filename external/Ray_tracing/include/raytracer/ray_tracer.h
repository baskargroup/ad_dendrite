//
// Created by boshun on 1/11/18.
//
#pragma once
#include <string>
#include <cmath>
#include "stl_reader.h"


/** Define namespace for return type of ifInside
 * */
namespace InsideType {
/**
 * Type < 0   -> error
 * Type == 0  -> outside
 * Type > 0   -> inside or on surface
 */
enum Type {
  UNKNOWN = -1,
  OUTSIDE = 0,
  INSIDE = 1,
  ON_SURFACE = 2,
};
}

/**
 * return type of line intersection
 */
namespace IntersectionType {
enum Type {
  INTERSECT = 0,
  PARALLEL = 1,
  ON_EDGE = 2,
  ON_SURFACE = 3,
  NO_INTERSECTION = 4,
  ON_STARTPOINT = 5,
};
}

class RayTracer {
 protected:
  inline static void Vec_Cross(double* dest, const double* v1, const double* v2) {
    dest[0] = v1[1] * v2[2] - v1[2] * v2[1];
    dest[1] = v1[2] * v2[0] - v1[0] * v2[2];
    dest[2] = v1[0] * v2[1] - v1[1] * v2[0];
  }

  inline static double Vec_Dot(const double* v1, const double* v2) {
    return (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]);
  }

  inline static void Vec_Sub(double *dest, const double *v1, const double *v2) {
    dest[0] = v1[0] - v2[0];
    dest[1] = v1[1] - v2[1];
    dest[2] = v1[2] - v2[2];
  }

  /**
   * Determine if a ray intersects with a sphere.
   * @param orig source point of ray
   * @param dir direction of ray (orig + dir = endpoint)
   * @param center center of sphere
   * @param radius radius of sphere
   * @return
   */
  static bool OutsideBoundingSphere(const double orig[3], const double dir[3],
                                    const double center[3], double radius);

  /**
   * Determine if a line intersects with a triangle surface.
   * @param orig source point of ray
   * @param dir scaled direction of ray (orig + dir = endpoint)
   * @param vert0 vertex 0 of triangle
   * @param vert1 vertex 1 of triangle
   * @param vert2 vertex 2 of triangle
   * @return type of intersection
   */
  static IntersectionType::Type ifLineIntersectTriangle(const double orig[3],
                              const double dir[3],
                              const double vert0[3],
                              const double vert1[3],
                              const double vert2[3]);

  /**
   * Determine if a point lies within an axis-aligned bounding box.
   * @param ray_end point to test
   * @param bounds bounding box
   * @return true if outside, false if inside
   */
  static bool ifOutsideBoundingBox(const double* point, const Box3d& bounds);

 public:
  struct TriangleData {
    Vector3d v[3];
    Sphere boundingSphere;
  };

  void setTriangles(std::vector<stl::Triangle>& triangles);
  void setTriangles(std::vector<TriangleData>&& triangles);
  InsideType::Type ifInside(const double point[3]) const;

  // note: ray_start must be inside the geometry
  InsideType::Type ifInside(const double ray_start[3], const double ray_end[3]) const;

  /**
   * Generates a random ray endpoint guaranteed to traverse bounding box (?)
   * @param pick affects which direction the ray is aimed
   * @param[out] result double[3] the result ray will be written to
   */
  void generateRay(int pick, double* result);

  inline const std::vector<TriangleData>& triangles() const {
    return triangles_;
  }

  inline const Box3d& bounds() const {
    return bounds_;
  }

 protected:
  std::vector<TriangleData> triangles_;

  // filled in by setTriangles
  Box3d bounds_;
  Vector3d ray_starts_[8];

  static Box3d calcBoundingBox(const std::vector<TriangleData>& triangles);
};
