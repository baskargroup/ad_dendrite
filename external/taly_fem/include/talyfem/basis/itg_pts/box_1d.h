/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#pragma once


#include <talyfem/grid/zeroptv.h>
#include <talyfem/data_structures/constexpr_array.h>
#include <talyfem/basis/constants.h>

namespace TALYFEMLIB {

// Gauss point source (Aug 2016):
// https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature

// sqrt() cannot be evaluated at compile-time, because it is not constexpr.
// So, these constants are calculated by hand elsewhere.
// The analytic expressions are included in comments where appropriate.

/**
 * 1D gauss points for N = 1 (1 point).
 * Source: https://en.wikipedia.org/wiki/Gaussian_quadrature
 */
template<>
struct BoxItgPts<1, 1, 0> {
  static constexpr int n_itg_pts = 1;  ///< number of integration points
  ///! integration points
  static constexpr ZEROPTV itg_pts[1] = { ZEROPTV(0.0, 0, 0) };
  ///! integration point weights
  static constexpr constexpr_array<double, 1> weights = {{ 2.0 }};
};

/**
 * 1D gauss points for N = 2 (2 points).
 * Source: https://en.wikipedia.org/wiki/Gaussian_quadrature
 */
template<>
struct BoxItgPts<2, 1, 0> {
  static constexpr int n_itg_pts = 2;  ///< number of integration points
  ///! integration points
  static constexpr ZEROPTV itg_pts[2] = {
    ZEROPTV(-1.0 / 1.7320508075688772935, 0, 0),  // -(1.0 / sqrt(3.0))
    ZEROPTV(+1.0 / 1.7320508075688772935, 0, 0)   // +(1.0 / sqrt(3.0))
  };
  ///! integration point weights
  static constexpr constexpr_array<double, 2> weights = {{ 1, 1 }};
};

/**
 * 1D gauss points for N = 3 (3 points).
 * Source: https://en.wikipedia.org/wiki/Gaussian_quadrature
 */
template<>
struct BoxItgPts<3, 1, 0> {
  static constexpr int n_itg_pts = 3;  ///< number of integration points
  ///! integration points
  static constexpr ZEROPTV itg_pts[3] = {
    ZEROPTV(-0.77459666924148337703585, 0, 0),  // -(sqrt(3.0) / sqrt(5.0))
    ZEROPTV(0.0, 0, 0),                         // 0
    ZEROPTV(+0.77459666924148337703585, 0, 0)   // +(sqrt(3.0) / sqrt(5.0))
  };
  ///! integration point weights
  static constexpr constexpr_array<double, 3> weights = {{
    5.0 / 9.0,
    8.0 / 9.0,
    5.0 / 9.0
  }};
};

/**
 * 1D gauss points for N = 4 (4 points).
 * Source: https://en.wikipedia.org/wiki/Gaussian_quadrature
 */
template<>
struct BoxItgPts<4, 1, 0> {
  static constexpr int n_itg_pts = 4;  ///< number of integration points
  ///! integration points
  static constexpr ZEROPTV itg_pts[4] = {
    ZEROPTV(-0.86113631159405257522394, 0, 0),  // -(sqrt((3/7) + (2/7) * sqrt(6/5)))
    ZEROPTV(-0.33998104358485626480266, 0, 0),  // -(sqrt((3/7) - (2/7) * sqrt(6/5)))
    ZEROPTV(+0.33998104358485626480266, 0, 0),  // +(sqrt((3/7) - (2/7) * sqrt(6/5)))
    ZEROPTV(+0.86113631159405257522394, 0, 0)   // +(sqrt((3/7) + (2/7) * sqrt(6/5)))
  };
  ///! integration point weights
  static constexpr constexpr_array<double, 4> weights = {{
    0.347854845137453857373063,  // (18 - sqrt(30)) / 36
    0.652145154862546142626936,  // (18 + sqrt(30)) / 36
    0.652145154862546142626936,  // (18 + sqrt(30)) / 36
    0.347854845137453857373063   // (18 - sqrt(30)) / 36
  }};
};

/**
 * 1D gauss points for N = 5 (5 points).
 * Source: https://en.wikipedia.org/wiki/Gaussian_quadrature
 */
template<>
struct BoxItgPts<5, 1, 0> {
  static constexpr int n_itg_pts = 5;  ///< number of integration points
  ///! integration points
  static constexpr ZEROPTV itg_pts[5] = {
    ZEROPTV(-0.90617984593866399279762, 0, 0),  // -((1/3) * sqrt(5 + 2 * sqrt(10/7)))
    ZEROPTV(-0.53846931010568309103631, 0, 0),  // -((1/3) * sqrt(5 - 2 * sqrt(10/7)))
    ZEROPTV(0.0, 0, 0),                         // 0
    ZEROPTV(+0.53846931010568309103631, 0, 0),  // +((1/3) * sqrt(5 - 2 * sqrt(10/7)))
    ZEROPTV(+0.90617984593866399279762, 0, 0)   // +((1/3) * sqrt(5 + 2 * sqrt(10/7)))
  };
  ///! integration point weights
  static constexpr constexpr_array<double, 5> weights = {{
    0.236926885056189087514264040,  // (322 - 13 * sqrt(70)) / 900
    0.478628670499366468041291514,  // (322 + 13 * sqrt(70)) / 900
    128.0 / 225.0,                  // 128.0 / 225.0
    0.478628670499366468041291514,  // (322 + 13 * sqrt(70)) / 900
    0.236926885056189087514264040   // (322 - 13 * sqrt(70)) / 900
  }};
};

}  // namespace TALYFEMLIB
