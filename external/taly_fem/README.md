TalyFEM - Finite Element Method Framework 
=========================================

[![Build Status](https://baskar-group.me.iastate.edu/jenkins/buildStatus/icon?job=taly_fem)](https://baskar-group.me.iastate.edu/jenkins/job/taly_fem/)

This library provides a scalable framework for solving FEM problems using the PETSc
toolkit.

Features:

* Works in serial and parallel

* Easily split up big meshes across multiple processes (domain decomposition)

* Load custom meshes (from Tecplot, HDF5, and Gmsh)

* Save results as Tecplot files and HDF5 files (viewable in ParaView via XDMF)

* Support for Dirichlet, Neumann, and periodic boundary conditions

Dependencies
============

#### Required Dependencies

* A C++ compiler with C++11 support is required. We aim for compatibility with gcc, clang, and icc (Intel C++ compiler).

* [PETSc](https://www.mcs.anl.gov/petsc/) (3.6.x) with MPI (typically MPICH), ParMETIS, and optionally MUMPS (used in some tests).

* [libconfig](http://www.hyperrealm.com/oss_libconfig.shtml)

* [CMake](https://cmake.org/download/) 3.5 or above

* Python 2.7 (used in part of the build system)

#### Optional Dependencies

* [HDF5](https://support.hdfgroup.org/HDF5/release/obtainsrc518.html) (tested with 1.8.14), for saving HDF5/XDMF files
  (writes ~40% smaller files than Tecplot, viewable with ParaView)

* [PT-SCOTCH](https://www.labri.fr/perso/pelegrin/scotch/), for an alternative domain decomposition graph partitioning implementation
  (may work better than ParMETIS for very large problems)

* [MESQUITE](https://trilinos.org/oldsite/packages/mesquite/), an adapter is included for running MESQUITE mesh optimizations directly on TalyFEM's data structures

Building
========

See [docs/BUILDING.md](docs/BUILDING.md) for detailed instructions on how to build TalyFEM and its dependencies.

See [docs/CMAKE.md](docs/CMAKE.md) for a quick intro to CMake if you haven't used it before.

See [docs/RECIPES.md](docs/RECIPES.md) for recommended TalyFEM build recipes for different clusters and [docs/CLUSTER_QUIRKS](docs/CLUSTER_QUIRKS.md) for some information on different cluster quirks. If you discover any new problems or incorrect information, please update these!

Using the Library
=================

A tutorials folder is provided which gives examples of how to use the library.
The steadystate_heat tutorial is the simplest place to start.

Documentation is available online [here](https://baskar-group.me.iastate.edu/talydocs) (automatically published from the [taly_documentation](https://bitbucket.org/baskargroup/taly_documentation) repository). See the [Getting Started](https://baskar-group.me.iastate.edu/talydocs//tutorials/A_0_getting_started.html) tutorial for information on how to set up an "empty" project that links to TalyFEM. See the [Hello World](https://baskar-group.me.iastate.edu/talydocs//tutorials/A_1_talyfem_hello_world.html) for an example of the TalyFEM structure.

Development of the Library
==========================

If you are committing any changes to the library, it is very important to first
read the [style guide](docs/style.md) and ensure that your code meets those guidelines before
you commit it.

Contact
=======

If you find any bugs in the library, check the [BitBucket issue tracker](https://bitbucket.org/baskargroup/taly_fem/issues?status=new&status=open) to see if it has already been reported. If it hasn't, feel free to open a new issue.

If you have made any improvements to the library you would like to share, please
[open a BitBucket pull request](https://bitbucket.org/baskargroup/taly_fem/pull-request/new)!

Questions and comments should be sent to Baskar Ganapathysubramanian at [baskarg@iastate.edu](mailto:baskarg@iastate.edu).

```
Baskar Ganapathysubramanian
Department of Mechanical Engineering
Iowa State University
306 Lab of Mechanics
Ames, IA 50011
```
