#Dendrite with space time adaptivity

The following modules are needed to build:

* Petsc3.8.4;
* libconfig1.4.10;


Compilation instruction
```
cd <path to AD_Dendrite directory >;
cd build;
cmake ../; 
make -j 8;
```

The examples folder contain the space time solution with 
adaptivity for the following cases:

* A diffusive heat source.
* A pulse moving in a rotating flow field.
* Nonlinear Allen-Cahn equation

In order to run the examples:

```
cd examples/XXX/io*;
bash run_case.sh;
```