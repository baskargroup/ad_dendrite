//
// Created by maksbh on 11/19/18.
//

#include "dendrite/Globals.h"
#include <libconfig.h++>
#include <iostream>
#include <assert.h>
#include <math.h>

Globals::Globals() {
    bf_ = 1;
    relOrder_ = 0;
    set_ = false;
    nsd_ = 3;
    setGlobals();
}

const  int Globals::getrelOrder(){
    assert(set_ == true);
    return relOrder_;
}

void Globals::setGlobals() {

    if(set_ == false) {
        std::string config_file = "config.txt";
        libconfig::Config cfg;
        cfg.readFile(config_file.c_str());
        cfg.lookupValue("basisRelOrder", relOrder_);
        cfg.lookupValue("BasisFunction", bf_);
        cfg.lookupValue("nsd", nsd_);
        set_ = true;
    }


}
const unsigned int Globals::getBasisFunction(){
    assert(set_ == true);
    return bf_;
}

void Globals::setBasisFunction(const int bf){

    bf_ = bf;
    relOrder_ = 0;
}
void Globals::setrelOrder(const int relOrder) {
    relOrder_ = relOrder;
}

const unsigned int Globals::nsd() {
    assert(set_ == true);
    return nsd_;
}

const unsigned int Globals::ndof() {
    assert(set_ == true);
    return (pow((bf_ + 1),nsd_));
}


