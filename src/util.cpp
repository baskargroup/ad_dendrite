#include <dendrite/util.h>
#include <talyfem/utils/utils.h>

int write_mat_matlab(Mat mat, const char *name, double ts) {
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  std::stringstream ss;
  ss << name << "__size" << size << "_ts" << (int) ts << ".m";
  std::string path = ss.str();

  int ierr;
  PetscViewer viewer;
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, path.c_str(), &viewer);
  CHKERRQ(ierr);
  ierr = PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
  CHKERRQ(ierr);
  //PetscViewerView(viewer,PETSC_VIEWER_STDOUT_WORLD);
  ierr = MatView(mat, viewer);
  CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);
  CHKERRQ(ierr);

//  MatView(mat, PETSC_VIEWER_STDOUT_WORLD);
  return 0;
}

int write_vec_matlab(Vec vec, const char *name, double ts) {
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  std::stringstream ss;
  ss << name << "__size" << size << "_ts" << (int) ts << ".m";

  PetscViewer viewer;
  PetscViewerASCIIOpen(PETSC_COMM_WORLD, ss.str().c_str(), &viewer);
  PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
  VecView(vec, viewer);
  PetscViewerDestroy(&viewer);

  return 0;
}

// set data for PETSc DA
int setScalarByFunction(DM da, int Ns, Vec vec, int dof, const std::function<double(double, double, double, int)>& f) {
  int x, y, z, m, n, p;
  int mx, my, mz, xne, yne, zne;

  CHKERRQ(DMDAGetCorners(da, &x, &y, &z, &m, &n, &p));
  CHKERRQ(DMDAGetInfo(da, 0, &mx, &my, &mz, 0, 0, 0, 0, 0, 0, 0, 0, 0));

  if (x + m == mx) {
    xne = m - 1;
  } else {
    xne = m;
  }
  if (y + n == my) {
    yne = n - 1;
  } else {
    yne = n;
  }
  if (z + p == mz) {
    zne = p - 1;
  } else {
    zne = p;
  }

  double hx = 1.0 / ((double) Ns);

  // allocate for temporary buffers ...
  unsigned int elemSize = Ns * Ns * Ns;  // number of elements
  unsigned int nodeSize = (Ns + 1) * (Ns + 1) * (Ns + 1);  // number of nodes

  // Set Elemental material properties
  PetscScalar ***vec_data = NULL;
  CHKERRQ(DMDAVecGetArray(da, vec, &vec_data));

  // loop through all nodes ...
  for (int k = z; k < z + p; k++) {
    for (int j = y; j < y + n; j++) {
      for (int i = x; i < x + m; i++) {
        double coords[3] = {hx * i, hx * j, hx * k};
        for (int d = 0; d < dof; d++) {
          double val = f(coords[0], coords[1], coords[2], d);
          vec_data[k][j][i * dof + d] = val;
        }
      } // end i
    } // end j
  } // end k

  CHKERRQ(DMDAVecRestoreArray(da, vec, &vec_data));
  return 0;
}

void setScalarByFunction(ot::DA *da, const double *size, Vec vec, int dof,
                         const std::function<double(double, double, double, int)>& f) {

  PetscScalar *_vec = NULL;
  da->vecGetBuffer(vec, _vec, false, false, false, dof);

  unsigned int maxD = da->getMaxDepth();
  unsigned int lev;
  double hx, hy, hz;
  Point pt;

  double xFac = size[0] / ((double) (1 << (maxD - 1)));
  double yFac = size[1] / ((double) (1 << (maxD - 1)));
  double zFac = size[2] / ((double) (1 << (maxD - 1)));
  double xx[8], yy[8], zz[8];
  unsigned int idx[8];

  for (da->init<ot::DA_FLAGS::ALL>(); da->curr() < da->end<ot::DA_FLAGS::ALL>(); da->next<ot::DA_FLAGS::ALL>()) {
    // set the value
    lev = da->getLevel(da->curr());
    hx = xFac * (1u << (maxD - lev));
    hy = yFac * (1u << (maxD - lev));
    hz = zFac * (1u << (maxD - lev));

    pt = da->getCurrentOffset();

    //! get the correct coordinates of the nodes ...
    // unsigned int chNum = da->getChildNumber();
    unsigned char hangingMask = da->getHangingNodeIndex(da->curr());

    // if hanging, use parents, else mine.
    xx[0] = pt.x() * xFac;
    yy[0] = pt.y() * yFac;
    zz[0] = pt.z() * zFac;
    xx[1] = pt.x() * xFac + hx;
    yy[1] = pt.y() * yFac;
    zz[1] = pt.z() * zFac;
    xx[2] = pt.x() * xFac;
    yy[2] = pt.y() * yFac + hy;
    zz[2] = pt.z() * zFac;
    xx[3] = pt.x() * xFac + hx;
    yy[3] = pt.y() * yFac + hy;
    zz[3] = pt.z() * zFac;

    xx[4] = pt.x() * xFac;
    yy[4] = pt.y() * yFac;
    zz[4] = pt.z() * zFac + hz;
    xx[5] = pt.x() * xFac + hx;
    yy[5] = pt.y() * yFac;
    zz[5] = pt.z() * zFac + hz;
    xx[6] = pt.x() * xFac;
    yy[6] = pt.y() * yFac + hy;
    zz[6] = pt.z() * zFac + hz;
    xx[7] = pt.x() * xFac + hx;
    yy[7] = pt.y() * yFac + hy;
    zz[7] = pt.z() * zFac + hz;

    da->getNodeIndices(idx);
    for (unsigned int i = 0; i < 8; ++i) {
      if (!(hangingMask & (1u << i))) {
        for (int d = 0; d < dof; d++) {
          _vec[idx[i] * dof + d] = f(xx[i], yy[i], zz[i], d);
        }
      }
    }
  }

  da->vecRestoreBuffer(vec, _vec, false, false, false, dof);
}

void printApproximateWork(ot::DA* octDA) {
  using TALYFEMLIB::PrintInfo;

  MPI_Comm comm = octDA->getComm();

  int rank, nProcs;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nProcs);

  const int N_NODES = 0;
  const int N_GHOSTED_NODES = 1;
  const int N_ELEMENTS = 2;
  const int N_GHOSTED_ELEMENTS = 3;
  unsigned int counts[4];
  counts[N_NODES] = octDA->getNodeSize();
  counts[N_GHOSTED_NODES] = octDA->getGhostedNodeSize();
  counts[N_ELEMENTS] = octDA->getElementSize();
  counts[N_GHOSTED_ELEMENTS] = octDA->getGhostedElementSize();

  unsigned int mins[4], maxs[4], avgs[4];
  for (int i = 0; i < 4; i++) {
    MPI_Allreduce(&counts[i], &mins[i], 1, MPI_UNSIGNED, MPI_MIN, comm);
    MPI_Allreduce(&counts[i], &maxs[i], 1, MPI_UNSIGNED, MPI_MAX, comm);
    MPI_Allreduce(&counts[i], &avgs[i], 1, MPI_UNSIGNED, MPI_SUM, comm);
    avgs[i] /= nProcs;
  }
  if (!rank) {
    PrintInfo("N_NODES - min: ", mins[N_NODES], ", max: ", maxs[N_NODES], ", avg: ", avgs[N_NODES]);
    PrintInfo("N_GHOSTED_NODES - min: ", mins[N_GHOSTED_NODES], ", max: ", maxs[N_GHOSTED_NODES], ", avg: ", avgs[N_GHOSTED_NODES]);
    PrintInfo("N_ELEMENTS - min: ", mins[N_ELEMENTS], ", max: ", maxs[N_ELEMENTS], ", avg: ", avgs[N_ELEMENTS]);
    PrintInfo("N_GHOSTED_ELEMENTS - min: ", mins[N_GHOSTED_ELEMENTS], ", max: ", maxs[N_GHOSTED_ELEMENTS], ", avg: ", avgs[N_GHOSTED_ELEMENTS]);
  }
}

void printMemoryUsage(const std::string& msg) {
  MPI_Comm comm = PETSC_COMM_WORLD;

  int rank, nProcs;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &nProcs);

  PetscLogDouble myUsage, sumUsage, maxUsage, minUsage;
  PetscMemoryGetCurrentUsage(&myUsage);

  MPI_Reduce(&myUsage, &sumUsage, 1, MPI_DOUBLE, MPI_SUM, 0, comm);
  MPI_Reduce(&myUsage, &minUsage, 1, MPI_DOUBLE, MPI_MIN, 0, comm);
  MPI_Reduce(&myUsage, &maxUsage, 1, MPI_DOUBLE, MPI_MAX, 0, comm);

  if (!rank) {
    PetscLogDouble avgUsage = sumUsage / nProcs;
    std::cout << "[MEM] " << msg << " - avg: " << avgUsage << ", min: " << minUsage << ", " << "max: " << maxUsage << std::endl;
  }
}

void fill_nodes_by_values(ot::DA* da, const double* problemSize, std::vector<ot::NodeAndValues<PetscScalar, 3> >& vec) {
  const unsigned int maxLev = da->getMaxDepth();
  const unsigned int maxOctCoord = (1u << (maxLev - 1u));

  // calculate conversion factor for physical coordinates -> octree integer coordinates
  assert (problemSize != NULL);
  static const int nsd = 3;
  const double physToOct[nsd] = {
      (1u << (maxLev - 1)) / problemSize[0],
      (1u << (maxLev - 1)) / problemSize[1],
      (1u << (maxLev - 1)) / problemSize[2],
  };
  assert (physToOct[0] > 0.0 && physToOct[1] > 0.0 && physToOct[2] > 0.0);

  for (auto& n : vec) {
    unsigned int octCoords[3];
    for (unsigned int dim = 0; dim < nsd; dim++) {
      // octree coordinate
      octCoords[dim] = (unsigned int) (n.values[dim] * physToOct[dim]);

      // special fix for positive boundaries... (explanation from Hari, May 7, 2018):
      /*
       * Basically, if the point is on the boundary, then the interpolation needs to happen on the face that overlaps
       * with the boundary. This face can be part of 2 elements, one that is inside the domain and one that is outside.
       * By default we always go for the "right" element, but this is not correct for those on the positive boundaries,
       * hence the error. You can fix it when you compute the TreeNode corresponding to the element. You will need a
       * correction like:
       *
       *   if (xint == xyzFac)
       *     xint = xyzFac - 1;
       *   if (yint == xyzFac)
       *     yint = xyzFac -1;
       *   if (zint == xyzFac)
       *     zint = xyzFac -1;
       */
      if (octCoords[dim] == maxOctCoord)
        octCoords[dim] = maxOctCoord - 1;
    }

    n.node = ot::TreeNode(octCoords[0], octCoords[1], octCoords[2], maxLev - 1, 3, maxLev);
  }
}
