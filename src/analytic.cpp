#include <dendrite/analytic.h>

#include <dendrite/interp.h>
#include <talyfem/grid/elem-types.h>
#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/femelm.h>
#include <dendrite/Globals.h>

// #include <fortran.h>


/*void addAnalyticalSolution(ot::DA* da, const double* problemSize, Vec u_vec, Vec* output_vec, int ndof, double ts)
{
  unsigned int maxD = da->getMaxDepth();
  unsigned int lev;
  double hx, hy, hz, dist, half;
  Point pt;

  double xFac = problemSize[0]/((double)(1<<(maxD-1)));
  double yFac = problemSize[1]/((double)(1<<(maxD-1)));
  double zFac = problemSize[2]/((double)(1<<(maxD-1)));
  double xx[8], yy[8], zz[8];
  unsigned int idx[8];

  if (!da->computedLocalToGlobal())
    da->computeLocalToGlobalMappings();
  assert(da->computedLocalToGlobal());

  da->createVector(*output_vec, false, false, ndof+1);

  PetscScalar* output_vec_data;
  da->vecGetBuffer(*output_vec, output_vec_data, false, false, false, ndof+1);

  PetscScalar* u_vec_data;
  da->vecGetBuffer(u_vec, u_vec_data, false, false, false, ndof);

  //for ( da->init<ot::DA_FLAGS::WRITABLE>(); da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>() ) {
  for ( da->init<ot::DA_FLAGS::ALL>(); da->curr() < da->end<ot::DA_FLAGS::ALL>(); da->next<ot::DA_FLAGS::ALL>() ) {
    // set the value
    lev = da->getLevel(da->curr());
    hx = xFac * (1 << (maxD - lev));
    hy = yFac * (1 << (maxD - lev));
    hz = zFac * (1 << (maxD - lev));

    pt = da->getCurrentOffset();

    //! get the correct coordinates of the nodes ...
    unsigned char hangingMask = da->getHangingNodeIndex(da->curr());

    xx[0] = pt.x() * xFac;
    yy[0] = pt.y() * yFac;
    zz[0] = pt.z() * zFac;
    xx[1] = pt.x() * xFac + hx;
    yy[1] = pt.y() * yFac;
    zz[1] = pt.z() * zFac;
    xx[2] = pt.x() * xFac;
    yy[2] = pt.y() * yFac + hy;
    zz[2] = pt.z() * zFac;
    xx[3] = pt.x() * xFac + hx;
    yy[3] = pt.y() * yFac + hy;
    zz[3] = pt.z() * zFac;

    xx[4] = pt.x() * xFac;
    yy[4] = pt.y() * yFac;
    zz[4] = pt.z() * zFac + hz;
    xx[5] = pt.x() * xFac + hx;
    yy[5] = pt.y() * yFac;
    zz[5] = pt.z() * zFac + hz;
    xx[6] = pt.x() * xFac;
    yy[6] = pt.y() * yFac + hy;
    zz[6] = pt.z() * zFac + hz;
    xx[7] = pt.x() * xFac + hx;
    yy[7] = pt.y() * yFac + hy;
    zz[7] = pt.z() * zFac + hz;

    da->getNodeIndices(idx);

    for (int i = 0; i < 8; ++i) {
      if (!(hangingMask & (1u << i))) {
        double u_analytical = calc_u_analytical(xx[i], yy[i], zz[i], ts);
        output_vec_data[idx[i]*2] = u_vec_data[idx[i]];
        output_vec_data[idx[i]*2 + 1] = u_analytical;
      }
    }
  } // loop over elements

  da->vecRestoreBuffer(*output_vec, output_vec_data, false, false, false, ndof+1);
  da->vecRestoreBuffer(u_vec, u_vec_data, false, false, false, ndof);
}*/

double calcL2Error(ot::DA *da, const double *problemSize, Vec u, int ndof, int dof, const AnalyticFunction &f) {
    std::vector<AnalyticFunction> fs(ndof);
    fs[dof] = f;
    auto errors = calcL2Errors(da, problemSize, u, ndof, fs);
    return errors[dof];
}

/*class DynamicNodeData {
  static unsigned int g_n_values;
  static unsigned int g_live_count = 0;

  std::vector<double> values_;

 public:
  DynamicNodeData() {
    g_live_count++;
    values_.resize(g_n_values);
  }
  ~DynamicNodeData() {
    g_live_count--;
  }

  static void resize(unsigned int n) {
    assert(g_live_count == 0);
    g_n_values = n;
  }

  static int valueno() {
    return g_live_count;
  }

  static const char* name(int i) {
    static char buff[1024];
    snprintf(buff, sizeof(buff), "data%d", i);
    return buff;
  }

  double& value(int i) {
    return values_.at(i);
  }

  inline double value(int i) const {
    return const_cast<DynamicNodeData*>(this)->value(i);
  }
};*/

std::vector<double>
calcL2Errors(ot::DA *da, const double *problemSize, Vec u, int ndof, const std::vector<AnalyticFunction> &fs) {
    assert (fs.size() == ndof);
    using namespace TALYFEMLIB;

    GRID grid;
    grid.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);

    FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);

    double coords[8 * 3];
    double local_in_dendro[8 * ndof];
    double local_in_taly[8 * ndof];

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));

    PetscScalar *u_vec_data;
    da->vecGetBuffer(u, u_vec_data, false, false, true, ndof);
    da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
    da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);

    std::vector<double> l2_errors(ndof, 0.0);
    double timer = 0;
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {

        int lev = da->getLevel(da->curr());
        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
        Point pt = da->getCurrentOffset();

        pt.x() *= xFac;
        pt.y() *= yFac;
        pt.z() *= zFac;

        // build node coordinates (fill coords)
        build_taly_coordinates(coords, pt, h);

        // interpolate (in -> node_data_temp)
        interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);

        // map from dendro order to taly order
        dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0]) * ndof);

        // update node coordinates and values
        for (unsigned int i = 0; i < 8; i++) {
            grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
        }


        fe.refill(elem, BASIS_LINEAR, 0);

        while (fe.next_itg_pt()) {
            for (unsigned int i = 0; i < 1; i++) {
                if (!fs[i])  // skip null functions
                    continue;

                // do GridField::valueFEM, but without a GridField

                double val_c = 0.0;
                double stime = MPI_Wtime();


                for (ElemNodeID a = 0; a < fe.nbf(); a++) {


                    val_c += fe.N(a) * local_in_taly[a * ndof + i];
                }
//                std::cout << local_in_taly[0] << " " << local_in_taly[1] << " " <<local_in_taly[2] << "\n";
                for (int k = 0; k < fe.nsd(); k++) {

                    double sum = 0;
                    { // Value Derivative
                        const int nbf = fe.nbf();
                        for (ElemNodeID a = 0; a < nbf; a++) {
                            sum += fe.dN(a, k) * local_in_taly[a * ndof + 1];
//                            std::cout << "In " << a << " " << k << " " << fe.dN(a,k) << " " << local_in_taly[a * ndof + 1] <<"\n";

                        }
                    }
//                    std::cout << "Derivative = " << sum << "\n";

                }
                //assert(false);

                double etime = MPI_Wtime();
                timer += etime - stime;

                double val_a = fs[i](fe.position().x(), fe.position().y(), fe.position().z());
                l2_errors[i] += (val_c - val_a) * (val_c - val_a) * fe.detJxW();

            }
        }




//        std::cout << c.f[0] << " " << c.f[1] << " " << c.f[2] << " " << c.f[3] << " " << check <<"\n";
//        assert(false);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    //std::cout << "Total time for Value FEM = " << timer << "\n";

    da->vecRestoreBuffer(u, u_vec_data, false, false, true, ndof);

    for (unsigned int i = 0; i < l2_errors.size(); i++) {
        double all_err;
        MPI_Allreduce(&l2_errors[i], &all_err, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
        l2_errors[i] = sqrt(all_err);
    }

    return l2_errors;
}


/*double calc_l2_error(ot::DA* da, const double* problemSize, Vec u_vec, int ndof, double ts)
{
  using namespace TALYFEMLIB;
  GRID grid;
  grid.redimArrays(8, 1);
  for (int i = 0; i < 8; i++) {
    grid.node_array_[i] = new NODE();
  }

  ELEM* elem = new ELEM3dHexahedral();
  grid.elm_array_[0] = elem;

  static const int node_id_array[8] = {
    0, 1, 2, 3, 4, 5, 6, 7
  };
  elem->redim(8, node_id_array);

  GridField<HTNodeData> gf;
  gf.redimGrid(&grid);
  gf.redimNodeData();

  FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);

  double coords[8*3];
  double local_in_dendro[8 * ndof];
  double local_in_taly[8 * ndof];

  const unsigned int maxD = da->getMaxDepth();
  const double xFac = problemSize[0]/((double)(1<<(maxD-1)));
  const double yFac = problemSize[1]/((double)(1<<(maxD-1)));
  const double zFac = problemSize[2]/((double)(1<<(maxD-1)));

  PetscScalar* u_vec_data;
  da->vecGetBuffer(u_vec, u_vec_data, false, false, true, ndof);
  da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
  da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);

  double l2_error = 0.0;
  double a_norm = 0.0, c_norm = 0.0;
  //for ( da->init<ot::DA_FLAGS::ALL>(); da->curr() < da->end<ot::DA_FLAGS::ALL>(); da->next<ot::DA_FLAGS::ALL>() ) {
  for ( da->init<ot::DA_FLAGS::WRITABLE>(); da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>() ) {
    int lev = da->getLevel(da->curr());
    Point h(xFac*(1<<(maxD - lev)), yFac*(1<<(maxD - lev)), zFac*(1<<(maxD - lev)));
    Point pt = da->getCurrentOffset();
    pt.x() *= xFac;
    pt.y() *= yFac;
    pt.z() *= zFac;

    // build node coordinates (fill coords)
    build_taly_coordinates(coords, pt, h);

    // interpolate (in -> node_data_temp)
    interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);

    // map from dendro order to taly order
    dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0])*ndof);

    // update node coordinates and values
    for (unsigned int i = 0; i < 8; i++) {
      grid.node_array_[i]->setCoor(coords[i*3], coords[i*3+1], coords[i*3+2]);

      for (unsigned int dof = 0; dof < ndof; dof++) {
        gf.GetNodeData(i).value(dof) = local_in_taly[i*ndof+dof];
      }
    }

    fe.refill(elem, BASIS_LINEAR, 0);
    while (fe.next_itg_pt()) {
      double val_c = gf.valueFEM(fe, 0);
      double val_a = calc_u_analytical(fe.position().x(), fe.position().y(), fe.position().z(), ts);
      c_norm += val_c * val_c * fe.detJxW();
      a_norm += val_a * val_a * fe.detJxW();
      l2_error += (val_c - val_a) * (val_c - val_a) * fe.detJxW();
    }
  }

  da->vecRestoreBuffer(u_vec, u_vec_data, false, false, true, ndof);

  double all_err, all_a_norm, all_c_norm;
  MPI_Allreduce(&l2_error, &all_err, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
  //MPI_Allreduce(&a_norm, &all_a_norm, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
  //MPI_Allreduce(&c_norm, &all_c_norm, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);

  return sqrt(all_err);
}*/
void
calcEnergy(ot::DA *da, const double *problemSize, int ndof, Vec u, double &free_energy, double &interfacial_energy) {

    using namespace TALYFEMLIB;

    GRID grid;
    double energy1(0.0), energy2(0.0), epsilon(0.002);
    grid.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);


    FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);

    double coords[8 * 3];
    double local_in_dendro[8 * ndof];
    double local_in_taly[8 * ndof];

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));


    PetscScalar *u_vec_data;
    da->vecGetBuffer(u, u_vec_data, false, false, true, ndof);

    da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
    da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);

    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
        int lev = da->getLevel(da->curr());
        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
        Point pt = da->getCurrentOffset();
        pt.x() *= xFac;
        pt.y() *= yFac;
        pt.z() *= zFac;

        // build node coordinates (fill coords)
        build_taly_coordinates(coords, pt, h);

        // interpolate (in -> node_data_temp)
        interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);

        // map from dendro order to taly order
        dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0]) * ndof);

        // update node coordinates and values
        for (unsigned int i = 0; i < 8; i++) {
            grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
        }

        fe.refill(elem, BASIS_LINEAR, 0);

        const double A(1), B(3);
        while (fe.next_itg_pt()) {
//            for(int i = 0; i < 6; i++){
//                std::cout << local_in_taly[i] << "\n";
//            }
//            assert(false);


            const double detJxW = fe.detJxW();
            for (int k = 0; k < fe.nsd(); k++) {

                double sum = 0;
                { // Value Derivative
                    const int nbf = fe.nbf();
                    for (ElemNodeID a = 0; a < nbf; a++) {
                        sum += fe.dN(a, k) * local_in_taly[a * ndof + 0];
//                        std::cout << "Value = " << a << " "<<  local_in_taly[a * ndof + 0] << "\n";
                    }
                }


                double val_d = sum * sum;
                energy2 += 0.5 * epsilon * val_d * detJxW;
            }


            double sum = 0;
            {
                const int nbf = fe.nbf();
                for (ElemNodeID a = 0; a < nbf; a++) {
                    sum += fe.N(a) * local_in_taly[a * ndof + 0];
                }
            }
            double val_c = sum;
            energy1 += A * 0.25 * (val_c * val_c - 1) * (val_c * val_c - 1) * detJxW;
        }
    }
    da->vecRestoreBuffer(u, u_vec_data, false, false, true, ndof);

    MPI_Allreduce(&energy1, &free_energy, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
    MPI_Allreduce(&energy2, &interfacial_energy, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);



//  return l2_errors;
}

void calcRefinementfunction(ot::DA *da, const double *problemSize, int ndof, Vec u,
                            std::vector<unsigned int> &refinement, int initial_refinement, int solution_refinement,
                            int boundary_refinement,
                            double val, double eps) {

    using namespace TALYFEMLIB;

    da->createVector(refinement, true, true, 1);
    GRID grid;
    double energy1(0.0), energy2(0.0), epsilon(0.1);
    grid.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);


    FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);

    double coords[8 * 3];
    double local_in_dendro[8 * ndof];
    double local_in_taly[8 * ndof];

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));


    PetscScalar *u_vec_data;
    da->vecGetBuffer(u, u_vec_data, false, false, true, ndof);

    da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
    da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);

    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
        int lev = da->getLevel(da->curr());
        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
        Point pt = da->getCurrentOffset();
        pt.x() *= xFac;
        pt.y() *= yFac;
        pt.z() *= zFac;

        // build node coordinates (fill coords)
        build_taly_coordinates(coords, pt, h);

        // interpolate (in -> node_data_temp)
        interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);

        // map from dendro order to taly order
        dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0]) * ndof);

        // update node coordinates and values
        for (unsigned int i = 0; i < 8; i++) {
            grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
        }

        bool boundary = false;
        for (int i = 0; i < 24; i++) {
            if ((fabs(coords[0] - 0.0) < eps) || (fabs(coords[1] - 0.0) < eps) ||
                (fabs(coords[2] - 0.0) < eps)) {
                boundary = true;
            }

        }

        fe.refill(elem, BASIS_LINEAR, 0);

//        double val_d(0.0);
//        while (fe.next_itg_pt()) {
//            const double detJxW = fe.detJxW();
//            for (int k = 0; k < fe.nsd(); k++) {
//
//                double sum = 0;
//                { // Value Derivative
//                    const int nbf = fe.nbf();
//                    for (ElemNodeID a = 0; a < nbf; a++) {
//                        sum += fe.dN(a, k) * local_in_taly[a * ndof + 0];
//                    }
//                }
//                val_d += sum*sum*detJxW;
//            }
//        }
        //Average Refinament
        double val_c(0.0);
        while (fe.next_itg_pt()) {
            for (ElemNodeID a = 0; a < fe.nbf(); a++) {
                val_c += fe.N(a) * local_in_taly[a * ndof + 0];
            }
        }

        if (fabs(val_c) < val) {

//            double ratio = val_d/eps;
//            unsigned int temp = initial_refinement + ceil(log(ratio)/log(8));
            refinement[da->curr()] = solution_refinement;

        } else {

            refinement[da->curr()] = 1;// initial_refinement - 1;
        }

//    double counter = 0;
//    for (ElemNodeID a = 0; a < fe.nbf(); a++) {
//        if(fabs(local_in_taly[a*ndof + 0]) < val){
//            refinement[da->curr()] = solution_refinement;
//            counter = 1;
//            break;
//        }
//    }
//    if(counter == 0){
//        refinement[da-> curr()] =1;// initial_refinement - 1;
//    }
    }


    da->vecRestoreBuffer(u, u_vec_data, false, false, true, ndof);
}

//void calcRefinementfunctionHeat(ot::DA *da, const double *problemSize, int ndof, Vec u,
//                                std::vector<unsigned int> &refinement,
//                                const ElementError &fs) {
//
////    std::vector<AnalyticFunction> fs(ndof);
////    fs[0] = f;
//
//    using namespace TALYFEMLIB;
//
//    da->createVector(refinement, true, true, 1);
//    GRID grid;
//
//    grid.redimArrays(8, 1);
//    for (int i = 0; i < 8; i++) {
//        grid.node_array_[i] = new NODE();
//    }
//
//    ELEM *elem = new ELEM3dHexahedral();
//    grid.elm_array_[0] = elem;
//
//    static const int node_id_array[8] = {
//            0, 1, 2, 3, 4, 5, 6, 7
//    };
//    elem->redim(8, node_id_array);
//
//    FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);
//
//    double coords[8 * 3];
//    double local_in_dendro[8 * ndof];
//    double local_in_taly[8 * ndof];
//
//    const unsigned int maxD = da->getMaxDepth();
//    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
//    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
//    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));
//
//    PetscScalar *u_vec_data;
//    da->vecGetBuffer(u, u_vec_data, false, false, true, ndof);
//
//    da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
//    da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);
//
//    int rank;
//    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
//
//    for (da->init<ot::DA_FLAGS::WRITABLE>();
//         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
//        int lev = da->getLevel(da->curr());
//        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
//        Point pt = da->getCurrentOffset();
//        pt.x() *= xFac;
//        pt.y() *= yFac;
//        pt.z() *= zFac;
//
//
////
//        // build node coordinates (fill coords)
//        build_taly_coordinates(coords, pt, h);
//
//        // interpolate (in -> node_data_temp)
//        interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
//
//        // map from dendro order to taly order
//        dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0]) * ndof);
//
//        // update node coordinates and values
//        for (unsigned int i = 0; i < 8; i++) {
//            grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
//        }
//
//        fe.refill(elem, BASIS_LINEAR, 0);
//
//
//        unsigned int refinement_level = fs(fe, local_in_taly);
//        refinement[da->curr()] = refinement_level;
//    }
//    da->vecRestoreBuffer(u, u_vec_data, false, false, true, ndof);
//}

void calc_error(ot::DA *da, const double *problemSize, int ndof, Vec u, std::vector<unsigned int> &refinement,
                const ElementError &fe_Error, const SurfaceError &surf_Error, const RefineFunction &fs,
                Vec estimator_vec, unsigned int num_err, double *overallEstimator, bool ifSurfErr) {
    using namespace TALYFEMLIB;
    da->createVector(refinement, true, true, 1);
    GRID grid;

    grid.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);

    FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);

    double coords[8 * 3];
    double local_in_dendro[8 * ndof];
    double local_in_taly[8 * ndof];
    //TODO: ndof case for surface element. Works only for ndof =1

    double local_surf_taly[4 * ndof];

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));

    PetscScalar *u_vec_data;
    da->vecGetBuffer(u, u_vec_data, false, false, true, ndof);

    da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
    da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);

    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
    double Elemental_error_timer = 0;
    double Surface_error_timer = 0;
    double Refine_timer = 0;

    da->init<ot::DA_FLAGS::WRITABLE>();
    da->currWithInfo();
    std::vector<std::vector<octInfo> > neighbour_list(6);
    double value_neigh[8 * ndof];
    double *coords_neigh = new double[24];

    octInfo oct;

    const int TalySurfIndicator[6]{-1, 1, 2, -2, 3, -3};
    const int NeighbourSurIndicator[6]{RIGHT, LEFT, BOTTOM, TOP, BACK, FRONT};

    /// This array gives the node ordering of the neighboring surface from the current element
    /// The [i][j]th component gives the node number of the neighboring
    /// element at the jth position of the ith surface of the current element
    const int NeighbourTransfer[6][4]{{1, 0, 3, 2},
                                      {1, 0, 3, 2},
                                      {2, 3, 0, 1},
                                      {2, 3, 0, 1},
                                      {0, 1, 2, 3},
                                      {0, 1, 2, 3}};

    /// Container for the values at the gauss points in the element volume
    double val_fem_volume[8 * ndof];
    /// Container for the derivatives at the gauss points in the element volume
    double **vald_fem_volume = new double *[8];
    for (int i_ = 0; i_ < 8; i_++) {
        vald_fem_volume[i_] = new double[3 * ndof];
    }

    /// Array for the values at the surface gauss points of two neighbors
    double val_fem1[4 * ndof];
    double val_fem2[4 * ndof];
    /// Array for the derivatives at the surface gauss points
    /// from the point of view of two neighboring elements
    double **vald_fem1 = new double *[4];
    double **vald_fem2 = new double *[4];
    for (int i_ = 0; i_ < 4; i_++) {
        vald_fem1[i_] = new double[3 * ndof];
        vald_fem2[i_] = new double[3 * ndof];
    }

    *overallEstimator = 0;
    /// Loop on God knows what (basically the elements)
    for (da->init<ot::DA_FLAGS::FROM_STORED>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {

        int lev = da->getLevel(da->curr());
        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
        Point pt = da->getCurrentOffset();
        pt.x() *= xFac;
        pt.y() *= yFac;
        pt.z() *= zFac;

        oct.curr_id = da->curr();
        // build node coordinates (fill coords)
        build_taly_coordinates(oct.coords, pt, h);

        // interpolate (in -> node_data_temp)
        interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);

        // map from dendro order to taly order
        dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0]) * ndof);

        // update node coordinates and values
        for (unsigned int i = 0; i < 8; i++) {
            grid.node_array_[i]->setCoor(oct.coords[i * 3], oct.coords[i * 3 + 1], oct.coords[i * 3 + 2]);
            oct.node_val[i] = local_in_taly[i];
        }

        /// Remember that the elem object is linked to the grid object
        /// So when grid.node_array is updated, that means elem is also
        /// updated inside the grid
        /// And that's why when we refill fe with elem, it works

        fe.refill(elem, BASIS_LINEAR, 0);
        double start_time = MPI_Wtime();
        valueFEM(fe, ndof, local_in_taly, val_fem_volume);

        /// After the call to valueFEM, all the gauss points of fe have
        /// been traversed. Thus it needs to be refilled again for the call
        /// to valueDerivatveFEM
        fe.refill(elem, BASIS_LINEAR, 0);
        valueDerivativeFEM(fe, ndof, local_in_taly, vald_fem_volume);

        /// ==============================================================
        /// Gathering some vital information about this element
        /// ==============================================================
        fe.refill(elem, BASIS_LINEAR, 0);
        double elem_len = pow(pow(2.0, fe.nsd()) * fe.jacc(), 1.0 / (1.0 * fe.nsd()));
        ZEROPTV center;
        for (int i_ = 0; i_ < fe.n_itg_pts(); i_++) {
            fe.next_itg_pt();
            center += fe.position();
        }
        center = center * (1.0 / 8.0);
        //PrintInfo("ElmCenter = ", center);

        /// ==============================================================
        /// ELEMENTAL RESIDUAL calculation
        /// ==============================================================
        fe.refill(elem, BASIS_LINEAR, 0);
        double residual_domain;
        double residual_domain_squared = 0;
        for (int i_ = 0; i_ < fe.n_itg_pts(); i_++) {
            fe.next_itg_pt();
            residual_domain_squared += (elem_len * elem_len) * fe.detJxW() *
                               fe_Error(fe, val_fem_volume, vald_fem_volume[i_]);
        }
        residual_domain = sqrt(residual_domain_squared);

        //PrintInfo("Position", center, ", elem_len = ", elem_len, ", currentEstimator^2 = ", residual_domain_squared);
        double end_time = MPI_Wtime();
        Elemental_error_timer += end_time - start_time;


        /// ==============================================================
        /// REST OF THE CODE IS ABOUT EDGE RESIDUAL
        /// ==============================================================

        /// ==============================================================
        /// (I) Neighbor calculation starts
        /// ==============================================================
        da->currWithInfo();
        if (ifSurfErr) {
            /// BIG FUNCTION THAT FINDS THE NEIGHBORS
            get_left(da, problemSize, oct, neighbour_list, u_vec_data, ndof);
        }

        da->init<ot::DA_FLAGS::FROM_STORED>();
        int total_num_neighbour = 0;
        for (int i = 0; i < 6; i++) {// 6 is the number of edges
            total_num_neighbour += neighbour_list[i].size();
        }
        std::vector<GRID> grid_neigh(6); // number of edges = 6
        std::vector<FEMElm *> fe_neigh(6);

        /// Initialize the surface errors to zero
        std::vector<double> surface_error(6, 0);

        /// loop on all the faces of this element
        if (ifSurfErr) {
            for (unsigned int i = 0; i < 6; i++) {
                /***
                 * oct.nodal_val contains the nodal values of the current octant
                 * value_neigh contains the nodal values of the neighbouring octants.
                 */

                /// interpolate the data to the appropriate nodes
                for (unsigned int j = 0; j < neighbour_list[i].size(); j++) {
                    interpolateNeighbours(da, neighbour_list[i], oct, coords_neigh, value_neigh, i, ndof);
                }

                /// if there are neighbors, then calculate the jump integration
                if (neighbour_list[i].size() > 0) {
                    ELEM *elem_ = new ELEM3dHexahedral();
                    createGrid(coords_neigh, grid_neigh[i], elem_);
                    TALYFEMLIB::FEMElm *fe_neigh = new FEMElm(&grid_neigh[i], BASIS_ALL);
                    fe_neigh->refill(elem, BASIS_LINEAR, 0);
#if TEST
                    if (i == LEFT) {
                        check_left(fe, *fe_neigh, oct.node_val, value_neigh);
                    }
                    if (i == RIGHT) {
                        check_right(fe, *fe_neigh, oct.node_val, value_neigh);
                    }
                    if (i == TOP) {
                        check_top(fe, *fe_neigh, oct.node_val, value_neigh);
                    }

                    if (i == BOTTOM) {
                        check_bottom(fe, *fe_neigh, oct.node_val, value_neigh);
                    }

                    if (i == FRONT) {
                        bool k_ = check_front(fe, *fe_neigh, oct.node_val, value_neigh);
                    }
                    if (i == BACK) {
                        check_back(fe, *fe_neigh, oct.node_val, value_neigh);
                    }

#endif

                    /// Set the surface indicator and the normal of the current element
                    TALYFEMLIB::SurfaceIndicator surf(TalySurfIndicator[i]);
                    surf.set_normal(elem->CalculateNormal(&grid, TalySurfIndicator[i]));

                    /// Set the surface indicator and the normal of the neighbor element
                    TALYFEMLIB::SurfaceIndicator surf_neigh(TalySurfIndicator[NeighbourSurIndicator[i]]);
                    surf_neigh.set_normal(
                            elem->CalculateNormal(&grid_neigh[i], TalySurfIndicator[NeighbourSurIndicator[i]]));

                    /// Save the normals (I don't know why)
                    ZEROPTV normal, normal_neigh;
                    normal = surf.normal();
                    normal_neigh = surf_neigh.normal();

                    /// Refill the surface FE objects (NEEDS to be done before accessing any FE related values)
                    fe.refill_surface(elem, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
                    fe_neigh->refill_surface(elem, &surf_neigh, TALYFEMLIB::BASIS_LINEAR, 0);
                    /// Calculate the valueFEM from both the FE objects
                    valueFEM(fe, ndof, oct.node_val, val_fem1);
                    valueFEM(*fe_neigh, ndof, value_neigh, val_fem2);

                    /// Refill the surface FE objects (NEEDS to be done before accessing any FE related values)
                    fe.refill_surface(elem, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
                    fe_neigh->refill_surface(elem, &surf_neigh, TALYFEMLIB::BASIS_LINEAR, 0);
                    /// Calculate the valueDerivativeFEM from both the FE objects
                    valueDerivativeFEM(fe, ndof, oct.node_val, vald_fem1);
                    valueDerivativeFEM(*fe_neigh, ndof, value_neigh, vald_fem2);

                    /// Refill the surface FE objects (NEEDS to be done before accessing any FE related values)
                    fe.refill_surface(elem, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
                    fe_neigh->refill_surface(elem, &surf_neigh, TALYFEMLIB::BASIS_LINEAR, 0);

                    /// Loop over the Gauss points of the current FE object
                    for (int nitg = 0; nitg < fe.n_itg_pts(); nitg++) {
                        /// Activate the next FE point on both the FE objects
                        fe.next_itg_pt();
                        fe_neigh->next_itg_pt();

                        /// Calculate the jump integral
                        double this_gauss_pt = surf_Error(fe, *fe_neigh, &val_fem1[nitg],
                                                          &val_fem2[NeighbourTransfer[i][nitg]],
                                                          vald_fem1[nitg],
                                                          vald_fem2[NeighbourTransfer[i][nitg]],
                                                          surf.normal(), surf_neigh.normal(), i);
                        surface_error[i] += 0.5 * elem_len * fe.detJxW() * this_gauss_pt;
                        assert(surface_error[i] >= 0);
                    }

/*#if TEST
                for (int i_ = 0; i_ < 4; i_++) {
                    if (not(EQUALS(coords_[i_].x(), coords_neigh_[NeighbourTransfer[i][i_]].x())
                            and EQUALS(coords_[i_].y(), coords_neigh_[NeighbourTransfer[i][i_]].y())
                            and EQUALS(coords_[i_].z(), coords_neigh_[NeighbourTransfer[i][i_]].z()))) {
                        std::cout << "Wrong in matching of neighbours\n";
                        for (int j_ = 0; j_ < 4; j_++) {
                            std::cout << coords_[j_] << " " << coords_neigh_[NeighbourTransfer[i][j_]] << "\n";
                        }
                        exit(0);
                    }
                }
#endif*/
                    delete fe_neigh;
                } /// end of jump integration on this particular face
            } /// end of loop on the faces
        }

        /// ==============================================================
        /// Calculate the Total EDGE RESIDUAL
        /// ==============================================================
        double residual_edge;
        double residual_edge_squared(0.0);
        for (int i = 0; i < surface_error.size(); i++) {
            residual_edge_squared += surface_error[i];
        }
        residual_edge = sqrt(residual_edge_squared);

        double residual_total_squared = residual_domain_squared + residual_edge_squared;

        // add the total residual squared to the overallEstimator
        *overallEstimator += residual_total_squared;

        // Calculate the total element residual
        double residual_total = sqrt(residual_total_squared);

        unsigned int refinement_level = fs(residual_domain, residual_edge, lev);

        refinement[da->curr()] = refinement_level;

        /// save the error values in estimator_vec
        double *dummyArray;
        PetscErrorCode ierr = VecGetArray(estimator_vec, &dummyArray);
        dummyArray[num_err * da->curr() + 0] = residual_domain;
        dummyArray[num_err * da->curr() + 1] = residual_edge;
        dummyArray[num_err * da->curr() + 2] = residual_total;
        ierr = VecRestoreArray(estimator_vec, &dummyArray);
    } /// end of loop on elements / octants

    for (int i_ = 0; i_ < 4; i_++) {
        delete[] vald_fem1[i_];
        delete[] vald_fem2[i_];
    }
    delete[] vald_fem1;
    delete[] vald_fem2;
    for (int i_ = 0; i_ < 8; i_++) {
        delete[] vald_fem_volume[i_];
    }
    delete[] vald_fem_volume;
    da->vecRestoreBuffer(u, u_vec_data, false, false, true, ndof);
} /// end of calc_error function


/*void get_surface(ot::DA *da, const double *problemSize, int ndof, Vec u) {
    using namespace TALYFEMLIB;


    GRID grid;

    grid.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);

    FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);

    double coords[8 * 3];
    double local_in_dendro[8 * ndof];
    double local_in_taly[8 * ndof];
    double local_surf_taly[4 * ndof];

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));

    PetscScalar *u_vec_data;
    da->vecGetBuffer(u, u_vec_data, false, false, true, ndof);

    da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
    da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);

    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
        int lev = da->getLevel(da->curr());
        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
        Point pt = da->getCurrentOffset();
        pt.x() *= xFac;
        pt.y() *= yFac;
        pt.z() *= zFac;


        // build node coordinates (fill coords)
        build_taly_coordinates(coords, pt, h);

        // interpolate (in -> node_data_temp)
        interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);

        // map from dendro order to taly order
        dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0]) * ndof);


        fe.refill(elem, BASIS_LINEAR, 0);


        for (int elmID = 0; elmID < grid.n_elements(); elmID++) {

            for (int surf_id = 3; surf_id >= -3; surf_id--) {
                if (surf_id != 0) {
                    TALYFEMLIB::SurfaceIndicator surf(surf_id);
                    surf.set_normal(elem->CalculateNormal(&grid, surf_id));
                    fe.refill_surface(elem, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
                    dendro_to_taly_surface(local_surf_taly, local_in_dendro, sizeof(local_surf_taly[0]),
                                           surf_id);

                }
            }
        }


    }


}*/

/*
double integrate(ot::DA *da, const double *problemSize, Vec u) {
    using namespace TALYFEMLIB;
    int ndof = 1;
    GRID grid;
    grid.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);

    FEMElm fe(&grid, BASIS_FIRST_DERIVATIVE | BASIS_POSITION);

    double coords[8 * 3];
    double local_in_dendro[8 * ndof];
    double local_in_taly[8 * ndof];

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));

    PetscScalar *u_vec_data;
    da->vecGetBuffer(u, u_vec_data, false, false, true, ndof);
    da->ReadFromGhostsBegin<PetscScalar>(u_vec_data, ndof);
    da->ReadFromGhostsEnd<PetscScalar>(u_vec_data);

    std::vector<double> l2_errors(ndof, 0.0);
    double sum = 0;
    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
        int lev = da->getLevel(da->curr());
        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
        Point pt = da->getCurrentOffset();
        pt.x() *= xFac;
        pt.y() *= yFac;
        pt.z() *= zFac;

        // build node coordinates (fill coords)
        build_taly_coordinates(coords, pt, h);

        // interpolate (in -> node_data_temp)
        interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);

        // map from dendro order to taly order
        dendro_to_taly(local_in_taly, local_in_dendro, sizeof(local_in_taly[0]) * ndof);

        // update node coordinates and values
        for (unsigned int i = 0; i < 8; i++) {
            grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
        }

        Globals globals;
        fe.refill(elem, BASIS_LINEAR, globals.getrelOrder());

        while (fe.next_itg_pt()) {
            sum += sin(M_PI * fe.position()(0)) * sin(M_PI * fe.position()(1)) * sin(M_PI * fe.position()(2)) *
                   fe.detJxW();
        }

    }
    return (sum);

}
*/

void valueDerivativeFEM(TALYFEMLIB::FEMElm &fe, int ndof, const double *value, double **val_d) {


    for (int i = 0; i < fe.n_itg_pts(); i++) {
        fe.next_itg_pt();
        for (int dof = 0; dof < ndof; dof++) {

            for (int dir = 0; dir < 3; dir++) {
                val_d[i][3 * dof + dir] = 0;
                for (ElemNodeID a = 0; a < fe.nbf(); a++) {
                    val_d[i][3 * dof + dir] += fe.dN(a, dir) * value[a * ndof + dof];
                }
            }
        }
    }
    /*for(int dof = 0; dof < ndof; dof++) {
        for (int dir = 0; dir < 3; dir++) {
            val_d[3*dof + dir] = 0;
            for (ElemNodeID a = 0; a < fe.nbf(); a++) {
                val_d[3*dof + dir] += fe.dN(a, dir) * value[a * ndof + dof];
            }
        }
    }*/
}

void
valueFEM(TALYFEMLIB::FEMElm &fe, int ndof, double *value, double *val_c/*, std::vector<TALYFEMLIB::ZEROPTV> &coords*/) {


//#if TEST
//    coords.clear();
//    coords.resize(4);
//#endif
    for (int i = 0; i < fe.n_itg_pts(); i++) {
        fe.next_itg_pt();
//#if TEST
//        coords[i] = fe.position();
//#endif
        for (int dof = 0; dof < ndof; dof++) {
            val_c[i + dof * fe.n_itg_pts()] = 0;
            for (ElemNodeID a = 0; a < fe.nbf(); a++) {
                val_c[i + fe.n_itg_pts() * dof] += fe.N(a) * value[a * ndof + dof];
            }
        }
    }
}

/**
 *
 * @param da The Dendro data structure
 * @param problemSize Array of the lengths in each of the directions
 * @param oct
 * @param neighbour_list
 * @param u_vec_data
 * @param ndof
 */
void get_left(ot::DA *da, const double *problemSize, octInfo &oct, std::vector<std::vector<octInfo> > &neighbour_list,
              double *u_vec_data, unsigned int ndof) {

    for (int i = 0; i < 6; i++) {
        neighbour_list[i].clear();
    }

    double *local_in_dendro = new double[8 * ndof];
    double min_x = oct.coords[0];
    double max_x = oct.coords[18];
    double min_y = oct.coords[1];
    double max_y = oct.coords[19];
    double min_z = oct.coords[2];
    double max_z = oct.coords[20];
    std::vector<bool> full(6, false);
    if ((EQUALS(min_x, 0.0))) {
        full[LEFT] = true;
    }
    if ((EQUALS(max_x, problemSize[0]))) {
        full[RIGHT] = true;
    }
    if ((EQUALS(min_y, 0.0))) {
        full[BOTTOM] = true;
    }
    if ((EQUALS(max_y, problemSize[1]))) {
        full[TOP] = true;
    }
    if ((EQUALS(min_z, 0))) {
        full[BACK] = true;
    }
    if ((EQUALS(max_z, problemSize[2]))) {
        full[FRONT] = true;
    }


#ifdef TEST

    for (int j = 0; j < 8; j++) {
        if ((oct.coords[3 * j + 0] < min_x) or (oct.coords[3 * j + 1] < min_y) or
            (oct.coords[3 * j + 2] < min_z)) {
            std::cout << "Wrong in computing minimum of octant. Maximum not checked  \n";
            exit(0);
        }
        if ((oct.coords[3 * j + 0] > max_x) or (oct.coords[3 * j + 1] > max_y) or
            (oct.coords[3 * j + 2] > max_z)) {
            std::cout << oct.coords[3 * j + 0] << " " << max_x << "\n";
            std::cout << oct.coords[3 * j + 1] << " " << max_y << "\n";
            std::cout << oct.coords[3 * j + 2] << " " << max_z << "\n";
            std::cout << "Wrong in computing maximum of octant \n";
            exit(0);
        }
    }

#endif

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));

    octInfo oct_;

    unsigned int curr_lvl = da->getLevel(oct.curr_id);

    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
        if (std::all_of(full.begin(), full.end(), [](int i) { return (i == true); })) {
            return;
        }

        if ((da->curr() != oct.curr_id)) {
            int lev = da->getLevel(da->curr());

            Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
            Point pt = da->getCurrentOffset();
            pt.x() *= xFac;
            pt.y() *= yFac;
            pt.z() *= zFac;
            build_taly_coordinates(oct_.coords, pt, h);

            if ((lev >= curr_lvl) and (fabs(oct_.coords[3] - min_x) < 1E-6) and (oct_.coords[4] >= min_y) and
                (oct_.coords[7] <= max_y) and
                (oct_.coords[2] >= min_z) and (oct_.coords[23] <= max_z)) {

                interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);

                oct_.curr_id = da->curr();
                neighbour_list[LEFT].push_back(oct_);
                if (lev == curr_lvl) {
                    full[LEFT] = true;
                } else if (neighbour_list[LEFT].size() == 4) {
                    full[LEFT] = true;
                }
/*
                std::cout << "-----------------Found left potential ------------------------------------ \n";
                for (int j = 0; j < 8; j++) {
                    std::cout << oct_.coords[3 * j + 0] << " " << oct_.coords[3 * j + 1] << " " << oct_.coords[3 * j + 2] << ";\n";
                }
                std::cout << "Coord x = " << oct_.coords[3] << "\n";
                std::cout << "Minimum = (" << min_x << "," << min_y << "," << min_z << ")\n";
                std::cout << "Maximum = (" << max_x << "," << max_y << "," << max_z << ")\n";
                std::cout << "-----------------Found left potential ------------------------------------ \n";
*/
            }
            if ((lev < curr_lvl) and (EQUALS(oct_.coords[3], min_x))) {

                bool case1 = ((EQUALS(oct_.coords[4], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case2 = ((oct_.coords[4] < min_y) and (EQUALS(oct_.coords[7], max_y)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case3 = ((oct_.coords[4] < min_y) and (EQUALS(oct_.coords[7], max_y)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                bool case4 = ((EQUALS(oct_.coords[4], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                if (case1 or case2 or case3 or case4) {
                    interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                    dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                    oct_.curr_id = da->curr();
                    if (case1) oct_.neighCase = 1;
                    if (case2) oct_.neighCase = 2;
                    if (case3) oct_.neighCase = 3;
                    if (case4) oct_.neighCase = 4;
                    neighbour_list[LEFT].push_back(oct_);
                    full[LEFT] = true;

                }

            }

            if ((lev >= curr_lvl) and (fabs(oct_.coords[10] - min_y) < 1E-6) and (oct_.coords[0] >= min_x) and
                (oct_.coords[3] <= max_x) and
                (oct_.coords[2] >= min_z) and (oct_.coords[23] <= max_z)) {

                interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                oct_.curr_id = da->curr();
                neighbour_list[BOTTOM].push_back(oct_);
                if (lev == curr_lvl) {
                    full[BOTTOM] = true;
                } else if (neighbour_list[BOTTOM].size() == 4) {
                    full[BOTTOM] = true;
                }

/*
                std::cout << "-----------------Found bottom potential ------------------------------------ \n";
                for (int j = 0; j < 8; j++) {
                    std::cout << oct_.coords[3 * j + 0] << " " << oct_.coords[3 * j + 1] << " " << oct_.coords[3 * j + 2] << ";\n";
                }
                std::cout << "Coord x = " << oct_.coords[3] << "\n";
                std::cout << "Minimum = (" << min_x << "," << min_y << "," << min_z << ")\n";
                std::cout << "Maximum = (" << max_x << "," << max_y << "," << max_z << ")\n";
                std::cout << "-----------------Found bottom potential ------------------------------------ \n";
*/
            }

            if ((lev < curr_lvl) and (EQUALS(oct_.coords[10], min_y))) {
                bool case1 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case2 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case3 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                bool case4 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                if (case1 or case2 or case3 or case4) {
                    interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                    dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                    oct_.curr_id = da->curr();
                    if (case1) oct_.neighCase = 1;
                    if (case2) oct_.neighCase = 2;
                    if (case3) oct_.neighCase = 3;
                    if (case4) oct_.neighCase = 4;
                    neighbour_list[BOTTOM].push_back(oct_);
                    full[BOTTOM] = true;
                }
            }


            if ((lev >= curr_lvl) and (fabs(oct_.coords[14] - min_z) < 1E-6) and (oct_.coords[0] >= min_x) and
                (oct_.coords[3] <= max_x) and
                (oct_.coords[4] >= min_y) and (oct_.coords[7] <= max_y)) {

                interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                oct_.curr_id = da->curr();

                neighbour_list[BACK].push_back(oct_);

                if (lev == curr_lvl) {
                    full[BACK] = true;
                } else if (neighbour_list[BACK].size() == 4) {
                    full[BACK] = true;
                }
/*
                std::cout << "-----------------Found back potential ------------------------------------ \n";
                for (int j = 0; j < 8; j++) {
                    std::cout << oct_.coords[3 * j + 0] << " " << oct_.coords[3 * j + 1] << " " << oct_.coords[3 * j + 2] << ";\n";
                }
                std::cout << "Coord x = " << oct_.coords[3] << "\n";
                std::cout << "Minimum = (" << min_x << "," << min_y << "," << min_z << ")\n";
                std::cout << "Maximum = (" << max_x << "," << max_y << "," << max_z << ")\n";
                std::cout << "-----------------Found back potential ------------------------------------ \n";
*/
            }

            if ((lev < curr_lvl) and (EQUALS(oct_.coords[14], min_z))) {
                bool case1 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (EQUALS(oct_.coords[1], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)));
                bool case2 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (EQUALS(oct_.coords[1], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)));
                bool case3 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (oct_.coords[1] < min_y) and (EQUALS(oct_.coords[7], max_y)));
                bool case4 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (oct_.coords[1] < min_y) and (EQUALS(oct_.coords[7], max_y)));
                if (case1 or case2 or case3 or case4) {
                    interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                    dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                    oct_.curr_id = da->curr();
                    if (case1) oct_.neighCase = 1;
                    if (case2) oct_.neighCase = 2;
                    if (case3) oct_.neighCase = 3;
                    if (case4) oct_.neighCase = 4;
                    neighbour_list[BACK].push_back(oct_);
                    full[BACK] = true;
                }

            }

            if ((lev >= curr_lvl) and (fabs(oct_.coords[0] - max_x) < 1E-6) and (oct_.coords[1] >= min_y) and
                (oct_.coords[7] <= max_y) and
                (oct_.coords[2] >= min_z) and (oct_.coords[23] <= max_z)) {

                interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                oct_.curr_id = da->curr();

                neighbour_list[RIGHT].push_back(oct_);

                if (lev == curr_lvl) {
                    full[RIGHT] = true;
                } else if (neighbour_list[RIGHT].size() == 4) {
                    full[RIGHT] = true;
                }
                /*std::cout << "-----------------Found right potential ------------------------------------ \n";
                for (int j = 0; j < 8; j++) {
                    std::cout << oct_.coords[3 * j + 0] << " " << oct_.coords[3 * j + 1] << " " << oct_.coords[3 * j + 2] << ";\n";
                }
                std::cout << "Coord x = " << oct_.coords[3] << "\n";
                std::cout << "Minimum = (" << min_x << "," << min_y << "," << min_z << ")\n";
                std::cout << "Maximum = (" << max_x << "," << max_y << "," << max_z << ")\n";
                std::cout << "-----------------Found right potential ------------------------------------ \n";*/
            }

            if ((lev < curr_lvl) and (fabs(oct_.coords[0] - max_x) < 1E-6)) {
                bool case1 = ((EQUALS(oct_.coords[1], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case2 = ((oct_.coords[1] < min_y) and (EQUALS(oct_.coords[7], max_y)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case3 = ((oct_.coords[1] < min_y) and (EQUALS(oct_.coords[7], max_y)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                bool case4 = ((EQUALS(oct_.coords[1], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                if (case1 or case2 or case3 or case4) {
                    interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                    dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                    oct_.curr_id = da->curr();
                    if (case1) oct_.neighCase = 1;
                    if (case2) oct_.neighCase = 2;
                    if (case3) oct_.neighCase = 3;
                    if (case4) oct_.neighCase = 4;
                    neighbour_list[RIGHT].push_back(oct_);
                    full[RIGHT] = true;
                }
            }


            if ((lev >= curr_lvl) and (fabs(oct_.coords[1] - max_y) < 1E-6) and (oct_.coords[0] >= min_x) and
                (oct_.coords[3] <= max_x) and
                (oct_.coords[2] >= min_z) and (oct_.coords[23] <= max_z)) {

                interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);

                oct_.curr_id = da->curr();

                neighbour_list[TOP].push_back(oct_);
                if (lev == curr_lvl) {
                    full[TOP] = true;
                } else if (neighbour_list[TOP].size() == 4) {
                    full[TOP] = true;
                }
                /* std::cout << "-----------------Found top potential ------------------------------------ \n";
                 for (int j = 0; j < 8; j++) {
                     std::cout << oct_.coords[3 * j + 0] << " " << oct_.coords[3 * j + 1] << " " << oct_.coords[3 * j + 2] << ";\n";
                 }
                 std::cout << "Coord x = " << oct_.coords[3] << "\n";
                 std::cout << "Minimum = (" << min_x << "," << min_y << "," << min_z << ")\n";
                 std::cout << "Maximum = (" << max_x << "," << max_y << "," << max_z << ")\n";
                 std::cout << "-----------------Found top potential ------------------------------------ \n";*/
            }
            if ((lev < curr_lvl) and (EQUALS(oct_.coords[1], max_y))) {

                bool case1 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case2 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (EQUALS(oct_.coords[2], min_z)) and (EQUALS(oct_.coords[23], 2.0 * max_z - min_z)));
                bool case3 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                bool case4 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (oct_.coords[2] < min_z) and (EQUALS(oct_.coords[23], max_z)));
                if (case1 or case2 or case3 or case4) {
                    interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                    dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                    oct_.curr_id = da->curr();
                    if (case1) oct_.neighCase = 1;
                    if (case2) oct_.neighCase = 2;
                    if (case3) oct_.neighCase = 3;
                    if (case4) oct_.neighCase = 4;
                    neighbour_list[TOP].push_back(oct_);
                    full[TOP] = true;
                }
            }

            if ((lev >= curr_lvl) and (fabs(oct_.coords[2] - max_z) < 1E-6) and (oct_.coords[0] >= min_x) and
                (oct_.coords[3] <= max_x) and
                (oct_.coords[4] >= min_y) and (oct_.coords[7] <= max_y)) {

                interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                oct_.curr_id = da->curr();

                neighbour_list[FRONT].push_back(oct_);

                if (lev == curr_lvl) {
                    full[FRONT] = true;
                } else if (neighbour_list[FRONT].size() == 4) {
                    full[FRONT] = true;
                }
                /*std::cout << "-----------------Found front potential ------------------------------------ \n";
                for (int j = 0; j < 8; j++) {
                    std::cout << oct_.coords[3 * j + 0] << " " << oct_.coords[3 * j + 1] << " " << oct_.coords[3 * j + 2] << ";\n";
                }
                std::cout << "Coord x = " << oct_.coords[3] << "\n";
                std::cout << "Minimum = (" << min_x << "," << min_y << "," << min_z << ")\n";
                std::cout << "Maximum = (" << max_x << "," << max_y << "," << max_z << ")\n";
                std::cout << "-----------------Found front potential ------------------------------------ \n";*/
            }
            if ((lev < curr_lvl) and (EQUALS(oct_.coords[2], max_z))) {
                bool case1 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (EQUALS(oct_.coords[1], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)));
                bool case2 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (EQUALS(oct_.coords[1], min_y)) and (EQUALS(oct_.coords[7], 2.0 * max_y - min_y)));
                bool case3 = ((oct_.coords[0] < min_x) and (EQUALS(oct_.coords[3], max_x)) and
                              (oct_.coords[1] < min_y) and (EQUALS(oct_.coords[7], max_y)));
                bool case4 = ((EQUALS(oct_.coords[0], min_x)) and (EQUALS(oct_.coords[3], 2.0 * max_x - min_x)) and
                              (oct_.coords[1] < min_y) and (EQUALS(oct_.coords[7], max_y)));
                if (case1 or case2 or case3 or case4) {
                    interp_global_to_local(u_vec_data, local_in_dendro, da, ndof);
                    dendro_to_taly(oct_.node_val, local_in_dendro, sizeof(local_in_dendro[0]) * ndof);
                    oct_.curr_id = da->curr();
                    if (case1) oct_.neighCase = 1;
                    if (case2) oct_.neighCase = 2;
                    if (case3) oct_.neighCase = 3;
                    if (case4) oct_.neighCase = 4;
                    neighbour_list[FRONT].push_back(oct_);
                    full[FRONT] = true;
                }
            }


        }

    }
    delete local_in_dendro;
}

/*void get_neighbours(ot::DA *da, double *problemSize) {
    using namespace TALYFEMLIB;

    const unsigned int maxD = da->getMaxDepth();
    const double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
    const double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
    const double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));
    da->init<ot::DA_FLAGS::WRITABLE>();
    da->currWithInfo();
    std::vector<std::vector<octInfo> > neighbour_list(6);
    double coords[24];
    for (da->init<ot::DA_FLAGS::FROM_STORED>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
        unsigned int curr_id = da->curr();

        octInfo oct;
        std::cout << "After Current = " << da->curr() << "\n";

        int lev = da->getLevel(da->curr());
        Point h(xFac * (1 << (maxD - lev)), yFac * (1 << (maxD - lev)), zFac * (1 << (maxD - lev)));
        Point pt = da->getCurrentOffset();
        pt.x() *= xFac;
        pt.y() *= yFac;
        pt.z() *= zFac;
        build_taly_coordinates(oct.coords, pt, h);
        da->getNodeIndices(oct.node_idx);
        oct.curr_id = da->curr();


        da->currWithInfo();
        get_left(da, problemSize, oct, neighbour_list);
        da->init<ot::DA_FLAGS::FROM_STORED>();
//        writeMatlabNeighbourList(oct, neighbour_list);


        int total_num_neighbour = 0;
        for (int i = 0; i < 6; i++) {
            total_num_neighbour += neighbour_list[i].size();
        }

        std::vector<GRID> grid(total_num_neighbour);
        std::vector<FEMElm *> fe(total_num_neighbour);
        int counter = 0;
        for (unsigned int i = 0; i < 6; i++) {
            for (unsigned int j = 0; j < neighbour_list[i].size(); j++) {
                interpolateNeighbours(da, neighbour_list[i], oct, coords, i);
                createFEMElm(neighbour_list[i][j].coords, grid[counter], fe[counter]);
                counter++;
            }
        }
        for (int i = 0; i < total_num_neighbour; i++) {
            delete fe[i];
        }
    }

}*/


void interpolateNeighbours(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords, double *val,
                           unsigned int side, const unsigned int ndof) {
#ifdef TEST
    if ((neighbours.size() > 1) and (neighbours.size() != 4) and
        (da->getLevel(curr_oct.curr_id) > da->getLevel(neighbours[0].curr_id))) {
        std::cout << "--------------------------Wrong computation ------------------------------\n";
        exit(0);
    }
#endif
    if (neighbours.size() == 1) {
        int i = 0;

        unsigned int lev_curr = da->getLevel(curr_oct.curr_id);
        unsigned int lev_neigh = da->getLevel(neighbours[i].curr_id);

        if (lev_curr == lev_neigh) {
            for (int j = 0; j < 24; j++) {
                coords[j] = neighbours[i].coords[j];
            }
            for (int j = 0; j < 8; j++) {
                val[j] = neighbours[i].node_val[j];
            }
        } else if (lev_curr > lev_neigh) {
#ifdef TEST
            if (neighbours.size() != 1) {
                std::cout << "--------------------------Wrong computation ------------------------------\n";
                exit(0);
            }

#endif


            for (int j = 0; j < 24; j++) {
                coords[j] = neighbours[i].coords[j];
            }
            for (int j = 0; j < 8; j++) {
                val[j] = neighbours[i].node_val[j];
            }

            switch (side) {
                case LEFT: {
                    interpolateXFromLowerLevel(da, neighbours[i], curr_oct, coords, val, LEFT, ndof);
#if TEST
                    {
                        const int check[16]{1, 2, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23};
                        for (int ch = 0; ch < 16; ch++) {
                            if (not(EQUALS(coords[check[ch]], curr_oct.coords[check[ch]]))) {
                                std::cout << "Error in computing coords of the left index " << "\n";
                                exit(0);
                            }
                        }
                    }
#endif
                    break;
                }
                case RIGHT: {
                    interpolateXFromLowerLevel(da, neighbours[i], curr_oct, coords, val, RIGHT, ndof);
#if TEST
                    {
                        const int check[16]{1, 2, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23};
                        for (int ch = 0; ch < 16; ch++) {
                            if (not(EQUALS(coords[check[ch]], curr_oct.coords[check[ch]]))) {
                                std::cout << "Error in computing coords of the right index " << "\n";
                                exit(0);
                            }
                        }
                    }
#endif
                    break;

                }
                case TOP: {
                    interpolateYFromLowerLevel(da, neighbours[i], curr_oct, coords, val, TOP, ndof);
#if TEST
                    {
                        const int check[16]{0, 2, 3, 5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21, 23};
                        for (int ch = 0; ch < 16; ch++) {
                            if (not(EQUALS(coords[check[ch]], curr_oct.coords[check[ch]]))) {
                                std::cout << "Error in computing coords of the top index " << "\n";
                                exit(0);
                            }
                        }
                    }
#endif
                    break;
                }
                case BOTTOM: {
                    interpolateYFromLowerLevel(da, neighbours[i], curr_oct, coords, val, BOTTOM, ndof);
#if TEST
                    {
                        const int check[16]{0, 2, 3, 5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21, 23};
                        for (int ch = 0; ch < 16; ch++) {
                            if (not(EQUALS(coords[check[ch]], curr_oct.coords[check[ch]]))) {
                                std::cout << "Error in computing coords of the bottom index " << "\n";
                                exit(0);
                            }
                        }
                    }
#endif
                    break;

                }
                case FRONT: {
                    interpolateZFromLowerLevel(da, neighbours[i], curr_oct, coords, val, FRONT, ndof);
#if TEST
                    {
                        const int check[16]{0, 1, 3, 4, 6, 7, 9, 10, 12, 13, 15, 16, 18, 19, 21, 22};
                        for (int ch = 0; ch < 16; ch++) {
                            if (not(EQUALS(coords[check[ch]], curr_oct.coords[check[ch]]))) {
                                std::cout << "Error in computing coords of the front index " << "\n";
                                exit(0);
                            }
                        }
                    }
#endif
                    break;
                }
                case BACK: {
                    interpolateZFromLowerLevel(da, neighbours[i], curr_oct, coords, val, BACK, ndof);
#if TEST
                    {
                        const int check[16]{0, 1, 3, 4, 6, 7, 9, 10, 12, 13, 15, 16, 18, 19, 21, 22};
                        for (int ch = 0; ch < 16; ch++) {
                            if (not(EQUALS(coords[check[ch]], curr_oct.coords[check[ch]]))) {
                                std::cout << "Error in computing coords of the back index " << "\n";
                                exit(0);
                            }
                        }
                    }
#endif
                    break;
                }
            }

        }


    } else {
        switch (side) {
            case RIGHT: {
                interpolateXFromHigherLevel(da, neighbours, curr_oct, coords, val, side, ndof);
#if TEST
                bool check1 = ((EQUALS(coords[0], curr_oct.coords[3])) and
                               (EQUALS(coords[1], curr_oct.coords[4])) and
                               (EQUALS(coords[2], curr_oct.coords[5])));
                bool check2 = ((EQUALS(coords[9], curr_oct.coords[6])) and
                               (EQUALS(coords[10], curr_oct.coords[7])) and
                               (EQUALS(coords[11], curr_oct.coords[8])));
                bool check3 = ((EQUALS(coords[12], curr_oct.coords[15])) and
                               (EQUALS(coords[13], curr_oct.coords[16])) and
                               (EQUALS(coords[14], curr_oct.coords[17])));
                bool check4 = ((EQUALS(coords[21], curr_oct.coords[18])) and
                               (EQUALS(coords[22], curr_oct.coords[19])) and
                               (EQUALS(coords[23], curr_oct.coords[20])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "Wrong in resultant Right interpolating to higher level = " << check1 << " " << check2
                              << " " << check3 << " " << check4 << "\n";
                    exit(0);
                }
#endif
                break;
            }
            case LEFT: {
                interpolateXFromHigherLevel(da, neighbours, curr_oct, coords, val, side, ndof);
#ifdef LEFT

                bool check1 = ((EQUALS(curr_oct.coords[0], coords[3])) and
                               (EQUALS(curr_oct.coords[1], coords[4])) and
                               (EQUALS(curr_oct.coords[2], coords[5])));
                bool check2 = ((EQUALS(curr_oct.coords[9], coords[6])) and
                               (EQUALS(curr_oct.coords[10], coords[7])) and
                               (EQUALS(curr_oct.coords[11], coords[8])));
                bool check3 = ((EQUALS(curr_oct.coords[12], coords[15])) and
                               (EQUALS(curr_oct.coords[13], coords[16])) and
                               (EQUALS(curr_oct.coords[14], coords[17])));
                bool check4 = ((EQUALS(curr_oct.coords[21], coords[18])) and
                               (EQUALS(curr_oct.coords[22], coords[19])) and
                               (EQUALS(curr_oct.coords[23], coords[20])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "Wrong in resultant Left interpolating to higher level = " << check1 << " " << check2
                              << " " << check3
                              << " " << check4 << "\n";
                    exit(0);
                }
#endif
                break;

            }

            case TOP: {
                interpolateYFromHigherLevel(da, neighbours, curr_oct, coords, val, side, ndof);
#if TEST

                bool check1 = ((EQUALS(coords[0], curr_oct.coords[9])) and
                               (EQUALS(coords[1], curr_oct.coords[10])) and
                               (EQUALS(coords[2], curr_oct.coords[11])));
                bool check2 = ((EQUALS(coords[3], curr_oct.coords[6])) and
                               (EQUALS(coords[4], curr_oct.coords[7])) and
                               (EQUALS(coords[5], curr_oct.coords[8])));
                bool check3 = ((EQUALS(coords[12], curr_oct.coords[21])) and
                               (EQUALS(coords[13], curr_oct.coords[22])) and
                               (EQUALS(coords[14], curr_oct.coords[23])));
                bool check4 = ((EQUALS(coords[15], curr_oct.coords[18])) and
                               (EQUALS(coords[16], curr_oct.coords[19])) and
                               (EQUALS(coords[17], curr_oct.coords[20])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "Wrong in resultant Top interpolating to higher level = " << check1 << " " << check2
                              << " " << check3
                              << " " << check4 << "\n";
                    std::cout << " Coords=" << coords[0] << " " << coords[1] << " " << coords[2] << "\n";
                    std::cout << " Cuurent=" << curr_oct.coords[9] << " " << curr_oct.coords[10] << " "
                              << curr_oct.coords[11] << "\n";
                    exit(0);
                }


#endif
                break;
            }
            case BOTTOM: {
                interpolateYFromHigherLevel(da, neighbours, curr_oct, coords, val, side, ndof);
#if TEST
                bool check1 = ((EQUALS(curr_oct.coords[0], coords[9])) and
                               (EQUALS(curr_oct.coords[1], coords[10])) and
                               (EQUALS(curr_oct.coords[2], coords[11])));
                bool check2 = ((EQUALS(curr_oct.coords[3], coords[6])) and
                               (EQUALS(curr_oct.coords[4], coords[7])) and
                               (EQUALS(curr_oct.coords[5], coords[8])));
                bool check3 = ((EQUALS(curr_oct.coords[12], coords[21])) and
                               (EQUALS(curr_oct.coords[13], coords[22])) and
                               (EQUALS(curr_oct.coords[14], coords[23])));
                bool check4 = ((EQUALS(curr_oct.coords[15], coords[18])) and
                               (EQUALS(curr_oct.coords[16], coords[19])) and
                               (EQUALS(curr_oct.coords[17], coords[20])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "Wrong in resultant Bottom interpolating to higher level = " << check1 << " " << check2
                              << " " << check3
                              << " " << check4 << "\n";
                    exit(0);
                }
#endif
                break;
            }
            case FRONT: {
                interpolateZFromHigherLevel(da, neighbours, curr_oct, coords, val, side, ndof);
#if TEST
                bool check1 = ((EQUALS(coords[0], curr_oct.coords[12])) and
                               (EQUALS(coords[1], curr_oct.coords[13])) and
                               (EQUALS(coords[2], curr_oct.coords[14])));
                bool check2 = ((EQUALS(coords[3], curr_oct.coords[15])) and
                               (EQUALS(coords[4], curr_oct.coords[16])) and
                               (EQUALS(coords[5], curr_oct.coords[17])));
                bool check3 = ((EQUALS(coords[6], curr_oct.coords[18])) and
                               (EQUALS(coords[7], curr_oct.coords[19])) and
                               (EQUALS(coords[8], curr_oct.coords[20])));
                bool check4 = ((EQUALS(coords[9], curr_oct.coords[21])) and
                               (EQUALS(coords[10], curr_oct.coords[22])) and
                               (EQUALS(coords[11], curr_oct.coords[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "Wrong in resultant Front interpolating to higher level = " << check1 << " " << check2
                              << " " << check3
                              << " " << check4 << "\n";
                    exit(0);
                }
#endif
                break;

            }
            case BACK: {
                interpolateZFromHigherLevel(da, neighbours, curr_oct, coords, val, side, ndof);
#if TEST
                bool check1 = ((EQUALS(curr_oct.coords[0], coords[12])) and
                               (EQUALS(curr_oct.coords[1], coords[13])) and
                               (EQUALS(curr_oct.coords[2], coords[14])));
                bool check2 = ((EQUALS(curr_oct.coords[3], coords[15])) and
                               (EQUALS(curr_oct.coords[4], coords[16])) and
                               (EQUALS(curr_oct.coords[5], coords[17])));
                bool check3 = ((EQUALS(curr_oct.coords[6], coords[18])) and
                               (EQUALS(curr_oct.coords[7], coords[19])) and
                               (EQUALS(curr_oct.coords[8], coords[20])));
                bool check4 = ((EQUALS(curr_oct.coords[9], coords[21])) and
                               (EQUALS(curr_oct.coords[10], coords[22])) and
                               (EQUALS(curr_oct.coords[11], coords[23])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "Wrong in resultant Back interpolating to higher level = " << check1 << " " << check2
                              << " " << check3
                              << " " << check4 << "\n";
                    std::cout << "Coords = " << coords[6] << " " << coords[7] << " " << coords[8] << "\n";
                    std::cout << "Current Coords = " << curr_oct.coords[6] << " " << curr_oct.coords[7] << " "
                              << curr_oct.coords[8] << "\n";
                    exit(0);
                }
#endif
                break;

            }
            default: {
                std::cout << "Wrong case here. Please check\n";
                exit(0);
            }
        }

    }
}

void createGrid(const double *coords, TALYFEMLIB::GRID &grid, TALYFEMLIB::ELEM *elem) {
    using namespace TALYFEMLIB;

    grid.redimArrays(8, 1);

    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);

    for (unsigned int i = 0; i < 8; i++) {
        grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
    }


}

void writeMatlabNeighbourList(octInfo &oct, std::vector<std::vector<octInfo> > &neighbour_list) {
    std::string filename = "neigbour" + std::to_string(oct.curr_id) + ".m";
    std::ofstream file_(filename);

    file_ << "clear; \n  close all;\n";

    file_ << "minimumPoint = [ " << oct.coords[0] << "," << oct.coords[1] << " , " << oct.coords[2] << "];\n";
    file_ << "maximumPoint = [ " << oct.coords[18] << "," << oct.coords[19] << " , " << oct.coords[20] << "];\n";
    file_ << "dist = maximumPoint - minimumPoint;\n";
    file_ << "plotcube(dist,minimumPoint,0.4,'y'); \n";
    file_ << "hold on;\n";
    int counter = 0;
    std::string var_;
    const char color[6]{'b', 'k', 'r', 'g', 'c', 'm'};
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < neighbour_list[i].size(); j++) {
            var_ = "Val" + std::to_string(counter);
            file_ << var_ << "=[\n";
            for (int k = 0; k < 8; k++) {
                file_ << neighbour_list[i][j].coords[3 * k + 0] << " " << neighbour_list[i][j].coords[3 * k + 1] << " "
                      << neighbour_list[i][j].coords[3 * k + 2] << ";\n";
            }
            file_ << "];\n";
            file_ << "plot3(" << var_ << "(:,1)," << var_ << "(:,2)," << var_ << "(:,3)," << "\'" << color[i]
                  << "*\');\n";
            file_ << "dist = " << var_ << "(7,:) - " << var_ << "(1,:);" << "\n";
            file_ << "plotcube(dist," << var_ << "(1,:),0.4," << "\'" << color[i] << "\');" << "\n";
            file_ << "axis([0 1 0 1 0 1]);" << "\n";
            counter++;
        }

    }

    file_ << "xlabel('x');\n";
    file_ << "ylabel('y');\n";
    file_ << "zlabel('z');\n";

}

void interpolateXFromLowerLevel(ot::DA *da, octInfo &neighbours, octInfo &curr_oct, double *coords,
                                double *val, unsigned int side, const unsigned int ndof) {
    int child_num = da->getChildNumber();

    double _val_[8 * ndof];
    for (int i = 0; i < 8 * ndof; i++) {
        _val_[i] = val[i];
    }
    if (ndof > 1) {
        std::cout << "Wrong number of ndof. \n";
        exit(0);
    }

#if TEST
    if (side == LEFT) {
        int x = child_num % 2;
        if (x == 1) {
            std::cout << "Wrong child num for LEFT\n";
            exit(0);
        }
    }

    if (side == RIGHT) {
        int x = child_num % 2;
        if (x == 0) {
            std::cout << "Wrong child num for RIGHT\n";
            exit(0);
        }
    }
    double *_coords_ = new double[24];

    for (int k = 0; k < 24; k++) {
        _coords_[k] = neighbours.coords[k];
    }
#endif

    switch (child_num) {
        case 0:
        case 1: {
            /** Adjusting y **/
            coords[7] = curr_oct.coords[7];
            coords[10] = curr_oct.coords[10];
            coords[19] = curr_oct.coords[19];
            coords[22] = curr_oct.coords[22];

            /** Adjusting z **/
            coords[14] = curr_oct.coords[14];
            coords[17] = curr_oct.coords[17];
            coords[20] = curr_oct.coords[20];
            coords[23] = curr_oct.coords[23];


            /** Interpolating Values **/
            val[2] = 0.5 * (_val_[1] + _val_[2]);
            val[3] = 0.5 * (_val_[0] + _val_[3]);
            val[4] = 0.5 * (_val_[0] + _val_[4]);
            val[5] = 0.5 * (_val_[1] + _val_[5]);
            val[6] = 0.25 * (_val_[1] + _val_[2] + _val_[6] + _val_[5]);
            val[7] = 0.25 * (_val_[0] + _val_[3] + _val_[7] + _val_[4]);


#if TEST
            {
                bool check1 = ((EQUALS(coords[7], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check2 = ((EQUALS(coords[10], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check3 = ((EQUALS(coords[19], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                bool check4 = ((EQUALS(coords[22], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,0) or (RIGHT,1): Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[14], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[17], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[20], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[23], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,0) or (RIGHT,1) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                if (side == LEFT) {
                    bool check1 = (EQUALS(val[1], curr_oct.node_val[0]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of left from Lower level \n";
                        exit(0);
                    }
                }
                if (side == RIGHT) {
                    bool check1 = (EQUALS(val[0], curr_oct.node_val[1]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of right from Lower level \n";
                        exit(0);
                    }
                }
            }
#endif
            break;
        }

        case 2:
        case 3: {
            /** Adjusting y **/
            coords[1] = curr_oct.coords[1];
            coords[4] = curr_oct.coords[4];
            coords[13] = curr_oct.coords[13];
            coords[16] = curr_oct.coords[16];

            /** Adjusting z **/
            coords[14] = curr_oct.coords[14];
            coords[17] = curr_oct.coords[17];
            coords[20] = curr_oct.coords[20];
            coords[23] = curr_oct.coords[23];

            /** Interpolating Values **/
            val[0] = 0.5 * (_val_[0] + _val_[3]);
            val[1] = 0.5 * (_val_[1] + _val_[2]);
            val[4] = 0.25 * (_val_[0] + _val_[4] + _val_[7] + _val_[3]);
            val[5] = 0.25 * (_val_[1] + _val_[2] + _val_[5] + _val_[6]);
            val[6] = 0.5 * (_val_[2] + _val_[6]);
            val[7] = 0.5 * (_val_[3] + _val_[7]);

#if TEST
            {
                bool check1 = ((EQUALS(coords[1], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check2 = ((EQUALS(coords[4], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check3 = ((EQUALS(coords[13], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                bool check4 = ((EQUALS(coords[16], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,2) or (RIGHT,3): Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[14], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[17], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[20], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[23], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,2) or (RIGHT,4) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                if (side == LEFT) {
                    bool check1 = (EQUALS(val[2], curr_oct.node_val[3]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of left from Lower level for child = 2 \n";
                        exit(0);
                    }
                }
                if (side == RIGHT) {
                    bool check1 = (EQUALS(val[3], curr_oct.node_val[2]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of right from Lower level for child = 3 \n";
                        exit(0);
                    }
                }
            }

#endif
            break;
        }
        case 4:
        case 5: {
            /** Adjusting y **/
            coords[7] = curr_oct.coords[7];
            coords[10] = curr_oct.coords[10];
            coords[19] = curr_oct.coords[19];
            coords[22] = curr_oct.coords[22];

            /** Adjusting z **/
            coords[2] = curr_oct.coords[2];
            coords[5] = curr_oct.coords[5];
            coords[8] = curr_oct.coords[8];
            coords[11] = curr_oct.coords[11];

            /** Interpolating Values **/
            val[0] = 0.5 * (_val_[0] + _val_[3]);
            val[1] = 0.5 * (_val_[1] + _val_[2]);
            val[3] = 0.25 * (_val_[0] + _val_[4] + _val_[7] + _val_[3]);
            val[2] = 0.25 * (_val_[1] + _val_[2] + _val_[5] + _val_[6]);
            val[6] = 0.5 * (_val_[5] + _val_[6]);
            val[7] = 0.5 * (_val_[4] + _val_[7]);
#if TEST
            {
                bool check1 = ((EQUALS(coords[7], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check2 = ((EQUALS(coords[10], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check3 = ((EQUALS(coords[19], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                bool check4 = ((EQUALS(coords[22], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,4) or (RIGHT,5) : Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[2], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[5], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[8], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[11], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,4) or (RIGHT,6) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                if (side == LEFT) {
                    bool check1 = (EQUALS(val[5], curr_oct.node_val[4]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of left from Lower level for child = 4 \n";
                        exit(0);
                    }
                }
                if (side == RIGHT) {
                    bool check1 = (EQUALS(val[4], curr_oct.node_val[5]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of right from Lower level for child = 5 \n";
                        exit(0);
                    }
                }
            }
#endif
            break;
        }
        case 6:
        case 7: {
            /** Adjusting y **/
            coords[1] = curr_oct.coords[1];
            coords[4] = curr_oct.coords[4];
            coords[13] = curr_oct.coords[13];
            coords[16] = curr_oct.coords[16];

            /** Adjusting z **/
            coords[2] = curr_oct.coords[2];
            coords[5] = curr_oct.coords[5];
            coords[8] = curr_oct.coords[8];
            coords[11] = curr_oct.coords[11];

            /** Interpolating Values **/
            val[0] = 0.25 * (_val_[0] + _val_[4] + _val_[7] + _val_[3]);
            val[1] = 0.25 * (_val_[1] + _val_[2] + _val_[5] + _val_[6]);
            val[2] = 0.5 * (_val_[2] + _val_[6]);
            val[3] = 0.5 * (_val_[3] + _val_[7]);
            val[4] = 0.5 * (_val_[4] + _val_[7]);
            val[5] = 0.5 * (_val_[5] + _val_[6]);
#if TEST
            {
                bool check1 = ((EQUALS(coords[1], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check2 = ((EQUALS(coords[4], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check3 = ((EQUALS(coords[13], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                bool check4 = ((EQUALS(coords[16], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,6) or (RIGHT,7) : Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[2], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[5], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[8], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[11], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(LEFT,6) or (RIGHT,7) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                if (side == LEFT) {
                    bool check1 = (EQUALS(val[6], curr_oct.node_val[7]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of left from Lower level for child = 7 \n";
                        exit(0);
                    }
                }
                if (side == RIGHT) {
                    bool check1 = (EQUALS(val[7], curr_oct.node_val[6]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of right from Lower level for child = 6 \n";
                        exit(0);
                    }
                }
            }
#endif
            break;
        }
        default:
            std::cout << " Wrong  child number in the interpolation of X coordinates  \n";
            std::cout << " Child number = " << child_num << "\n";
            exit(0);

    }
#if TEST
    delete[]_coords_;

#endif
}


void interpolateYFromLowerLevel(ot::DA *da, octInfo &neighbours, octInfo &curr_oct, double *coords, double *val,
                                unsigned int side,
                                const unsigned int ndof) {
    int child_num = da->getChildNumber();

    double _val_[8 * ndof];
    for (int i = 0; i < 8 * ndof; i++) {
        _val_[i] = val[i];
    }

    if (ndof > 1) {
        std::cout << "Wrong number of ndof. \n";
        exit(0);
    }

#if TEST
    if (side == TOP) {
        if (!((child_num == 2) or (child_num == 3) or (child_num == 6) or (child_num == 7))) {
            std::cout << "Wrong child number for top \n";
            exit(0);
        }
    }
    if (side == BOTTOM) {
        if (!((child_num == 0) or (child_num == 1) or (child_num == 4) or (child_num == 5))) {
            std::cout << "Wrong child number for bottom \n";
            exit(0);
        }
    }
    double *_coords_ = new double[24];

    for (int k = 0; k < 24; k++) {
        _coords_[k] = neighbours.coords[k];
    }
#endif
    switch (child_num) {
        case 2:
        case 0: {
            /** Adjusting x **/
            coords[3] = curr_oct.coords[3];
            coords[6] = curr_oct.coords[6];
            coords[15] = curr_oct.coords[15];
            coords[18] = curr_oct.coords[18];

            /** Adjusting z **/
            coords[14] = curr_oct.coords[14];
            coords[17] = curr_oct.coords[17];
            coords[20] = curr_oct.coords[20];
            coords[23] = curr_oct.coords[23];

            /** Interpolating Values **/
            val[1] = 0.5 * (_val_[0] + _val_[1]);
            val[2] = 0.5 * (_val_[2] + _val_[3]);
            val[4] = 0.5 * (_val_[0] + _val_[4]);
            val[5] = 0.25 * (_val_[0] + _val_[1] + _val_[5] + _val_[4]);
            val[6] = 0.25 * (_val_[3] + _val_[2] + _val_[6] + _val_[7]);
            val[7] = 0.5 * (_val_[3] + _val_[7]);
#if TEST
            {
                bool check1 = ((EQUALS(coords[3], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[6], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[15], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[18], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,2) : Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[14], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[17], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[20], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[23], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,2) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                if (side == TOP) {
                    bool check1 = (EQUALS(val[0], curr_oct.node_val[3]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of top from Lower level for child = 2 \n";
                        exit(0);
                    }
                }
                if (side == BOTTOM) {
                    bool check1 = (EQUALS(val[3], curr_oct.node_val[0]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of bottom from Lower level for child = 0 \n";
                        exit(0);
                    }
                }
            }
#endif
            break;
        }
        case 3:
        case 1: {
            /** Adjusting x **/
            coords[0] = curr_oct.coords[0];
            coords[9] = curr_oct.coords[9];
            coords[12] = curr_oct.coords[12];
            coords[21] = curr_oct.coords[21];

            /** Adjusting z **/
            coords[14] = curr_oct.coords[14];
            coords[17] = curr_oct.coords[17];
            coords[20] = curr_oct.coords[20];
            coords[23] = curr_oct.coords[23];

            /** Interpolating Values **/
            val[0] = 0.5 * (_val_[0] + _val_[1]);
            val[3] = 0.5 * (_val_[2] + _val_[3]);
            val[4] = 0.25 * (_val_[0] + _val_[1] + _val_[5] + _val_[4]);
            val[5] = 0.5 * (_val_[1] + _val_[5]);
            val[6] = 0.5 * (_val_[2] + _val_[6]);
            val[7] = 0.25 * (_val_[3] + _val_[2] + _val_[6] + _val_[7]);
#if TEST
            {
                bool check1 = ((EQUALS(coords[0], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[9], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[12], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[21], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,3) : Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[14], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[17], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[20], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[23], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,3) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                if (side == TOP) {

                    bool check1 = (EQUALS(val[1], curr_oct.node_val[2]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of top from Lower level for child = 3 \n";
                        exit(0);
                    }
                }
                if (side == BOTTOM) {
                    bool check1 = (EQUALS(val[2], curr_oct.node_val[1]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of bottom from Lower level for child = 1 \n";
                        exit(0);
                    }
                }
            }
#endif
            break;

        }

        case 6:
        case 4: {
            /** Adjusting x **/
            coords[3] = curr_oct.coords[3];
            coords[6] = curr_oct.coords[6];
            coords[15] = curr_oct.coords[15];
            coords[18] = curr_oct.coords[18];

            /** Adjusting z **/
            coords[2] = curr_oct.coords[2];
            coords[5] = curr_oct.coords[5];
            coords[8] = curr_oct.coords[8];
            coords[11] = curr_oct.coords[11];


            /** Interpolating Values **/
            val[0] = 0.5 * (_val_[0] + _val_[4]);
            val[1] = 0.25 * (_val_[0] + _val_[1] + _val_[5] + _val_[4]);
            val[2] = 0.25 * (_val_[3] + _val_[2] + _val_[6] + _val_[7]);
            val[3] = 0.5 * (_val_[3] + _val_[7]);
            val[5] = 0.5 * (_val_[4] + _val_[5]);
            val[6] = 0.5 * (_val_[6] + _val_[7]);

#if TEST
            {
                bool check1 = ((EQUALS(coords[3], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[6], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[15], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[18], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,6) : Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[2], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[5], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[8], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[11], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,6) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                if (side == TOP) {

                    bool check1 = (EQUALS(val[4], curr_oct.node_val[7]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of top from Lower level for child = 6 \n";
                        exit(0);
                    }
                }
                if (side == BOTTOM) {
                    bool check1 = (EQUALS(val[7], curr_oct.node_val[4]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of bottom from Lower level for child = 4 \n";
                        exit(0);
                    }
                }
            }
#endif
            break;
        }
        case 7:
        case 5: {
            /** Adjusting x **/
            coords[0] = curr_oct.coords[0];
            coords[9] = curr_oct.coords[9];
            coords[12] = curr_oct.coords[12];
            coords[21] = curr_oct.coords[21];

            /** Adjusting z **/
            coords[2] = curr_oct.coords[2];
            coords[5] = curr_oct.coords[5];
            coords[8] = curr_oct.coords[8];
            coords[11] = curr_oct.coords[11];


            /** Interpolating Values **/
            val[0] = 0.25 * (_val_[0] + _val_[1] + _val_[5] + _val_[4]);
            val[1] = 0.5 * (_val_[1] + _val_[5]);
            val[2] = 0.5 * (_val_[2] + _val_[6]);
            val[3] = 0.25 * (_val_[3] + _val_[2] + _val_[6] + _val_[7]);
            val[4] = 0.5 * (_val_[4] + _val_[5]);
            val[7] = 0.5 * (_val_[6] + _val_[7]);

#if TEST
            {
                bool check1 = ((EQUALS(coords[0], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[9], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[12], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[21], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,7) : Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }

            {
                bool check1 = ((EQUALS(coords[2], 0.5 * _coords_[2] + 0.5 * _coords_[14])));
                bool check2 = ((EQUALS(coords[5], 0.5 * _coords_[5] + 0.5 * _coords_[17])));
                bool check3 = ((EQUALS(coords[8], 0.5 * _coords_[8] + 0.5 * _coords_[20])));
                bool check4 = ((EQUALS(coords[11], 0.5 * _coords_[11] + 0.5 * _coords_[23])));

                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(TOP,7) : Wrong in z interpolation of coordinates  \n";
                    exit(0);
                }
            }


            {
                if (side == TOP) {
                    bool check1 = (EQUALS(val[5], curr_oct.node_val[6]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of top from Lower level for child = 7 \n";
                        exit(0);
                    }
                }
                if (side == BOTTOM) {
                    bool check1 = (EQUALS(val[6], curr_oct.node_val[5]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of bottom from Lower level for child = 5 \n";
                        exit(0);
                    }
                }
            }
#endif
            break;

        }
        default: {
            std::cout << " Wrong  child number in the interpolation of Y coordinates  \n";
            std::cout << " Child number = " << child_num << "\n";
            exit(0);
        }


    }
#if TEST
    delete[]_coords_;

#endif
}

void interpolateZFromLowerLevel(ot::DA *da, octInfo &neighbours, octInfo &curr_oct, double *coords, double *val,
                                unsigned int side,
                                const unsigned int ndof) {
    int child_num = da->getChildNumber();
    double _val_[8 * ndof];
    for (int i = 0; i < 8 * ndof; i++) {
        _val_[i] = val[i];
    }

    if (ndof > 1) {
        std::cout << "Wrong number of ndof. \n";
        exit(0);
    }

#if TEST
    if (side == FRONT) {
        if (!((child_num == 4) or (child_num == 5) or (child_num == 6) or (child_num == 7))) {
            std::cout << "Wrong child number for front \n";
            exit(0);
        }
    }
    if (side == BACK) {
        if (!((child_num == 0) or (child_num == 1) or (child_num == 2) or (child_num == 3))) {
            std::cout << "Wrong child number for bottom \n";
            exit(0);
        }
    }
    double *_coords_ = new double[24];

    for (int k = 0; k < 24; k++) {
        _coords_[k] = neighbours.coords[k];
    }
#endif

    switch (child_num) {
        case 4:
        case 0: {
            /** Adjusting x **/
            coords[3] = curr_oct.coords[3];
            coords[6] = curr_oct.coords[6];
            coords[15] = curr_oct.coords[15];
            coords[18] = curr_oct.coords[18];

            /** Adjusting y **/
            coords[7] = curr_oct.coords[7];
            coords[10] = curr_oct.coords[10];
            coords[19] = curr_oct.coords[19];
            coords[22] = curr_oct.coords[22];

            /**Interpolating values*/
            val[1] = 0.5 * (_val_[0] + _val_[1]);
            val[2] = 0.25 * (_val_[0] + _val_[1] + _val_[2] + _val_[3]);
            val[3] = 0.5 * (_val_[0] + _val_[3]);
            val[5] = 0.5 * (_val_[4] + _val_[5]);
            val[6] = 0.25 * (_val_[4] + _val_[5] + _val_[6] + _val_[7]);
            val[7] = 0.5 * (_val_[4] + _val_[7]);

#if TEST
            {
                bool check1 = ((EQUALS(coords[7], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check2 = ((EQUALS(coords[10], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check3 = ((EQUALS(coords[19], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                bool check4 = ((EQUALS(coords[22], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,4) or (BACK,0): Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                bool check1 = ((EQUALS(coords[3], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[6], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[15], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[18], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,4) or (BACK,0): Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                if (side == FRONT) {
                    bool check1 = (EQUALS(val[0], curr_oct.node_val[4]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of front from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
                if (side == BACK) {
                    bool check1 = (EQUALS(val[4], curr_oct.node_val[0]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of back from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
            }


#endif
            break;
        }

        case 5:
        case 1: {
            /** Adjusting x **/
            coords[0] = curr_oct.coords[0];
            coords[9] = curr_oct.coords[9];
            coords[12] = curr_oct.coords[12];
            coords[21] = curr_oct.coords[21];

            /** Adjusting y **/
            coords[7] = curr_oct.coords[7];
            coords[10] = curr_oct.coords[10];
            coords[19] = curr_oct.coords[19];
            coords[22] = curr_oct.coords[22];

            /**Interpolating values*/
            val[0] = 0.5 * (_val_[0] + _val_[1]);
            val[2] = 0.5 * (_val_[1] + _val_[2]);
            val[3] = 0.25 * (_val_[0] + _val_[1] + _val_[2] + _val_[3]);
            val[4] = 0.5 * (_val_[4] + _val_[5]);
            val[6] = 0.5 * (_val_[5] + _val_[6]);
            val[7] = 0.25 * (_val_[4] + _val_[5] + _val_[6] + _val_[7]);
#if TEST
            {
                bool check1 = ((EQUALS(coords[7], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check2 = ((EQUALS(coords[10], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check3 = ((EQUALS(coords[19], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                bool check4 = ((EQUALS(coords[22], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,5) or (BACK,1): Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                bool check1 = ((EQUALS(coords[0], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[9], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[12], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[21], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,5) or (BACK,1): Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                if (side == FRONT) {
                    bool check1 = (EQUALS(val[1], curr_oct.node_val[5]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of front from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
                if (side == BACK) {
                    bool check1 = (EQUALS(val[5], curr_oct.node_val[1]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of back from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
            }

#endif
            break;
        }
        case 6:
        case 2: {
            /** Adjusting x **/
            coords[3] = curr_oct.coords[3];
            coords[6] = curr_oct.coords[6];
            coords[15] = curr_oct.coords[15];
            coords[18] = curr_oct.coords[18];

            /** Adjusting y **/
            coords[1] = curr_oct.coords[1];
            coords[4] = curr_oct.coords[4];
            coords[13] = curr_oct.coords[13];
            coords[16] = curr_oct.coords[16];

            /**Interpolating values*/
            val[0] = 0.5 * (_val_[0] + _val_[3]);
            val[1] = 0.25 * (_val_[0] + _val_[1] + _val_[2] + _val_[3]);
            val[2] = 0.5 * (_val_[2] + _val_[3]);
            val[4] = 0.5 * (_val_[4] + _val_[7]);
            val[5] = 0.25 * (_val_[4] + _val_[5] + _val_[6] + _val_[7]);
            val[6] = 0.5 * (_val_[6] + _val_[7]);

#if TEST
            {
                bool check1 = ((EQUALS(coords[3], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[6], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[15], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[18], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,6) or (BACK,2): Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                bool check1 = ((EQUALS(coords[1], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check2 = ((EQUALS(coords[4], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check3 = ((EQUALS(coords[13], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                bool check4 = ((EQUALS(coords[16], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,6) or (BACK,2) : Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                if (side == FRONT) {
                    bool check1 = (EQUALS(val[3], curr_oct.node_val[7]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of front from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
                if (side == BACK) {
                    bool check1 = (EQUALS(val[7], curr_oct.node_val[3]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of back from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
            }
#endif
            break;
        }
        case 7:
        case 3: {
            /** Adjusting x **/
            coords[0] = curr_oct.coords[0];
            coords[9] = curr_oct.coords[9];
            coords[12] = curr_oct.coords[12];
            coords[21] = curr_oct.coords[21];

            /** Adjusting y **/
            coords[1] = curr_oct.coords[1];
            coords[4] = curr_oct.coords[4];
            coords[13] = curr_oct.coords[13];
            coords[16] = curr_oct.coords[16];

            /**Interpolating values*/
            val[0] = 0.25 * (_val_[0] + _val_[1] + _val_[2] + _val_[3]);
            val[1] = 0.5 * (_val_[1] + _val_[2]);
            val[3] = 0.5 * (_val_[2] + _val_[3]);
            val[4] = 0.25 * (_val_[4] + _val_[5] + _val_[6] + _val_[7]);
            val[5] = 0.5 * (_val_[6] + _val_[5]);
            val[7] = 0.5 * (_val_[6] + _val_[7]);
#if TEST
            {
                bool check1 = ((EQUALS(coords[0], 0.5 * _coords_[0] + 0.5 * _coords_[3])));
                bool check2 = ((EQUALS(coords[9], 0.5 * _coords_[9] + 0.5 * _coords_[6])));
                bool check3 = ((EQUALS(coords[12], 0.5 * _coords_[12] + 0.5 * _coords_[15])));
                bool check4 = ((EQUALS(coords[21], 0.5 * _coords_[21] + 0.5 * _coords_[18])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,7) or (BACK,3): Wrong in x interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                bool check1 = ((EQUALS(coords[1], 0.5 * _coords_[1] + 0.5 * _coords_[10])));
                bool check2 = ((EQUALS(coords[4], 0.5 * _coords_[4] + 0.5 * _coords_[7])));
                bool check3 = ((EQUALS(coords[13], 0.5 * _coords_[13] + 0.5 * _coords_[22])));
                bool check4 = ((EQUALS(coords[16], 0.5 * _coords_[16] + 0.5 * _coords_[19])));
                if (not(check1 and check2 and check3 and check4)) {
                    std::cout << "(FRONT,7) or (BACK,3) : Wrong in y interpolation of coordinates  \n";
                    exit(0);
                }
            }
            {
                if (side == FRONT) {
                    bool check1 = (EQUALS(val[2], curr_oct.node_val[6]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of front from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
                if (side == BACK) {
                    bool check1 = (EQUALS(val[6], curr_oct.node_val[2]));
                    if (not(check1)) {
                        std::cout << "Wrong in value interpolation of back from Lower level for child =  " << child_num
                                  << "\n";
                        exit(0);
                    }
                }
            }
#endif
            break;
        }
        default: {
            std::cout << " Wrong  child number in the interpolation of Z coordinates  \n";
            std::cout << " Child number = " << child_num << "\n";
            exit(0);
        }


    }
#if TEST
    delete[]_coords_;

#endif
}


void interpolateXFromHigherLevel(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords,
                                 double *val,
                                 unsigned int side, const unsigned int ndof) {
#if TEST

    if (side == RIGHT) {
        bool check1 = ((EQUALS(neighbours[0].coords[0], curr_oct.coords[3])) and
                       (EQUALS(neighbours[0].coords[1], curr_oct.coords[4])) and
                       (EQUALS(neighbours[0].coords[2], curr_oct.coords[5])));
        bool check2 = ((EQUALS(neighbours[1].coords[9], curr_oct.coords[6])) and
                       (EQUALS(neighbours[1].coords[10], curr_oct.coords[7])) and
                       (EQUALS(neighbours[1].coords[11], curr_oct.coords[8])));
        bool check3 = ((EQUALS(neighbours[2].coords[12], curr_oct.coords[15])) and
                       (EQUALS(neighbours[2].coords[13], curr_oct.coords[16])) and
                       (EQUALS(neighbours[2].coords[14], curr_oct.coords[17])));
        bool check4 = ((EQUALS(neighbours[3].coords[21], curr_oct.coords[18])) and
                       (EQUALS(neighbours[3].coords[22], curr_oct.coords[19])) and
                       (EQUALS(neighbours[3].coords[23], curr_oct.coords[20])));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in Right interpolating to higher level = " << check1 << " " << check2 << " " << check3
                      << " " << check4 << "\n";
            exit(0);
        }


    }

    if (side == LEFT) {

        bool check1 = ((EQUALS(curr_oct.coords[0], neighbours[0].coords[3])) and
                       (EQUALS(curr_oct.coords[1], neighbours[0].coords[4])) and
                       (EQUALS(curr_oct.coords[2], neighbours[0].coords[5])));
        bool check2 = ((EQUALS(curr_oct.coords[9], neighbours[1].coords[6])) and
                       (EQUALS(curr_oct.coords[10], neighbours[1].coords[7])) and
                       (EQUALS(curr_oct.coords[11], neighbours[1].coords[8])));
        bool check3 = ((EQUALS(curr_oct.coords[12], neighbours[2].coords[15])) and
                       (EQUALS(curr_oct.coords[13], neighbours[2].coords[16])) and
                       (EQUALS(curr_oct.coords[14], neighbours[2].coords[17])));
        bool check4 = ((EQUALS(curr_oct.coords[21], neighbours[3].coords[18])) and
                       (EQUALS(curr_oct.coords[22], neighbours[3].coords[19])) and
                       (EQUALS(curr_oct.coords[23], neighbours[3].coords[20])));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in Left interpolating to higher level = " << check1 << " " << check2 << " " << check3
                      << " " << check4 << "\n";
            exit(0);
        }
    }

#endif

    coords[0] = neighbours[0].coords[0];
    coords[1] = neighbours[0].coords[1];
    coords[2] = neighbours[0].coords[2];
    coords[3] = neighbours[0].coords[3];
    coords[4] = neighbours[0].coords[4];
    coords[5] = neighbours[0].coords[5];

    coords[6] = neighbours[1].coords[6];
    coords[7] = neighbours[1].coords[7];
    coords[8] = neighbours[1].coords[8];
    coords[9] = neighbours[1].coords[9];
    coords[10] = neighbours[1].coords[10];
    coords[11] = neighbours[1].coords[11];

    coords[12] = neighbours[2].coords[12];
    coords[13] = neighbours[2].coords[13];
    coords[14] = neighbours[2].coords[14];
    coords[15] = neighbours[2].coords[15];
    coords[16] = neighbours[2].coords[16];
    coords[17] = neighbours[2].coords[17];

    coords[18] = neighbours[3].coords[18];
    coords[19] = neighbours[3].coords[19];
    coords[20] = neighbours[3].coords[20];
    coords[21] = neighbours[3].coords[21];
    coords[22] = neighbours[3].coords[22];
    coords[23] = neighbours[3].coords[23];

    val[0] = neighbours[0].node_val[0];
    val[1] = neighbours[0].node_val[1];
    val[2] = neighbours[1].node_val[2];
    val[3] = neighbours[1].node_val[3];
    val[4] = neighbours[2].node_val[4];
    val[5] = neighbours[2].node_val[5];
    val[6] = neighbours[3].node_val[6];
    val[7] = neighbours[3].node_val[7];

#if TEST
    if (side == LEFT) {
        bool check1 = (EQUALS(val[1], curr_oct.node_val[0]));
        bool check2 = (EQUALS(val[2], curr_oct.node_val[3]));
        bool check3 = (EQUALS(val[5], curr_oct.node_val[4]));
        bool check4 = (EQUALS(val[6], curr_oct.node_val[7]));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in value interpolation of left from Higher level for child \n";
            exit(0);
        }
    }
    if (side == RIGHT) {
        bool check1 = (EQUALS(val[0], curr_oct.node_val[1]));
        bool check2 = (EQUALS(val[3], curr_oct.node_val[2]));
        bool check3 = (EQUALS(val[4], curr_oct.node_val[5]));
        bool check4 = (EQUALS(val[7], curr_oct.node_val[6]));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in value interpolation of right from Higher level for child =  \n";
            exit(0);
        }
    }
#endif

}

void interpolateYFromHigherLevel(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords,
                                 double *val,
                                 unsigned int side, const unsigned int ndof) {
#if TEST

    if (side == TOP) {
        bool check1 = ((EQUALS(neighbours[0].coords[0], curr_oct.coords[9])) and
                       (EQUALS(neighbours[0].coords[1], curr_oct.coords[10])) and
                       (EQUALS(neighbours[0].coords[2], curr_oct.coords[11])));
        bool check2 = ((EQUALS(neighbours[1].coords[3], curr_oct.coords[6])) and
                       (EQUALS(neighbours[1].coords[4], curr_oct.coords[7])) and
                       (EQUALS(neighbours[1].coords[5], curr_oct.coords[8])));
        bool check3 = ((EQUALS(neighbours[2].coords[12], curr_oct.coords[21])) and
                       (EQUALS(neighbours[2].coords[13], curr_oct.coords[22])) and
                       (EQUALS(neighbours[2].coords[14], curr_oct.coords[23])));
        bool check4 = ((EQUALS(neighbours[3].coords[15], curr_oct.coords[18])) and
                       (EQUALS(neighbours[3].coords[16], curr_oct.coords[19])) and
                       (EQUALS(neighbours[3].coords[17], curr_oct.coords[20])));

        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in Top interpolating to higher level = " << check1 << " " << check2 << " " << check3
                      << " " << check4 << "\n";
            exit(0);
        }


    }

    if (side == BOTTOM) {
        bool check1 = ((EQUALS(curr_oct.coords[0], neighbours[0].coords[9])) and
                       (EQUALS(curr_oct.coords[1], neighbours[0].coords[10])) and
                       (EQUALS(curr_oct.coords[2], neighbours[0].coords[11])));
        bool check2 = ((EQUALS(curr_oct.coords[3], neighbours[1].coords[6])) and
                       (EQUALS(curr_oct.coords[4], neighbours[1].coords[7])) and
                       (EQUALS(curr_oct.coords[5], neighbours[1].coords[8])));
        bool check3 = ((EQUALS(curr_oct.coords[12], neighbours[2].coords[21])) and
                       (EQUALS(curr_oct.coords[13], neighbours[2].coords[22])) and
                       (EQUALS(curr_oct.coords[14], neighbours[2].coords[23])));
        bool check4 = ((EQUALS(curr_oct.coords[15], neighbours[3].coords[18])) and
                       (EQUALS(curr_oct.coords[16], neighbours[3].coords[19])) and
                       (EQUALS(curr_oct.coords[17], neighbours[3].coords[20])));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in Bottom interpolating to higher level = " << check1 << " " << check2 << " " << check3
                      << " " << check4 << "\n";
            exit(0);
        }


    }

#endif
    coords[0] = neighbours[0].coords[0];
    coords[1] = neighbours[0].coords[1];
    coords[2] = neighbours[0].coords[2];

    coords[3] = neighbours[1].coords[3];
    coords[4] = neighbours[1].coords[4];
    coords[5] = neighbours[1].coords[5];

    coords[6] = neighbours[1].coords[6];
    coords[7] = neighbours[1].coords[7];
    coords[8] = neighbours[1].coords[8];

    coords[9] = neighbours[0].coords[9];
    coords[10] = neighbours[0].coords[10];
    coords[11] = neighbours[0].coords[11];

    coords[12] = neighbours[2].coords[12];
    coords[13] = neighbours[2].coords[13];
    coords[14] = neighbours[2].coords[14];

    coords[15] = neighbours[3].coords[15];
    coords[16] = neighbours[3].coords[16];
    coords[17] = neighbours[3].coords[17];

    coords[18] = neighbours[3].coords[18];
    coords[19] = neighbours[3].coords[19];
    coords[20] = neighbours[3].coords[20];

    coords[21] = neighbours[2].coords[21];
    coords[22] = neighbours[2].coords[22];
    coords[23] = neighbours[2].coords[23];

    val[0] = neighbours[0].node_val[0];
    val[1] = neighbours[1].node_val[1];
    val[2] = neighbours[1].node_val[2];
    val[3] = neighbours[0].node_val[3];

    val[4] = neighbours[2].node_val[4];
    val[5] = neighbours[3].node_val[5];
    val[6] = neighbours[3].node_val[6];
    val[7] = neighbours[2].node_val[7];
#if TEST
    if (side == TOP) {
        bool check1 = (EQUALS(val[0], curr_oct.node_val[3]));
        bool check2 = (EQUALS(val[1], curr_oct.node_val[2]));
        bool check3 = (EQUALS(val[4], curr_oct.node_val[7]));
        bool check4 = (EQUALS(val[5], curr_oct.node_val[6]));

        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in value interpolation of bottom from Higher level for child \n";
            exit(0);
        }
    }
    if (side == BOTTOM) {
        bool check1 = (EQUALS(val[3], curr_oct.node_val[0]));
        bool check2 = (EQUALS(val[2], curr_oct.node_val[1]));
        bool check3 = (EQUALS(val[7], curr_oct.node_val[4]));
        bool check4 = (EQUALS(val[6], curr_oct.node_val[5]));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in value interpolation of top from Higher level for child =  \n";
            exit(0);
        }
    }
#endif

}

void interpolateZFromHigherLevel(ot::DA *da, std::vector<octInfo> &neighbours, octInfo &curr_oct, double *coords,
                                 double *val,
                                 unsigned int side, const unsigned int ndof) {
#if TEST

    if (side == FRONT) {
        bool check1 = ((EQUALS(neighbours[0].coords[0], curr_oct.coords[12])) and
                       (EQUALS(neighbours[0].coords[1], curr_oct.coords[13])) and
                       (EQUALS(neighbours[0].coords[2], curr_oct.coords[14])));
        bool check2 = ((EQUALS(neighbours[1].coords[3], curr_oct.coords[15])) and
                       (EQUALS(neighbours[1].coords[4], curr_oct.coords[16])) and
                       (EQUALS(neighbours[1].coords[5], curr_oct.coords[17])));
        bool check3 = ((EQUALS(neighbours[3].coords[6], curr_oct.coords[18])) and
                       (EQUALS(neighbours[3].coords[7], curr_oct.coords[19])) and
                       (EQUALS(neighbours[3].coords[8], curr_oct.coords[20])));
        bool check4 = ((EQUALS(neighbours[2].coords[9], curr_oct.coords[21])) and
                       (EQUALS(neighbours[2].coords[10], curr_oct.coords[22])) and
                       (EQUALS(neighbours[2].coords[11], curr_oct.coords[23])));

        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in Front interpolating to higher level = " << check1 << " " << check2 << " " << check3
                      << " " << check4 << "\n";
            exit(0);
        }


    }

    if (side == BACK) {
        bool check1 = ((EQUALS(curr_oct.coords[0], neighbours[0].coords[12])) and
                       (EQUALS(curr_oct.coords[1], neighbours[0].coords[13])) and
                       (EQUALS(curr_oct.coords[2], neighbours[0].coords[14])));
        bool check2 = ((EQUALS(curr_oct.coords[3], neighbours[1].coords[15])) and
                       (EQUALS(curr_oct.coords[4], neighbours[1].coords[16])) and
                       (EQUALS(curr_oct.coords[5], neighbours[1].coords[17])));
        bool check3 = ((EQUALS(curr_oct.coords[6], neighbours[3].coords[18])) and
                       (EQUALS(curr_oct.coords[7], neighbours[3].coords[19])) and
                       (EQUALS(curr_oct.coords[8], neighbours[3].coords[20])));
        bool check4 = ((EQUALS(curr_oct.coords[9], neighbours[2].coords[21])) and
                       (EQUALS(curr_oct.coords[10], neighbours[2].coords[22])) and
                       (EQUALS(curr_oct.coords[11], neighbours[2].coords[23])));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in Back interpolating to higher level = " << check1 << " " << check2 << " " << check3
                      << " " << check4 << "\n";
            exit(0);
        }


    }

#endif
    coords[0] = neighbours[0].coords[0];
    coords[1] = neighbours[0].coords[1];
    coords[2] = neighbours[0].coords[2];

    coords[3] = neighbours[1].coords[3];
    coords[4] = neighbours[1].coords[4];
    coords[5] = neighbours[1].coords[5];

    coords[6] = neighbours[3].coords[6];
    coords[7] = neighbours[3].coords[7];
    coords[8] = neighbours[3].coords[8];

    coords[9] = neighbours[2].coords[9];
    coords[10] = neighbours[2].coords[10];
    coords[11] = neighbours[2].coords[11];

    coords[12] = neighbours[0].coords[12];
    coords[13] = neighbours[0].coords[13];
    coords[14] = neighbours[0].coords[14];

    coords[15] = neighbours[1].coords[15];
    coords[16] = neighbours[1].coords[16];
    coords[17] = neighbours[1].coords[17];

    coords[18] = neighbours[3].coords[18];
    coords[19] = neighbours[3].coords[19];
    coords[20] = neighbours[3].coords[20];

    coords[21] = neighbours[2].coords[21];
    coords[22] = neighbours[2].coords[22];
    coords[23] = neighbours[2].coords[23];

    val[0] = neighbours[0].node_val[0];
    val[1] = neighbours[1].node_val[1];
    val[2] = neighbours[3].node_val[2];
    val[3] = neighbours[2].node_val[3];
    val[4] = neighbours[0].node_val[4];
    val[5] = neighbours[1].node_val[5];
    val[6] = neighbours[3].node_val[6];
    val[7] = neighbours[2].node_val[7];
#if TEST

    if (side == FRONT) {
        bool check1 = (EQUALS(val[0], curr_oct.node_val[4]));
        bool check2 = (EQUALS(val[1], curr_oct.node_val[5]));
        bool check3 = (EQUALS(val[2], curr_oct.node_val[6]));
        bool check4 = (EQUALS(val[3], curr_oct.node_val[7]));

        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in value interpolation of front from Higher level for child \n";
            exit(0);
        }
    }
    if (side == BACK) {
        bool check1 = (EQUALS(val[4], curr_oct.node_val[0]));
        bool check2 = (EQUALS(val[5], curr_oct.node_val[1]));
        bool check3 = (EQUALS(val[6], curr_oct.node_val[2]));
        bool check4 = (EQUALS(val[7], curr_oct.node_val[3]));
        if (not(check1 and check2 and check3 and check4)) {
            std::cout << "Wrong in value interpolation of back from Higher level for child   \n";
            exit(0);
        }
    }
#endif
}

#if TEST

/*************************************************** Testing FE utils *****************************************/

bool isequal(TALYFEMLIB::ZEROPTV &coord1, TALYFEMLIB::ZEROPTV &coord2) {
    if ((EQUALS(coord1.x(), coord2.x())) and (EQUALS(coord1.x(), coord2.x())) and (EQUALS(coord1.x(), coord2.x()))) {
        return true;
    } else {
        return false;
    }
}

void check_left(TALYFEMLIB::FEMElm &fe_curr, TALYFEMLIB::FEMElm &fe_left, double *curr, double *left) {
    using namespace TALYFEMLIB;
    const GRID *grid_curr = fe_curr.grid();
    const GRID *grid_left = fe_left.grid();

    ZEROPTV coords_curr, coords_left;

    grid_curr->GetCoord(coords_curr, 0);
    grid_left->GetCoord(coords_left, 1);
    if (not(isequal(coords_curr, coords_left))) {
        std::cout << "Case 1 : Wrong coordinates in left \n";
        exit(0);
    }

    grid_curr->GetCoord(coords_curr, 3);
    grid_left->GetCoord(coords_left, 2);
    if (not(isequal(coords_curr, coords_left))) {
        std::cout << "Case 2 : Wrong coordinates in left \n";
        exit(0);
    }

    grid_curr->GetCoord(coords_curr, 4);
    grid_left->GetCoord(coords_left, 5);
    if (not(isequal(coords_curr, coords_left))) {
        std::cout << "Case 3 : Wrong coordinates in left \n";
        exit(0);
    }

    grid_curr->GetCoord(coords_curr, 7);
    grid_left->GetCoord(coords_left, 6);
    if (not(isequal(coords_curr, coords_left))) {
        std::cout << "Case 4 : Wrong coordinates in left \n";
        exit(0);
    }


//    if(not((EQUALS(curr[0],left[1])) and (EQUALS(curr[3],left[2])) and (EQUALS(curr[4],left[5])) and (EQUALS(curr[7],left[6])))){
//        std::cout << "Error in interpolation of values of left \n";
//        exit(0);
//
//    }


}

void check_right(TALYFEMLIB::FEMElm &fe_curr, TALYFEMLIB::FEMElm &fe_right, double *curr, double *right) {
    using namespace TALYFEMLIB;
    const GRID *grid_curr = fe_curr.grid();
    const GRID *grid_right = fe_right.grid();

    ZEROPTV coords_curr, coords_right;

    grid_curr->GetCoord(coords_curr, 1);
    grid_right->GetCoord(coords_right, 0);
    if (not(isequal(coords_curr, coords_right))) {
        std::cout << "Case 1 : Wrong coordinates in right \n";
        exit(0);
    }

    grid_curr->GetCoord(coords_curr, 2);
    grid_right->GetCoord(coords_right, 3);
    if (not(isequal(coords_curr, coords_right))) {
        std::cout << "Case 2 : Wrong coordinates in right \n";
        exit(0);
    }

    grid_curr->GetCoord(coords_curr, 5);
    grid_right->GetCoord(coords_right, 4);
    if (not(isequal(coords_curr, coords_right))) {
        std::cout << "Case 3 : Wrong coordinates in right \n";
        exit(0);
    }

    grid_curr->GetCoord(coords_curr, 6);
    grid_right->GetCoord(coords_right, 7);
    if (not(isequal(coords_curr, coords_right))) {
        std::cout << "Case 4 : Wrong coordinates in left \n";
        exit(0);
    }


//    if(not((EQUALS(curr[1],right[0])) and (EQUALS(curr[2],right[3])) and (EQUALS(curr[5],right[4])) and (EQUALS(curr[6],right[7])))){
//        std::cout << "Error in interpolation of values of right \n";
//        exit(0);
//
//    }


}

void check_top(TALYFEMLIB::FEMElm &fe_curr, TALYFEMLIB::FEMElm &fe_top, double *curr, double *top) {
    using namespace TALYFEMLIB;
    const int curr_id[4]{2, 3, 6, 7};
    const int top_id[4]{1, 0, 5, 4};
    ZEROPTV coords_curr, coords_top;
    const GRID *grid_curr = fe_curr.grid();
    const GRID *grid_top = fe_top.grid();

    for (int i = 0; i < 4; i++) {
        grid_curr->GetCoord(coords_curr, curr_id[i]);
        grid_top->GetCoord(coords_top, top_id[i]);
        if (not(isequal(coords_curr, coords_top))) {
            std::cout << "Case " << i << ": Wrong coordinates in top \n";
            exit(0);
        }
//        if(not (EQUALS(curr[curr_id[i]],top[top_id[i]]))){
//            std::cout << "Case " << i << ": Wrong interpolation in top \n";
//            exit(0);
//        }
    }
}

void check_bottom(TALYFEMLIB::FEMElm &fe_curr, TALYFEMLIB::FEMElm &fe_bottom, double *curr, double *bottom) {
    using namespace TALYFEMLIB;
    const int curr_id[4]{1, 0, 5, 4};
    const int bottom_id[4]{2, 3, 6, 7};

    ZEROPTV coords_curr, coords_bottom;
    const GRID *grid_curr = fe_curr.grid();
    const GRID *grid_bottom = fe_bottom.grid();

    for (int i = 0; i < 4; i++) {
        grid_curr->GetCoord(coords_curr, curr_id[i]);
        grid_bottom->GetCoord(coords_bottom, bottom_id[i]);
        if (not(isequal(coords_curr, coords_bottom))) {
            std::cout << "Case " << i << ": Wrong coordinates in bottom \n";
            exit(0);
        }
//        if(not (EQUALS(curr[curr_id[i]],bottom[bottom_id[i]]))){
//            std::cout << "Case " << i << ": Wrong interpolation in bottom\n";
//            exit(0);
//        }
    }
}

bool check_front(TALYFEMLIB::FEMElm &fe_curr, TALYFEMLIB::FEMElm &fe_front, double *curr, double *front) {
    using namespace TALYFEMLIB;
    const int curr_id[4]{4, 5, 6, 7};
    const int front_id[4]{0, 1, 2, 3};

    ZEROPTV coords_curr, coords_front;
    const GRID *grid_curr = fe_curr.grid();
    const GRID *grid_front = fe_front.grid();

    for (int i = 0; i < 4; i++) {
        grid_curr->GetCoord(coords_curr, curr_id[i]);
        grid_front->GetCoord(coords_front, front_id[i]);
        if (not(isequal(coords_curr, coords_front))) {
            std::cout << "Case " << i << ": Wrong coordinates in front \n";
            return (false);
        }
//        if(not (EQUALS(curr[curr_id[i]],front[front_id[i]]))){
//            std::cout << "Case " << i << ": Wrong interpolation in front\n";
//            return (false);
//        }
    }
    return (true);
}

void check_back(TALYFEMLIB::FEMElm &fe_curr, TALYFEMLIB::FEMElm &fe_back, double *curr, double *back) {
    using namespace TALYFEMLIB;
    const int curr_id[4]{0, 1, 2, 3};

    const int back_id[4]{4, 5, 6, 7};

    ZEROPTV coords_curr, coords_front;
    const GRID *grid_curr = fe_curr.grid();
    const GRID *grid_front = fe_back.grid();

    for (int i = 0; i < 4; i++) {
        grid_curr->GetCoord(coords_curr, curr_id[i]);
        grid_front->GetCoord(coords_front, back_id[i]);
        if (not(isequal(coords_curr, coords_front))) {
            std::cout << "Case " << i << ": Wrong coordinates in back \n";
            exit(0);
        }
//        if(not (EQUALS(curr[curr_id[i]],back[back_id[i]]))){
//            std::cout << curr[curr_id[i]] << " " << back[back_id[i]] << "\n";
//            std::cout << "Case " << i << ": Wrong interpolation in back\n";
//            exit(0);
//        }
    }
}


#endif