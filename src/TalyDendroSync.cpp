#include <dendrite/TalyDendroSync.h>

TalyDendroSync::TalyDendroSync()
    : m_octDA(NULL), vec_buff_(NULL), node_data_temp_(NULL), octToPhysScale{NAN, NAN, NAN}
{
}

TalyDendroSync::~TalyDendroSync() {
  releaseBuffers();
}

void TalyDendroSync::setVectors(const std::vector<VecInfo> &vecs) {
  assert (!haveBuffers());

  // validate
  for (unsigned int i = 0; i < vecs.size(); i++) {
    assert ((vecs[i].vec != NULL && vecs[i].placeholder == PLACEHOLDER_NONE)
        || (vecs[i].vec == NULL && vecs[i].placeholder != PLACEHOLDER_NONE));
  }

  vecs_ = vecs;
}

void TalyDendroSync::getBuffers(ot::DA *octDA, double m_dLx, double m_dLy, double m_dLz) {
  assert (!haveBuffers());

  m_octDA = octDA;

  // fill vec_buff_ with pointers to the data for each vector in vecs_
  vec_buff_ = new PetscScalar*[vecs_.size()];
  unsigned int maxdof = 0;
  for (unsigned int i = 0; i < vecs_.size(); i++) {
    if (vecs_[i].vec == NULL) {
      TALYFEMLIB::PrintWarning("TalyDendroSync vector ", i, " has placeholder ",  vecs_[i].placeholder,
                               ", but the placeholder was never set to anything.");
      vec_buff_[i] = NULL;
      continue;
    }

    octDA->vecGetBuffer(vecs_[i].vec, vec_buff_[i], false, false, vecs_[i].isReadOnly, vecs_[i].ndof);
//    for(int k = 0; k < 27; k++){
//      std::cout << k << " "<< vec_buff_[i][k] << "\n";
//    }

    // read ghosts (no interleaved communication)
    octDA->ReadFromGhostsBegin(vec_buff_[i], vecs_[i].ndof);
    octDA->ReadFromGhostsEnd(vec_buff_[i]);

    maxdof = std::max(maxdof, vecs_[i].ndof);
  }

  // allocate scratch space for reordering
  assert(node_data_temp_ == NULL);
  node_data_temp_ = new PetscScalar[8*maxdof];

  // set up xFac/yFac/zFac (converts integer octree coordinates to physical coordinates)
  const unsigned int maxD = octDA->getMaxDepth();
  octToPhysScale[0] = m_dLx / ((PetscScalar) (1 << (maxD - 1)));
  octToPhysScale[1] = m_dLy / ((PetscScalar) (1 << (maxD - 1)));
  octToPhysScale[2] = m_dLz / ((PetscScalar) (1 << (maxD - 1)));
}

void TalyDendroSync::releaseBuffers() {
  if (!haveBuffers())
    return;

  // clean up vec_buff_
  for (unsigned int i = 0; i < vecs_.size(); i++) {
    if (vec_buff_[i] != NULL)
      m_octDA->vecRestoreBuffer(vecs_[i].vec, vec_buff_[i], false, false, vecs_[i].isReadOnly, vecs_[i].ndof);
  }

  delete[] vec_buff_;
  vec_buff_ = NULL;

  delete[] node_data_temp_;
  node_data_temp_ = NULL;
}

void TalyDendroSync::setPlaceholder(VecPlaceholder p, Vec v) {
  assert (!haveBuffers());
  assert(p == PLACEHOLDER_GUESS);
  for (unsigned int i = 0; i < vecs_.size(); i++) {
    if (vecs_[i].placeholder == p) {
        
      vecs_[i].vec = v;
    }
  }
  
}
