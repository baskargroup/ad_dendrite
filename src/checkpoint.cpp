#include <dendrite/checkpoint.h>
#include <dendrite/DendroIO.h>
#include <dendrite/TimeInfo.h>

#include <libconfig.h++>

void Checkpointer::checkpoint(const std::vector<ot::TreeNode>& tree_nodes,
                              const std::vector<Vec>& vecs, const TimeInfo* ti) {
  current_checkpoint_++;

  // only write files every frequency_ calls to checkpoint()
  if (current_checkpoint_ % frequency_ != 0)
    return;

  shift_backups();

  auto filenames = get_backup_names(0);
  std::string filename_oct = filenames[0];
  std::string filename_vec = filenames[1];
	std::string filename_cfg = filenames[2];

  write_octree(filename_oct, tree_nodes, comm_);
  write_values(filename_vec, vecs, comm_);
  write_cfg(filename_cfg, ti, comm_);
}

void Checkpointer::load(ot::DA** da_out, std::vector<Vec*> vecs_out, TimeInfo* ti_out) {
  auto filenames = get_backup_names(0);
  std::string filename_oct = filenames[0];
  std::string filename_vec = filenames[1];
	std::string filename_cfg = filenames[2];

  std::vector<ot::TreeNode> nodes = read_octree(filename_oct, comm_);
  *da_out = new ot::DA(nodes, comm_, comm_, 0.0);

  read_values(filename_vec, vecs_out, comm_);
	read_cfg(filename_cfg, ti_out, comm_);
}

void Checkpointer::shift_backups() {
  int ierr, rank;
  MPI_Comm_rank(comm_, &rank);

  if (rank == 0 && n_backups_ > 0) {
    for (int i = n_backups_; i > 0; i--) {
      auto old_filenames = get_backup_names(i - 1);
      auto new_filenames = get_backup_names(i);
      for (int j = 0; j < old_filenames.size(); j++) {
      	ierr = access(old_filenames[j].c_str(), F_OK);
        if (ierr == 0 || errno != ENOENT) {
		      ierr = rename (old_filenames[j].c_str (), new_filenames[j].c_str ());
		      if (ierr != 0) {
			      // there was an error moving the backup (and the error isn't that old filename doesn't exist)
			      std::cerr << "Could not rename backup '" << old_filenames[j]
			                << "' to '" << new_filenames[j] << "' (errno: " << errno << ")" << std::endl;
			      MPI_Abort(comm_, ierr);
		      }
	      }
      }
    }
  }

  MPI_Barrier(comm_);
}

std::vector<std::string> Checkpointer::get_backup_names(int i) const {
  if (i == 0) {
    return {file_prefix_ + "_octree.dat", file_prefix_ + "_values.dat", file_prefix_ + "_state.dat"};
  } else {
    return {file_prefix_ + "_octree.dat.bak" + std::to_string(i),
            file_prefix_ + "_values.dat.bak" + std::to_string(i),
		        file_prefix_ + "_state.dat.bak" + std::to_string(i)};
  }
}

const MPI_Offset VALUES_HEADER_SIZE = sizeof (int) + sizeof (PetscInt);

void Checkpointer::write_values(const std::string& filename, const std::vector<Vec>& vecs, MPI_Comm comm)
{
  int ierr, rank, size;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);

  MPI_File file;
  ierr = MPI_File_open(comm, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &file);
  if (ierr) {
    throw std::runtime_error("Could not open values file for writing.");
  }
  MPI_Barrier(comm);

  for (unsigned int vec_idx = 0; vec_idx < vecs.size(); vec_idx++)
  {
    Vec vec = vecs.at(vec_idx);

    MPI_Offset header_start;
    ierr = MPI_File_get_position_shared(file, &header_start);  CHKERRABORT (comm, ierr);

    // write header
    if (rank == 0)
    {
	    ierr = MPI_File_seek(file, header_start, MPI_SEEK_SET);  CHKERRABORT (comm, ierr);

      // write number of procs
      ierr = MPI_File_write (file, &size, 1, MPI_INT, MPI_STATUS_IGNORE);
      CHKERRABORT (comm, ierr);

      // write total n rows
      PetscInt total_n_rows;
      ierr = VecGetSize (vec, &total_n_rows);
      CHKERRABORT (comm, ierr);
      ierr = MPI_File_write (file, &total_n_rows, 1, MPIU_INT, MPI_STATUS_IGNORE);
      CHKERRABORT (comm, ierr);

      // TODO write blocksize

      // verify header size is correct
      MPI_Offset header_end;
      MPI_File_get_position (file, &header_end);
      assert (header_end - header_start == VALUES_HEADER_SIZE);
    }

    // move shared file pointer past the header
    ierr = MPI_File_seek_shared (file, VALUES_HEADER_SIZE, MPI_SEEK_CUR);
    CHKERRABORT (comm, ierr);

    // write number of rows on this proc
    PetscInt my_n_rows;
    ierr = VecGetLocalSize (vec, &my_n_rows);
    CHKERRABORT (comm, ierr);

    ierr = MPI_File_write_ordered (file, &my_n_rows, 1, MPIU_INT, MPI_STATUS_IGNORE);
    CHKERRABORT (comm, ierr);

    // write data
    const PetscScalar* vec_buff;
    ierr = VecGetArrayRead(vec, &vec_buff);
    CHKERRABORT (comm, ierr);
    ierr = MPI_File_write_ordered (file, vec_buff, my_n_rows, MPIU_SCALAR, MPI_STATUS_IGNORE);
    CHKERRABORT (comm, ierr);
    ierr = VecRestoreArrayRead(vec, &vec_buff);
    CHKERRABORT(comm, ierr);
  }

  ierr = MPI_File_close(&file);  CHKERRABORT (comm, ierr);
}

void Checkpointer::read_values(const std::string& filename, const std::vector<Vec*>& vecs_out, MPI_Comm comm)
{
  int ierr, rank, size;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &size);

  MPI_File file;
  ierr = MPI_File_open(comm, filename.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &file);
  if (ierr)
    throw std::runtime_error("Could not open values file for reading.");

  for (unsigned int vec_idx = 0; vec_idx < vecs_out.size(); vec_idx++)
  {
    int file_nprocs;
    PetscInt n_total_rows;

    // read header
    if (rank == 0)
    {
      ierr = MPI_File_read_shared(file, &file_nprocs, 1, MPI_INT, MPI_STATUS_IGNORE);
      CHKERRABORT (comm, ierr);
      ierr = MPI_File_read_shared(file, &n_total_rows, 1, MPIU_INT, MPI_STATUS_IGNORE);
      CHKERRABORT (comm, ierr);
    }
    MPI_Bcast(&file_nprocs, 1, MPI_INT, 0, comm);
    MPI_Bcast(&n_total_rows, 1, MPIU_INT, 0, comm);

    if (file_nprocs != size)
    {
      throw std::runtime_error (
          "Checkpoint file was written with " + std::to_string (file_nprocs)
          + " procs, but we are running with " + std::to_string (size)
          + " (vec idx " + std::to_string(vec_idx) + ")");
    }

    // read number of rows for this proc
    PetscInt my_n_rows;
    ierr = MPI_File_read_ordered (file, &my_n_rows, 1, MPIU_INT, MPI_STATUS_IGNORE);
    CHKERRABORT(comm, ierr);

    // create vector
    ierr = VecCreate(comm, vecs_out[vec_idx]);  CHKERRABORT (comm, ierr);
    ierr = VecSetType(*vecs_out[vec_idx], VECMPI);  CHKERRABORT (comm, ierr);
    ierr = VecSetSizes(*vecs_out[vec_idx], my_n_rows, n_total_rows);  CHKERRABORT (comm, ierr);

    // read data
    PetscScalar* vec_buff;
    VecGetArray(*vecs_out[vec_idx], &vec_buff);

    ierr = MPI_File_read_ordered (file, vec_buff, my_n_rows, MPIU_SCALAR, MPI_STATUS_IGNORE);
    CHKERRABORT (comm, ierr);

    VecRestoreArray(*vecs_out[vec_idx], &vec_buff);
  }

  ierr = MPI_File_close(&file);  CHKERRABORT (comm, ierr);
}
void Checkpointer::write_cfg(const std::string& filename, const TimeInfo* ti, MPI_Comm comm)
{
  int rank;
  MPI_Comm_rank(comm, &rank);

  if (rank == 0) {
    libconfig::Config cfg;
    auto &cfgTi = cfg.getRoot().add("time_info", libconfig::Setting::TypeGroup);
    cfgTi.add("current_time", libconfig::Setting::TypeFloat) = ti->current;
	  cfgTi.add("current_step_num", libconfig::Setting::TypeInt) = (int) ti->currentstep;
    cfg.writeFile(filename.c_str());
  }
}

void Checkpointer::read_cfg(const std::string& filename, TimeInfo* ti, MPI_Comm comm)
{
  libconfig::Config cfg;
  cfg.readFile (filename.c_str());

  const auto& cfgTi = cfg.getRoot()["time_info"];
  ti->current = cfgTi["current_time"];
  ti->currentstep = (int) cfgTi["current_step_num"];
}
