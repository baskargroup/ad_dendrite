#include <dendrite/BoundaryConditions.h>
#include <dendrite/PetscLogging.h>

void BoundaryConditions::clear() {
  m_boundaryRows.clear();
  m_boundaryValues.clear();
}

void getBoundaries(ot::DA *da, const double *problemSize, int ndof,
                   std::vector<PetscInt> *rows_out, std::vector<PetscScalar> *values_out,
                   const std::function<Boundary(double, double, double, unsigned int)> &f) {
  std::map<unsigned int, Boundary> bdy;

  unsigned int maxD = da->getMaxDepth();
  unsigned int lev;
  double hx, hy, hz;
  Point pt;

  double xFac = problemSize[0] / ((double) (1 << (maxD - 1)));
  double yFac = problemSize[1] / ((double) (1 << (maxD - 1)));
  double zFac = problemSize[2] / ((double) (1 << (maxD - 1)));
  double xx[8], yy[8], zz[8];
  unsigned int idx[8];

  // TODO should this be WRITABLE?
  for (da->init<ot::DA_FLAGS::ALL>(); da->curr() < da->end<ot::DA_FLAGS::ALL>(); da->next<ot::DA_FLAGS::ALL>()) {
    // set the value

    lev = da->getLevel(da->curr());

    hx = xFac * (1 << (maxD - lev));
    hy = yFac * (1 << (maxD - lev));
    hz = zFac * (1 << (maxD - lev));


    pt = da->getCurrentOffset();

    //! get the correct coordinates of the nodes ...
    unsigned char hangingMask = da->getHangingNodeIndex(da->curr());

    xx[0] = pt.x() * xFac;
    yy[0] = pt.y() * yFac;
    zz[0] = pt.z() * zFac;
    xx[1] = pt.x() * xFac + hx;
    yy[1] = pt.y() * yFac;
    zz[1] = pt.z() * zFac;
    xx[2] = pt.x() * xFac;
    yy[2] = pt.y() * yFac + hy;
    zz[2] = pt.z() * zFac;
    xx[3] = pt.x() * xFac + hx;
    yy[3] = pt.y() * yFac + hy;
    zz[3] = pt.z() * zFac;

    xx[4] = pt.x() * xFac;
    yy[4] = pt.y() * yFac;
    zz[4] = pt.z() * zFac + hz;
    xx[5] = pt.x() * xFac + hx;
    yy[5] = pt.y() * yFac;
    zz[5] = pt.z() * zFac + hz;
    xx[6] = pt.x() * xFac;
    yy[6] = pt.y() * yFac + hy;
    zz[6] = pt.z() * zFac + hz;
    xx[7] = pt.x() * xFac + hx;
    yy[7] = pt.y() * yFac + hy;
    zz[7] = pt.z() * zFac + hz;

    da->getNodeIndices(idx);
    for (int i = 0; i < 8; ++i) {
      if (!(hangingMask & (1u << i))) {
        auto boundary = f(xx[i], yy[i], zz[i], idx[i]);
        if (!boundary.empty()) {
          bdy[idx[i]] = boundary;
        }
      }
    }

  } // loop over elements

  rows_out->clear();
  values_out->clear();

  // preallocate some memory
  rows_out->reserve(bdy.size());
  values_out->reserve(bdy.size());

  if (!da->computedLocalToGlobal())
    da->computeLocalToGlobalMappings();

  const DendroIntL *localToGlobal = da->getLocalToGlobalMap();

  for (const auto &it : bdy) {
    for (const auto &direchlet : it.second.dirichlets) {
      PetscInt row = ndof * localToGlobal[it.first] + direchlet.first;
      rows_out->push_back(row);
      values_out->push_back(direchlet.second);
    }
  }
}

void BoundaryConditions::addByNodalFunction(ot::DA* da, const double* problemSize, int ndof,
                                            const std::function<Boundary(double x, double y, double z, unsigned int size)>& f) {

  getBoundaries(da, problemSize, ndof, &m_boundaryRows, &m_boundaryValues, f);

}

void BoundaryConditions::addDirichlet(PetscInt row, PetscScalar val) {
  // make sure we aren't adding duplicates
  for (unsigned int i = 0; i < m_boundaryRows.size(); i++) {
    if (m_boundaryRows[i] == row) {
      m_boundaryValues[i] = val;
      return;
    }
  }

  m_boundaryRows.push_back(row);
  m_boundaryValues.push_back(val);
}

PetscErrorCode BoundaryConditions::applyMatBC(ot::DA* da, Mat mat) {
  PetscLogEventBegin(applyMatBCEvent, 0, 0, 0, 0);

  int ierr;

  // TODO cache this if it works
  IS is;
  ierr = ISCreateGeneral(da->getComm(), m_boundaryRows.size(), m_boundaryRows.data(), PETSC_COPY_VALUES, &is); CHKERRQ(ierr);
  ierr = ISSortRemoveDups(is); CHKERRQ(ierr);
  ierr = MatZeroRowsIS(mat, is, 1.0, NULL, NULL); CHKERRQ(ierr);
  ISDestroy(&is);

  PetscLogEventEnd(applyMatBCEvent, 0, 0, 0, 0);
  return 0;
}

PetscErrorCode BoundaryConditions::applyVecBC(ot::DA* da, Vec rhs) {
  PetscLogEventBegin(applyVecBCEvent, 0, 0, 0, 0);
  int ierr = VecSetValues(rhs, m_boundaryRows.size(), m_boundaryRows.data(), m_boundaryValues.data(), INSERT_VALUES);
  PetscLogEventEnd(applyVecBCEvent, 0, 0, 0, 0);
  return ierr;
}

PetscErrorCode BoundaryConditions::applyResidualBC(ot::DA* da, Vec residual) {
  PetscLogEventBegin(applyResidualVecBCEvent, 0, 0, 0, 0);
  std::vector<double> zeros(m_boundaryRows.size(), 0.0);
  int ierr = VecSetValues(residual, m_boundaryRows.size(), m_boundaryRows.data(), zeros.data(), INSERT_VALUES);
  PetscLogEventEnd(applyResidualVecBCEvent, 0, 0, 0, 0);
  return ierr;
}

PetscErrorCode BoundaryConditions::applyMatrixFreeBC(ot::DA* da, Vec in, Vec out) {
  int ierr;

  IS is;
  ierr = ISCreateGeneral(da->getComm(), m_boundaryRows.size(), m_boundaryRows.data(), PETSC_USE_POINTER, &is);
  CHKERRQ(ierr);

  VecScatter scatter;
  ierr = VecScatterCreate(in, is, out, is, &scatter); CHKERRQ(ierr);

  ierr = VecScatterBegin(scatter, in, out, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(scatter, in, out, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  ierr = VecScatterDestroy(&scatter); CHKERRQ(ierr);
  ierr = ISDestroy(&is); CHKERRQ(ierr);
  return 0;
}

PetscErrorCode BoundaryConditions::applyVecBC(DM da, Vec rhs) {
  PetscLogEventBegin(applyVecBCEvent, 0, 0, 0, 0);
  assert(false);  // not implemented
  PetscLogEventEnd(applyVecBCEvent, 0, 0, 0, 0);
  return -1;
}

PetscErrorCode BoundaryConditions::applyMatBC(DM da, Mat mat) {
  PetscLogEventBegin(applyMatBCEvent, 0, 0, 0, 0);
  assert(false);  // not implemented
  PetscLogEventEnd(applyMatBCEvent, 0, 0, 0, 0);
  return -1;
}

PetscErrorCode BoundaryConditions::applyResidualBC(DM da, Vec residual) {
  PetscLogEventBegin(applyResidualVecBCEvent, 0, 0, 0, 0);
  assert(false);  // not implemented
  PetscLogEventEnd(applyResidualVecBCEvent, 0, 0, 0, 0);
  return -1;
}
