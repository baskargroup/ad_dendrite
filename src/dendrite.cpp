#include <dendrite/dendrite.h>

#include "externVars.h"  // must be included or many linker errors will occur
#include "hcurvedata.h"
#include <oda/odaUtils.h>
#include <sys.h>
#include <fstream>

#define PETSC_LOGGING_IMPL
#include <dendrite/PetscLogging.h>  // talydendro events
#undef PETSC_LOGGING_IMPL

#ifdef _OPENMP
#include <omp.h>  // for omp_set_num_threads
#endif

void dendrite_init(int argc, char** argv) {
#ifdef _OPENMP
  // work-around for bug in Dendro (race condition in octree sort OpenMP parallelization)
  omp_set_num_threads(1);
#endif
   
  PetscInitialize(&argc, &argv, NULL, NULL);
  _InitializeHcurve(3);

  if (std::ifstream("ShapeFnCoeffs.inp")) {

    ot::DA_Initialize(MPI_COMM_WORLD);
   
  } else {
    
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (!rank)
      std::cerr << "ShapeFnCoeffs.inp missing - interpolation will be unavailable." << std::endl;
  }
  ot::RegisterEvents();  // for PETSc log summary
  talydendro_register_events();

  PetscLogEventBegin(totalEvent, 0, 0, 0, 0);
}

void dendrite_finalize() {
  PetscLogEventEnd(totalEvent, 0, 0, 0, 0);
  PetscFinalize();
}
