#include <dendrite/DendroIO.h>
#include <dendrite/interp.h>
#include <dendrite/endian.h>
//
///**
// * Print floating-point data to a file in binary (big-endian) or (space-delimited) ASCII.
// * Used for VTK files.
// * NOTE: Currently prints as 4-byte floats in binary mode instead of full 8-byte doubles.
// * @param out file to write to
// * @param[in] data data to write
// * @param count number of elements in data
// * @param binary whether or not to print in binary or not
// */
//void write_data(std::ofstream& out, double* data, size_t count, bool binary) {
//    if (binary) {
//        for (size_t i = 0; i < count; i++) {
//            float tmp_float = (float) data[i];  // convert from 8-byte double to 4-byte float
//            uint32_t tmp = *((uint32_t*) &tmp_float);  // reinterpret as uint32 (no type conversion!)
//            tmp = htobe32(tmp);  // swap bytes if necessary
//            out.write((char*) &tmp, 4);
//        }
//    } else {
//        for (size_t i = 0; i < count; i++) {
//            out << data[i] << " ";
//        }
//    }
//}
//
///**
// * Print 32-bit integer data to a file in binary (big-endian) or (space-delimited) ASCII.
// * Used for VTK files.
// * @param out file to write to
// * @param[in] data data to write
// * @param count number of elements in data
// * @param binary whether or not to print in binary or not
// */
//void write_data(std::ofstream& out, int32_t* data, size_t count, bool binary) {
//    if (binary) {
//        for (size_t i = 0; i < count; i++) {
//            uint32_t tmp = *((uint32_t*) data + i);  // reinterpret as uint32 (no type conversion!)
//            tmp = htobe32(tmp);
//            out.write((char*) &tmp, 4);
//        }
//    } else {
//        for (size_t i = 0; i < count; i++) {
//
//            out << data[i] << " ";
//        }
//    }
//}
//

//
//Attribute Attribute::scalar(const std::string& attr_name, unsigned int dof) {
//    return Attribute {
//            .name = attr_name, .type = "SCALARS", .start_idx = dof, .len = 1
//    };
//}
//
//Attribute Attribute::vector(const std::string& attr_name, unsigned int start_dof) {
//    return Attribute {
//            .name = attr_name, .type = "VECTORS", .start_idx = start_dof, .len = 3
//    };
//}
//
std::string Attribute::vtu_header() const {
    std::stringstream out;
    out << type << "=\"" << name << "\">\n";
    if (type == "SCALARS") {
        out << "<DataArray type=\"Float64\" Name=\""<< name << "\" format=\"ascii\">" << std::endl;
//        out << "LOOKUP_TABLE default";
    }
//    out << std::endl;
    return out.str();

}
//
//
//
//void write_octree(const std::string& filename, const std::vector<ot::TreeNode>& nodes, MPI_Comm comm) {
//    int ierr, rank, size;
//    MPI_Comm_rank(comm, &rank);
//    MPI_Comm_size(comm, &size);
//
//    MPI_File file;
//    ierr = MPI_File_open(comm, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &file);
//    if (ierr) {
//        throw std::runtime_error("Could not open octree file for writing.");
//    }
//
//    if (rank == 0) {
//        ierr = MPI_File_write_at(file, 0, &size, 1, MPI_INT, MPI_STATUS_IGNORE);
//        assert (ierr == 0);
//    }
//
//    ierr = MPI_File_seek_shared(file, 4 /* skip to after size */, MPI_SEEK_SET);
//    assert (ierr == 0);
//
//    // write number of nodes on this proc
//    int n_nodes = nodes.size();
//    ierr = MPI_File_write_ordered(file, &n_nodes, 1, MPI_INT, MPI_STATUS_IGNORE);
//    assert (ierr == 0);
//
//    // write TreeNodes
//    MPI_Datatype treenode_mpi_dtype = par::Mpi_datatype<ot::TreeNode>::value();
//    ierr = MPI_File_write_ordered(file, nodes.data(), n_nodes, treenode_mpi_dtype, MPI_STATUS_IGNORE);
//    if (ierr)
//        throw std::runtime_error("Could not write TreeNodes.");
//
//    ierr = MPI_File_close(&file);
//    assert (ierr == 0);
//}
//
//std::vector<ot::TreeNode> read_octree(const std::string& filename, MPI_Comm comm) {
//    int ierr, rank, size;
//    MPI_Comm_rank(comm, &rank);
//    MPI_Comm_size(comm, &size);
//
//    MPI_File file;
//    ierr = MPI_File_open(comm, filename.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &file);
//    if (ierr)
//        throw std::runtime_error("Could not open octree file for reading.");
//
//    int file_nprocs;
//    ierr = MPI_File_read(file, &file_nprocs, 1, MPI_INT, MPI_STATUS_IGNORE);
//    assert (ierr == 0);
//    if (file_nprocs != size) {
//        throw std::runtime_error(
//                "Checkpoint file was written with " + std::to_string(file_nprocs)
//                + " procs, but we are running with " + std::to_string(size));
//    }
//
//    ierr = MPI_File_seek_shared(file, 4 /* skip to after size */, MPI_SEEK_SET);
//    assert (ierr == 0);
//
//    // read number of nodes for this proc
//    int n_nodes;
//    ierr = MPI_File_read_ordered(file, &n_nodes, 1, MPI_INT, MPI_STATUS_IGNORE);
//    assert (ierr == 0);
//
//    // read TreeNodes
//    std::vector<ot::TreeNode> nodes(n_nodes);
//    MPI_Datatype treenode_mpi_dtype = par::Mpi_datatype<ot::TreeNode>::value();
//    ierr = MPI_File_read_ordered(file, nodes.data(), n_nodes, treenode_mpi_dtype, MPI_STATUS_IGNORE);
//    if (ierr)
//        throw std::runtime_error("Could not read TreeNodes.");
//
//    ierr = MPI_File_close(&file);
//    assert (ierr == 0);
//
//    return nodes;
//}
//

#include <dendrite/DendroIO.h>
#include <dendrite/interp.h>
#include <dendrite/endian.h>

/**
 * Print floating-point data to a file in binary (big-endian) or (space-delimited) ASCII.
 * Used for VTK files.
 * NOTE: Currently prints as 4-byte floats in binary mode instead of full 8-byte doubles.
 * @param out file to write to
 * @param[in] data data to write
 * @param count number of elements in data
 * @param binary whether or not to print in binary or not
 */
void write_data(std::ofstream& out, double* data, size_t count, bool binary) {
    if (binary) {
        for (size_t i = 0; i < count; i++) {
            float tmp_float = (float) data[i];  // convert from 8-byte double to 4-byte float
            uint32_t tmp = *((uint32_t*) &tmp_float);  // reinterpret as uint32 (no type conversion!)
            tmp = htobe32(tmp);  // swap bytes if necessary
            out.write((char*) &tmp, 4);
        }
    } else {
        for (size_t i = 0; i < count; i++) {
            out << data[i] << " ";
        }
    }
}

/**
 * Print 32-bit integer data to a file in binary (big-endian) or (space-delimited) ASCII.
 * Used for VTK files.
 * @param out file to write to
 * @param[in] data data to write
 * @param count number of elements in data
 * @param binary whether or not to print in binary or not
 */
void write_data(std::ofstream& out, int32_t* data, size_t count, bool binary) {
    if (binary) {
        for (size_t i = 0; i < count; i++) {
            uint32_t tmp = *((uint32_t*) data + i);  // reinterpret as uint32 (no type conversion!)
            tmp = htobe32(tmp);
            out.write((char*) &tmp, 4);
        }
    } else {
        for (size_t i = 0; i < count; i++) {
            out << data[i] << " ";
        }
    }
}

void octree2VTK(ot::DA* da, const double* problemSize, const std::vector<VecOutInfo>& vecs,
                const std::string& file_prefix, bool binary) {
    // don't output if "-no_output" is specified on the command line
    {
        PetscBool no_output = PETSC_FALSE;
        PetscOptionsGetBool(NULL, NULL, "-no_output", &no_output, NULL);
        if (no_output == PETSC_TRUE)
            return;
    }

    // dump data as binary (used by tests)
    {
        PetscBool dump_vec = PETSC_FALSE;
        PetscOptionsGetBool(NULL, NULL, "-dump_vec", &dump_vec, NULL);
        if (dump_vec == PETSC_TRUE) {
            for (unsigned int i = 0; i < vecs.size(); i++) {
                const auto& v = vecs[i];

                std::stringstream ss;
                if (vecs.size() == 1)
                    ss << file_prefix << ".vec";
                else
                    ss << file_prefix << ".v" << i << ".vec";

                PetscViewer viewer;
                int ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, ss.str().c_str(), FILE_MODE_WRITE, &viewer);
                assert(ierr == 0);
                ierr = VecView(v.vec, viewer);
                assert(ierr == 0);
                ierr = PetscViewerDestroy(&viewer);
                assert(ierr == 0);
            }
        }
    }


    int rank, size;
    char fname[PATH_MAX];

    MPI_Comm_rank(da->getComm(), &rank);
    MPI_Comm_size(da->getComm(), &size);

    std::stringstream ss;
    snprintf(fname, sizeof(fname), "%s_%05d.vtk", file_prefix.c_str(), rank);

    if (!rank)
        std::cout << "Writing to VTK file: " << file_prefix << std::endl;

    // gather some statistics
    const unsigned int dim = 3;
    const unsigned int unit_points = 1u << dim;

    unsigned int num_cells = 0;
    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>();
         da->next<ot::DA_FLAGS::WRITABLE>()) {
        num_cells++;
    }

    const unsigned int num_vertices = num_cells * (unit_points);
    const unsigned int num_cells_elements = num_cells * unit_points + num_cells;


    // start writing to file
    std::ofstream out;
    out.open(fname);

    // VTK header
    out << "# vtk DataFile Version 2.0" << std::endl;
    out << "DENDRO OCTREES" << std::endl;
    out << (binary ? "BINARY" : "ASCII") << std::endl;
    out << "DATASET UNSTRUCTURED_GRID" << std::endl;

    // only for NSD = 3 right now
    assert (dim == 3);

    // write out all the coordinates
    {
        out << "POINTS " << num_vertices << " float" << std::endl;

        // precalculate conversion factor for building node coordinates
        unsigned int maxD = da->getMaxDepth();
        const double xFac = problemSize[0] / ((double) (1u << (maxD - 1)));
        const double yFac = problemSize[1] / ((double) (1u << (maxD - 1)));
        const double zFac = problemSize[2] / ((double) (1u << (maxD - 1)));

        // loop over all elements on this process
        for (da->init<ot::DA_FLAGS::WRITABLE>();
             da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {

            unsigned int lev = da->getLevel(da->curr());

            // element size (in physical coordinates)
            double hx = xFac * (1u << (maxD - lev));
            double hy = yFac * (1u << (maxD - lev));
            double hz = zFac * (1u << (maxD - lev));

            // coordinate of "bottom left" node in octree integer coordinates
            Point pt;
            pt = da->getCurrentOffset();

            // convert to physical coordinates
            double xx = pt.x() * xFac;
            double yy = pt.y() * yFac;
            double zz = pt.z() * zFac;

            // actually write the node coords
            double coords[8*3] = {
                    pt.x() * xFac, pt.y() * yFac, pt.z() * zFac,
                    pt.x() * xFac + hx, pt.y() * yFac, pt.z() * zFac,
                    pt.x() * xFac + hx, pt.y() * yFac + hy, pt.z() * zFac,
                    pt.x() * xFac, pt.y() * yFac + hy, pt.z() * zFac,

                    pt.x() * xFac, pt.y() * yFac, pt.z() * zFac + hz,
                    pt.x() * xFac + hx, pt.y() * yFac, pt.z() * zFac + hz,
                    pt.x() * xFac + hx, pt.y() * yFac + hy, pt.z() * zFac + hz,
                    pt.x() * xFac, pt.y() * yFac + hy, pt.z() * zFac + hz,
            };
            write_data(out, coords, 8*3, binary);
        }
    }

    // write out connectivity (trivial, since we duplicate nodes for each element)
    {
        out << "CELLS " << num_cells << " " << num_cells_elements << std::endl;

        std::vector<int> data(1 + unit_points);
        for (int i = 0; i < num_cells; i++) {
            data[0] = unit_points;
            for (int j = 0; j < unit_points; j++) {
                data[1+j] = i * unit_points + j;
            }
            write_data(out, data.data(), data.size(), binary);
        }
        // out << std::endl;

        out << "CELL_TYPES " << num_cells << std::endl;
        data.resize(1);
        data[0] = VTK_HEXAHEDRON;
        for (int i = 0; i < num_cells; i++) {
            write_data(out, data.data(), data.size(), binary);
        }
        out << std::endl;
    }


    // print data in vectors
    std::vector<VecOutInfo> cells, points;
    std::copy_if(vecs.begin(), vecs.end(), std::back_inserter(cells),
                 [&] (const VecOutInfo& v) { return v.is_elemental; });
    std::copy_if(vecs.begin(), vecs.end(), std::back_inserter(points),
                 [&] (const VecOutInfo& v) { return !v.is_elemental; });

    out << "CELL_DATA " << num_cells << std::endl;
    for (const auto& v : cells) {
        assert (v.vec != NULL && v.ndof != 0 && v.is_elemental);

        PetscScalar* buff = NULL;
        da->vecGetBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
        da->ReadFromGhostsBegin<PetscScalar>(buff, v.ndof);
        da->ReadFromGhostsEnd<PetscScalar>(buff);

        for (unsigned int attr_idx = 0; attr_idx < v.attributes.size(); attr_idx++) {
            const auto& attr = v.attributes[attr_idx];
            out << attr.vtk_header();

            std::vector<double> data(attr.len);
            for (da->init<ot::DA_FLAGS::WRITABLE>();
                 da->curr() < da->end<ot::DA_FLAGS::WRITABLE>();
                 da->next<ot::DA_FLAGS::WRITABLE>()) {
                for (unsigned int i = 0; i < attr.len; i++) {
                    data[i] = buff[da->curr() * v.ndof + attr.start_idx + i];
                }
                write_data(out, data.data(), data.size(), binary);
            }
        }

        da->vecRestoreBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
        out << std::endl;
    }

    out << "POINT_DATA " << num_vertices << std::endl;
    for (const auto& v : points) {
        assert (v.vec != NULL && v.ndof != 0 && !v.is_elemental);

        PetscScalar *buff = NULL;
        da->vecGetBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
        da->ReadFromGhostsBegin<PetscScalar>(buff, v.ndof);
        da->ReadFromGhostsEnd<PetscScalar>(buff);

        std::vector<PetscScalar> local(8 * v.ndof);  // temp for interpolated data
        for (unsigned int attr_idx = 0; attr_idx < v.attributes.size(); attr_idx++) {
            const auto& attr = v.attributes[attr_idx];
            out << attr.vtk_header();

            std::vector<double> data(attr.len * 8);
            for (da->init<ot::DA_FLAGS::WRITABLE>();
                 da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
                interp_global_to_local(buff, local.data(), da, v.ndof);

                static const int order[8] = {0, 1, 3, 2, 4, 5, 7, 6};
                for (unsigned int n = 0; n < 8; n++) {
                    for (unsigned int i = 0; i < attr.len; i++) {
                        data[attr.len * n + i] = local[order[n] * v.ndof + attr.start_idx + i];
                    }
                }
                write_data(out, data.data(), data.size(), binary);
            }
        }

        da->vecRestoreBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
        out << std::endl;
    }

    out.close();
}

Attribute Attribute::scalar(const std::string& attr_name, unsigned int dof) {
    return Attribute {
            .name = attr_name, .type = "SCALARS", .start_idx = dof, .len = 1
    };
}

Attribute Attribute::vector(const std::string& attr_name, unsigned int start_dof) {
    return Attribute {
            .name = attr_name, .type = "VECTORS", .start_idx = start_dof, .len = 3
    };
}

std::string Attribute::vtk_header() const {
    std::stringstream out;
    out << type << " " << name << " float";
    if (type == "SCALARS") {
        out << " " << len << std::endl;
        out << "LOOKUP_TABLE default";
    }
    out << std::endl;
    return out.str();
}



void write_octree(const std::string& filename, const std::vector<ot::TreeNode>& nodes, MPI_Comm comm) {
    int ierr, rank, size;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    MPI_File file;
    ierr = MPI_File_open(comm, filename.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &file);
    if (ierr) {
        throw std::runtime_error("Could not open octree file for writing.");
    }

    if (rank == 0) {
        ierr = MPI_File_write_at(file, 0, &size, 1, MPI_INT, MPI_STATUS_IGNORE);
        assert (ierr == 0);
    }

    ierr = MPI_File_seek_shared(file, 4 /* skip to after size */, MPI_SEEK_SET);
    assert (ierr == 0);

    // write number of nodes on this proc
    int n_nodes = nodes.size();
    ierr = MPI_File_write_ordered(file, &n_nodes, 1, MPI_INT, MPI_STATUS_IGNORE);
    assert (ierr == 0);

    // write TreeNodes
    MPI_Datatype treenode_mpi_dtype = par::Mpi_datatype<ot::TreeNode>::value();
    ierr = MPI_File_write_ordered(file, nodes.data(), n_nodes, treenode_mpi_dtype, MPI_STATUS_IGNORE);
    if (ierr)
        throw std::runtime_error("Could not write TreeNodes.");

    ierr = MPI_File_close(&file);
    assert (ierr == 0);
}

std::vector<ot::TreeNode> read_octree(const std::string& filename, MPI_Comm comm) {
    int ierr, rank, size;
    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &size);

    MPI_File file;
    ierr = MPI_File_open(comm, filename.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &file);
    if (ierr)
        throw std::runtime_error("Could not open octree file for reading.");

    int file_nprocs;
    ierr = MPI_File_read(file, &file_nprocs, 1, MPI_INT, MPI_STATUS_IGNORE);
    assert (ierr == 0);
    if (file_nprocs != size) {
        throw std::runtime_error(
                "Checkpoint file was written with " + std::to_string(file_nprocs)
                + " procs, but we are running with " + std::to_string(size));
    }

    ierr = MPI_File_seek_shared(file, 4 /* skip to after size */, MPI_SEEK_SET);
    assert (ierr == 0);

    // read number of nodes for this proc
    int n_nodes;
    ierr = MPI_File_read_ordered(file, &n_nodes, 1, MPI_INT, MPI_STATUS_IGNORE);
    assert (ierr == 0);

    // read TreeNodes
    std::vector<ot::TreeNode> nodes(n_nodes);
    MPI_Datatype treenode_mpi_dtype = par::Mpi_datatype<ot::TreeNode>::value();
    ierr = MPI_File_read_ordered(file, nodes.data(), n_nodes, treenode_mpi_dtype, MPI_STATUS_IGNORE);
    if (ierr)
        throw std::runtime_error("Could not read TreeNodes.");

    ierr = MPI_File_close(&file);
    assert (ierr == 0);

    return nodes;
}

void octree2VTU(ot::DA* da, const double* problemSize, const std::vector<VecOutInfo>& vecs,
                const std::string& file_prefix, bool binary) {
    // don't output if "-no_output" is specified on the command line
    {
        PetscBool no_output = PETSC_FALSE;
        PetscOptionsGetBool(NULL, NULL, "-no_output", &no_output, NULL);
        if (no_output == PETSC_TRUE)
            return;
    }

    // dump data as binary (used by tests)
    {
        PetscBool dump_vec = PETSC_FALSE;
        PetscOptionsGetBool(NULL, NULL, "-dump_vec", &dump_vec, NULL);
        if (dump_vec == PETSC_TRUE) {
            for (unsigned int i = 0; i < vecs.size(); i++) {
                const auto& v = vecs[i];

                std::stringstream ss;
                if (vecs.size() == 1)
                    ss << file_prefix << ".vec";
                else
                    ss << file_prefix << ".v" << i << ".vec";

                PetscViewer viewer;
                int ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, ss.str().c_str(), FILE_MODE_WRITE, &viewer);
                assert(ierr == 0);
                ierr = VecView(v.vec, viewer);
                assert(ierr == 0);
                ierr = PetscViewerDestroy(&viewer);
                assert(ierr == 0);
            }
        }
    }


    int rank, size;
    char fname[PATH_MAX];

    MPI_Comm_rank(da->getComm(), &rank);
    MPI_Comm_size(da->getComm(), &size);

    std::stringstream ss;
    snprintf(fname, sizeof(fname), "%s_%05d.vtu", file_prefix.c_str(), rank);

    if (!rank)
        std::cout << "Writing to VTK file: " << file_prefix << std::endl;

    // gather some statistics
    const unsigned int dim = 3;
    const unsigned int unit_points = 1u << dim;

    unsigned int num_cells = 0;
    for (da->init<ot::DA_FLAGS::WRITABLE>();
         da->curr() < da->end<ot::DA_FLAGS::WRITABLE>();
         da->next<ot::DA_FLAGS::WRITABLE>()) {
        num_cells++;
    }

    const unsigned int num_vertices = num_cells * (unit_points);
    const unsigned int num_cells_elements = num_cells * unit_points + num_cells;


    // start writing to file
    std::ofstream out;
    out.open(fname);

    // VTK header
    out <<"<?xml version=\"1.0\"?> "<<std::endl;
    out <<"<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" >"<<std::endl;
    out <<"<UnstructuredGrid >"<<std::endl;
    out << "<Piece NumberOfPoints=\" " << num_vertices << "\" NumberOfCells=\" " <<  num_cells << "\" >\n";

//   out << (binary ? "BINARY" : "ASCII") << std::endl;
//   out << "DATASET UNSTRUCTURED_GRID" << std::endl;

    // only for NSD = 3 right now
    assert (dim == 3);

    // write out all the coordinates
    {
//     out << "POINTS " << num_vertices << " float" << std::endl;
        out << "<Points>\n";
        out << "<DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n";
        // precalculate conversion factor for building node coordinates
        unsigned int maxD = da->getMaxDepth();
        const double xFac = problemSize[0] / ((double) (1u << (maxD - 1)));
        const double yFac = problemSize[1] / ((double) (1u << (maxD - 1)));
        const double zFac = problemSize[2] / ((double) (1u << (maxD - 1)));

        // loop over all elements on this process
        for (da->init<ot::DA_FLAGS::WRITABLE>();
             da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {

            unsigned int lev = da->getLevel(da->curr());

            // element size (in physical coordinates)
            double hx = xFac * (1u << (maxD - lev));
            double hy = yFac * (1u << (maxD - lev));
            double hz = zFac * (1u << (maxD - lev));

            // coordinate of "bottom left" node in octree integer coordinates
            Point pt;
            pt = da->getCurrentOffset();

            // convert to physical coordinates
            double xx = pt.x() * xFac;
            double yy = pt.y() * yFac;
            double zz = pt.z() * zFac;

            // actually write the node coords
            double coords[8*3] = {
                    pt.x() * xFac, pt.y() * yFac, pt.z() * zFac,
                    pt.x() * xFac + hx, pt.y() * yFac, pt.z() * zFac,
                    pt.x() * xFac + hx, pt.y() * yFac + hy, pt.z() * zFac,
                    pt.x() * xFac, pt.y() * yFac + hy, pt.z() * zFac,

                    pt.x() * xFac, pt.y() * yFac, pt.z() * zFac + hz,
                    pt.x() * xFac + hx, pt.y() * yFac, pt.z() * zFac + hz,
                    pt.x() * xFac + hx, pt.y() * yFac + hy, pt.z() * zFac + hz,
                    pt.x() * xFac, pt.y() * yFac + hy, pt.z() * zFac + hz,
            };
            write_data(out, coords, 8*3, binary);
        }
        out << "</DataArray>\n";

        out << "</Points>\n";
    }

    // write out connectivity (trivial, since we duplicate nodes for each element)
    {
//     out << "CELLS " << num_cells << " " << num_cells_elements << std::endl;
        out << "<Cells>\n";
        out << "<DataArray type=\"UInt32\" Name=\"connectivity\" format=\"ascii\">\n";

        std::vector<int> data(unit_points);
        for (int i = 0; i < num_cells; i++) {
            for (int j = 0; j < unit_points; j++) {
                data[j] = i * unit_points + j ;
            }
            write_data(out, data.data(), data.size(), binary);
            out << "\n";
        }
        out << "</DataArray>\n";
        // offsets
        out << "<DataArray type=\"UInt32\" Name=\"offsets\" format=\"ascii\">\n";
        data.resize(num_cells);
        for (int i = 0; i < num_cells; i++) {
            data[i] = (i + 1)*8;

        }
        write_data(out, data.data(), data.size(), binary);
        out << "</DataArray>\n";

        out << "<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n";
        for (int i = 0; i < num_cells; i++) {
            data[i] = VTK_HEXAHEDRON;
        }
        write_data(out, data.data(), data.size(), binary);
        out << "</DataArray>\n";
        out << "</Cells>\n";
    }


    // print data in vectors
    std::vector<VecOutInfo> cells, points;
    std::copy_if(vecs.begin(), vecs.end(), std::back_inserter(cells),
                 [&] (const VecOutInfo& v) { return v.is_elemental; });
    std::copy_if(vecs.begin(), vecs.end(), std::back_inserter(points),
                 [&] (const VecOutInfo& v) { return !v.is_elemental; });

//    out << "CELL_DATA " << num_cells << std::endl;
//    for (const auto& v : cells) {
//        assert (v.vec != NULL && v.ndof != 0 && v.is_elemental);
//
//        PetscScalar* buff = NULL;
//        da->vecGetBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
//        da->ReadFromGhostsBegin<PetscScalar>(buff, v.ndof);
//        da->ReadFromGhostsEnd<PetscScalar>(buff);
//
//        for (unsigned int attr_idx = 0; attr_idx < v.attributes.size(); attr_idx++) {
//            const auto& attr = v.attributes[attr_idx];
//            out << attr.vtk_header();
//
//            std::vector<double> data(attr.len);
//            for (da->init<ot::DA_FLAGS::WRITABLE>();
//                 da->curr() < da->end<ot::DA_FLAGS::WRITABLE>();
//                 da->next<ot::DA_FLAGS::WRITABLE>()) {
//                for (unsigned int i = 0; i < attr.len; i++) {
//                    data[i] = buff[da->curr() * v.ndof + attr.start_idx + i];
//                }
//                write_data(out, data.data(), data.size(), binary);
//            }
//        }
//
//        da->vecRestoreBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
//        out << std::endl;
//    }

    out << "<PointData " ;
    for (const auto& v : points) {
        assert (v.vec != NULL && v.ndof != 0 && !v.is_elemental);

        PetscScalar *buff = NULL;
        da->vecGetBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
        da->ReadFromGhostsBegin<PetscScalar>(buff, v.ndof);
        da->ReadFromGhostsEnd<PetscScalar>(buff);

        std::vector<PetscScalar> local(8 * v.ndof);  // temp for interpolated data
        for (unsigned int attr_idx = 0; attr_idx < v.attributes.size(); attr_idx++) {
            const auto& attr = v.attributes[attr_idx];
            out << attr.vtu_header();

            std::vector<double> data(attr.len * 8);
            for (da->init<ot::DA_FLAGS::WRITABLE>();
                 da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
                interp_global_to_local(buff, local.data(), da, v.ndof);

                static const int order[8] = {0, 1, 3, 2, 4, 5, 7, 6};
                for (unsigned int n = 0; n < 8; n++) {
                    for (unsigned int i = 0; i < attr.len; i++) {
                        data[attr.len * n + i] = local[order[n] * v.ndof + attr.start_idx + i];
                    }
                }
                write_data(out, data.data(), data.size(), binary);
            }
        }

        da->vecRestoreBuffer(v.vec, buff, v.is_elemental, false, true, v.ndof);
        out << std::endl;
    }
    out << "</DataArray>\n";
    out << "</PointData>\n";
    out << "</Piece>\n";
    out << "</UnstructuredGrid>\n";
    out << "</VTKFile>\n";

    out.close();
}