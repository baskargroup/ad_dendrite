#include <dendrite/solvers/Solver.h>

Solver::Solver() {
  m_octDA = NULL;
  m_da = NULL;

  m_Mat = NULL;
  m_Vec = NULL;

  // Matrix 
  m_matJacobian = NULL;

  m_vecRHS = NULL;
  m_vecSolution = NULL;

  m_uiDof = 0;
}

void Solver::setMatrix(feMat *taly) {
  m_Mat = taly;
}

void Solver::setVector(feVec *taly) {
  m_Vec = taly;
}

void Solver::setBoundaryCondition(const std::function<Boundary(double, double, double, unsigned int)> &f) {
  m_boundaryCondition = f;
}



void Solver::updateBoundaries(ot::DA *da) {
  if (m_boundaryCondition) {
    boundary_conditions_.clear();

    boundary_conditions_.addByNodalFunction(da, getProblemSize(), getDof(), m_boundaryCondition);

  }
}

void Solver::updateBoundaries(DM da) {
  assert(false);  // not implemented
}
