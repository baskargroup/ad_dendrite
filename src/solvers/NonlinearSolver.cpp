#include <dendrite/solvers/NonlinearSolver.h>
#include <dendrite/PetscLogging.h>

PetscErrorCode NonlinearSolver::init() {
  PetscLogEventBegin(solverInitEvent, 0, 0, 0, 0);
  assert(m_octDA || m_da);
  assert(!(m_octDA && m_da));
  assert(m_vecSolution == NULL);
  assert(m_vecRHS == NULL);
  assert(getDof() != 0);

  int ierr;

  // Allocate memory for working vectors
  ierr = m_octDA->createVector(m_vecSolution, false, false, getDof()); CHKERRQ(ierr);
  ierr = m_octDA->createVector(m_vecRHS, false, false, getDof()); CHKERRQ(ierr);

  // create the matrix/shell matrix
  if (!m_matrixFree) {
    if (m_da) {  // petsc
      ierr = DMCreateMatrix(m_da, &m_matJacobian); CHKERRQ(ierr);
    } else if (m_octDA) {  // octree
      ierr = m_octDA->createMatrix(m_matJacobian, MATAIJ, getDof()); CHKERRQ(ierr);
    } else {
      assert(false);
    }
  } else {
    // matrix-free (same for both petsc/octree)
    int matsize;
    ierr = VecGetLocalSize(m_vecRHS, &matsize); CHKERRQ(ierr);

    ierr = MatCreateShell(PETSC_COMM_WORLD, matsize, matsize, PETSC_DETERMINE, PETSC_DETERMINE, this, &m_matJacobian);
    CHKERRQ(ierr);
    ierr = MatShellSetOperation(m_matJacobian, MATOP_MULT, (void (*)()) (ShellMatMult));
    CHKERRQ(ierr);
  }


  // avoids new nonzero errors when boundary conditions are used
  ierr = MatSetOption(m_matJacobian, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRQ(ierr);

  // Create a KSP context to solve  @ every timestep
  ierr = SNESCreate(PETSC_COMM_WORLD, &m_snes); CHKERRQ(ierr);
  ierr = SNESSetFunction(m_snes, m_vecRHS, FormFunction, this); CHKERRQ(ierr);
  ierr = SNESSetJacobian(m_snes, m_matJacobian, m_matJacobian, FormJacobian, this); CHKERRQ(ierr);

  KSP ksp;
  SNESGetKSP(m_snes,&ksp);
  KSPSetTolerances(ksp,1E-12,1E-12,PETSC_DEFAULT,PETSC_DEFAULT);
  SNESSetTolerances(m_snes,1E-12,1E-12,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);

  ierr = SNESSetFromOptions(m_snes); CHKERRQ(ierr);



  PetscLogEventEnd(solverInitEvent, 0, 0, 0, 0);
  return 0;
}

/**
 * @brief Solve the timestepping problem
 * @return bool, return true if successful , false otherwise
 **/
PetscErrorCode NonlinearSolver::solve() {
  PetscLogEventBegin(solverSolveEvent, 0, 0, 0, 0);
  int ierr;


  // rebuild dirichlet BC row/val list
  if (m_da) {

      updateBoundaries(m_da);

  }
  if (m_octDA) {

      updateBoundaries(m_octDA);

  }

  // apply dirichlet BC

  if (m_da) {
    ierr = boundary_conditions_.applyVecBC(m_da, m_vecSolution);
    CHKERRQ(ierr);
  }

  if (m_octDA) {
    ierr = boundary_conditions_.applyVecBC(m_octDA, m_vecSolution);
    CHKERRQ(ierr);
  }

  ierr = VecAssemblyBegin(m_vecSolution); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(m_vecSolution); CHKERRQ(ierr);

  ierr = SNESSolve(m_snes, PETSC_NULL, m_vecSolution); CHKERRQ(ierr);

  SNESConvergedReason converged_reason;
  SNESGetConvergedReason(m_snes, &converged_reason);
  if (converged_reason < 0) {
       // diverged
       std::cout << "Non-linear solve diverged.\n";
       assert(false);
    }
  PetscLogEventEnd(solverSolveEvent, 0, 0, 0, 0);

  return 0;
}

/**
 * @brief Jacobian matrix-vec product done at every timestep
 *        This is called by the shell matrix when using matrix-free.
 * @param in, PETSC Vec which is the current solution
 * @param out, PETSC Vec which is Out = J*in, J is the Jacobian
 **/
PetscErrorCode NonlinearSolver::jacobianMatMult(Vec in, Vec out) {
  assert(m_matrixFree);
  assert(m_Mat != NULL);

  int ierr;

  ierr = VecZeroEntries(out); CHKERRQ(ierr);

  m_Mat->setPlaceholder(PLACEHOLDER_GUESS, m_guess);
  m_Mat->MatVec(in, out);
  surfaceIntegralJacMfree(in, out, m_guess);

  ierr = getBC().applyMatrixFreeBC(m_octDA, in, out); CHKERRQ(ierr);
  return 0;
}

PetscErrorCode NonlinearSolver::FormJacobian(SNES snes, Vec sol, Mat jac, Mat precond_matrix, void *ctx) {
    int ierr;
  NonlinearSolver *nl = (NonlinearSolver *) ctx;

  // note: jacobianMatMult depends on this staying set
  nl->m_guess = sol;
  ierr = MatSetOption(jac, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);

  if (!nl->m_matrixFree) {
    ierr = MatZeroEntries(jac); CHKERRQ(ierr);

    nl->m_Mat->setPlaceholder(PLACEHOLDER_GUESS, sol);

    // PrintStatus("Start volume matrix assemble");
    PetscLogEventBegin(assembleVolMatNSEvent, 0, 0, 0, 0);
    nl->m_Mat->GetAssembledMatrix(&jac, 0);
    PetscLogEventEnd(assembleVolMatNSEvent, 0, 0, 0, 0);
    // PrintStatus("Finish volume matrix assemble");

    // PrintStatus("Start surface matrix assemble");
    PetscLogEventBegin(assembleSurfMatNSEvent, 0, 0, 0, 0);
    nl->surfaceIntegralJac(&jac, sol);
    PetscLogEventEnd(assembleSurfMatNSEvent, 0, 0, 0, 0);
    // PrintStatus("Finish surface matrix  assemble");

    PetscLogEventBegin(solverJacobianAssemblyEvent, 0, 0, 0, 0);
    ierr = MatAssemblyBegin(jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    PetscLogEventEnd(solverJacobianAssemblyEvent, 0, 0, 0, 0);

    if (nl->m_da) {
      ierr = nl->getBC().applyMatBC(nl->m_da, jac); CHKERRQ(ierr);
    }
    if (nl->m_octDA) {
      ierr = nl->getBC().applyMatBC(nl->m_octDA, jac); CHKERRQ(ierr);
    }

    nl->m_Mat->setPlaceholder(PLACEHOLDER_GUESS, NULL);
  }

  return 0;
}

PetscErrorCode NonlinearSolver::FormFunction(SNES snes, Vec in, Vec out, void *ctx) {

  int ierr;
  NonlinearSolver *nl = (NonlinearSolver *) ctx;

  nl->m_guess = in;

  nl->m_Vec->setPlaceholder(PLACEHOLDER_GUESS, in);

  VecZeroEntries(out);

  // PrintStatus("Start volume vector assemble");
  PetscLogEventBegin(assembleVolVecNSEvent, 0, 0, 0, 0);

    nl->m_Vec->addVec(out);
    PetscLogEventEnd(assembleVolVecNSEvent, 0, 0, 0, 0);
  // PrintStatus("Finish volume vector assemble");

  // PrintStatus("Start surface vector assemble");
  PetscLogEventBegin(assembleSurfVecNSEvent, 0, 0, 0, 0);
  nl->surfaceIntegralFuc(in, out);
  PetscLogEventEnd(assembleSurfVecNSEvent, 0, 0, 0, 0);
  // PrintStatus("Finish surface vector assemble");

  // boundary conditions
  if (nl->m_da) {
    ierr = nl->getBC().applyResidualBC(nl->m_da, out); CHKERRQ(ierr);
  }
  if (nl->m_octDA) {
    ierr = nl->getBC().applyResidualBC(nl->m_octDA, out); CHKERRQ(ierr);
  }

  PetscLogEventBegin(solverVectorAssemblyEvent, 0, 0, 0, 0);
  ierr = VecAssemblyBegin(out); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(out); CHKERRQ(ierr);
  PetscLogEventEnd(solverVectorAssemblyEvent, 0, 0, 0, 0);

  nl->m_Vec->setPlaceholder(PLACEHOLDER_GUESS, NULL);

  return 0;
}

NonlinearSolver::~NonlinearSolver() {
  cleanup();
}

PetscErrorCode NonlinearSolver::cleanup() {
  int ierr;
  ierr = SNESDestroy(&m_snes); CHKERRQ(ierr);
  ierr = MatDestroy(&m_matJacobian); CHKERRQ(ierr);
  ierr = VecDestroy(&m_vecSolution); CHKERRQ(ierr);
  ierr = VecDestroy(&m_vecRHS); CHKERRQ(ierr);
  m_guess = NULL;  // managed by petsc
  return 0;
}
