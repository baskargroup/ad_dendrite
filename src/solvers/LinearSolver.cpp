#include <dendrite/solvers/LinearSolver.h>
#include <dendrite/PetscLogging.h>

PetscErrorCode LinearSolver::init() {
  PetscLogEventBegin(solverInitEvent, 0, 0, 0, 0);
  assert(m_octDA || m_da);
  assert(!(m_octDA && m_da));
  assert(m_vecSolution == NULL);
  assert(m_vecRHS == NULL);
  assert(getDof() != 0);

  int ierr;  // for petsc error codes

  // Allocate memory for working vectors
  ierr = m_octDA->createVector(m_vecSolution, false, false, getDof()); CHKERRQ(ierr);
  ierr = m_octDA->createVector(m_vecRHS, false, false, getDof()); CHKERRQ(ierr);

  // create the matrix/shell matrix
  if (!m_matrixFree) {
    if (m_da) {  // petsc
      ierr = DMCreateMatrix(m_da, &m_matJacobian); CHKERRQ(ierr);
    } else if (m_octDA) {  // octree
      ierr = m_octDA->createMatrix(m_matJacobian, MATAIJ, getDof()); CHKERRQ(ierr);
    } else {
      assert(false);
    }
  } else {
    // matrix-free (same for both petsc/octree)
    int matsize;
    ierr = VecGetLocalSize(m_vecRHS, &matsize); CHKERRQ(ierr);

    ierr = MatCreateShell(PETSC_COMM_WORLD, matsize, matsize, PETSC_DETERMINE, PETSC_DETERMINE, this, &m_matJacobian);
    CHKERRQ(ierr);
    ierr = MatShellSetOperation(m_matJacobian, MATOP_MULT, (void (*)()) (ShellMatMult));
    CHKERRQ(ierr);
  }

  // avoids new nonzero errors when boundary conditions are used
  ierr = MatSetOption(m_matJacobian, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRQ(ierr);
  ierr = MatSetOption(m_matJacobian, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
  // create a KSP context
  ierr = KSPCreate(PETSC_COMM_WORLD, &m_ksp); CHKERRQ(ierr);
  ierr = KSPSetOperators(m_ksp, m_matJacobian, m_matJacobian); CHKERRQ(ierr);
  ierr = KSPSetFromOptions(m_ksp); CHKERRQ(ierr);

  // don't automatically zero the initial guess
  // ierr = KSPSetInitialGuessNonzero(m_ksp, PETSC_TRUE); CHKERRQ(ierr);

  PetscLogEventEnd(solverInitEvent, 0, 0, 0, 0);
  return 0;
}

PetscErrorCode LinearSolver::solve() {
  int ierr;  // for petsc error codes
  PetscInt my_rank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &my_rank);

    int size;
    VecGetSize(m_vecRHS, &size);
  // rebuild dirichlet BC row/value list (assume dynamic)
  if (m_da)
    updateBoundaries(m_da);
  if (m_octDA)
    updateBoundaries(m_octDA);

  // calculate rhs
  ierr = VecZeroEntries(m_vecRHS); CHKERRQ(ierr);
  m_Vec->addVec(m_vecRHS);

  // apply boundary conditions to m_vecRHS
  if (m_da) {
    ierr = getBC().applyVecBC(m_da, m_vecRHS); CHKERRQ(ierr);
  }
  if (m_octDA) {
    ierr = getBC().applyVecBC(m_octDA, m_vecRHS); CHKERRQ(ierr);
  }

  ierr = VecAssemblyBegin(m_vecRHS); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(m_vecRHS); CHKERRQ(ierr);

  // assemble matrix if this isn't matrix-free
  if (!m_matrixFree) {
    ierr = MatZeroEntries(m_matJacobian); CHKERRQ(ierr);
    m_Mat->GetAssembledMatrix(&m_matJacobian, 0);

    ierr = MatAssemblyBegin(m_matJacobian, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(m_matJacobian, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    // apply BC to matrix
    if (m_da) {
      ierr = getBC().applyMatBC(m_da, m_matJacobian); CHKERRQ(ierr);
    }
    if (m_octDA) {
      ierr = getBC().applyMatBC(m_octDA, m_matJacobian); CHKERRQ(ierr);
    }
  }
    KSPSetTolerances(m_ksp,1E-12,1E-12,PETSC_DEFAULT,PETSC_DEFAULT);

  // for matrix-free, the matrix is implicitly "assembled" during jacobianMatMult
  ierr = KSPSolve(m_ksp, m_vecRHS, m_vecSolution); CHKERRQ(ierr);
  int lins;
  KSPGetIterationNumber(m_ksp,&lins);

  if (my_rank == 0) {
    std::cout << "Size = " << size << "\n";
    std::cout << "Number of lins = " << lins << "\n";
  }


  return 0;
}

PetscErrorCode LinearSolver::jacobianMatMult(Vec In, Vec Out) {
  assert(m_matrixFree);
  int ierr;
  ierr = VecZeroEntries(Out); CHKERRQ(ierr);
  m_Mat->MatVec(In, Out);
  ierr = getBC().applyMatrixFreeBC(m_octDA, In, Out); CHKERRQ(ierr);
  return 0;
}

LinearSolver::~LinearSolver() {
  cleanup();
}

PetscErrorCode LinearSolver::cleanup() {
  int ierr;
  ierr = KSPDestroy(&m_ksp); CHKERRQ(ierr);
  ierr = MatDestroy(&m_matJacobian); CHKERRQ(ierr);
  ierr = VecDestroy(&m_vecSolution); CHKERRQ(ierr);
  ierr = VecDestroy(&m_vecRHS); CHKERRQ(ierr);
  return 0;
}
