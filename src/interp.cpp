#include <dendrite/interp.h>

#include "parUtils.h"
#include "octUtils.h"
#include "dendro.h"


#define NODE_0 1u
#define NODE_1 2u
#define NODE_2 4u
#define NODE_3 8u
#define NODE_4 16u
#define NODE_5 32u
#define NODE_6 64u
#define NODE_7 128u


void interp_global_to_local(PetscScalar *glo, PetscScalar * /*__restrict*/ loc, ot::DA* da, int m_uiDof) {
  unsigned int idx[8];
  unsigned char hangingMask = da->getHangingNodeIndex(da->curr());
  unsigned int chNum = da->getChildNumber();
  da->getNodeIndices(idx);

  //std::cout << "chNum: " << chNum << std::endl;

  switch (chNum) {

    case 0:
      // 0,7 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        loc[i] = glo[m_uiDof * idx[0] + i];

        if (hangingMask & NODE_1)
          loc[m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i]);
        else
          loc[m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        if (hangingMask & NODE_2)
          loc[2 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[2] + i]);
        else
          loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        if (hangingMask & NODE_3)
          loc[3 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[2] + i] +
                                  glo[m_uiDof * idx[3] + i]);
        else
          loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        if (hangingMask & NODE_4)
          loc[4 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[4] + i]);
        else
          loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        if (hangingMask & NODE_5)
          loc[5 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[4] + i] +
                                  glo[m_uiDof * idx[5] + i]);
        else
          loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        if (hangingMask & NODE_6)
          loc[6 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[4] + i] +
                                  glo[m_uiDof * idx[6] + i]);
        else
          loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];
      }
      break;
    case 1:
      // 1,6 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {

        if (hangingMask & NODE_0)
          loc[i] = 0.5 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i]);
        else
          loc[i] = glo[m_uiDof * idx[0] + i];

        loc[m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        if (hangingMask & NODE_2)
          loc[2 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[2] + i] +
                                  glo[m_uiDof * idx[3] + i]);
        else
          loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        if (hangingMask & NODE_3)
          loc[3 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[3] + i]);
        else
          loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        if (hangingMask & NODE_4)
          loc[4 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[4] + i] +
                                  glo[m_uiDof * idx[5] + i]);
        else
          loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        if (hangingMask & NODE_5)
          loc[5 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[5] + i]);
        else
          loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        if (hangingMask & NODE_7)
          loc[7 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[5] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];
      }
      break;
    case 2:
      // 2,5 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {

        if (hangingMask & NODE_0)
          loc[i] = 0.5 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[2] + i]);
        else
          loc[i] = glo[m_uiDof * idx[0] + i];

        if (hangingMask & NODE_1)
          loc[1 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[2] + i] +
                                  glo[m_uiDof * idx[3] + i]);
        else
          loc[1 * m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        if (hangingMask & NODE_3)
          loc[3 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[3] + i]);
        else
          loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        if (hangingMask & NODE_4)
          loc[4 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[4] + i] +
                                  glo[m_uiDof * idx[6] + i]);
        else
          loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        if (hangingMask & NODE_6)
          loc[6 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[6] + i]);
        else
          loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        if (hangingMask & NODE_7)
          loc[7 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];
      }
      break;
    case 3:
      // 3,4 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0)
          loc[i] = 0.25 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[2] + i] +
                           glo[m_uiDof * idx[3] + i]);
        else
          loc[i] = glo[m_uiDof * idx[0] + i];

        if (hangingMask & NODE_1)
          loc[m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[3] + i]);
        else
          loc[m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        if (hangingMask & NODE_2)
          loc[2 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[3] + i]);
        else
          loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        if (hangingMask & NODE_5)
          loc[5 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[5] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        if (hangingMask & NODE_6)
          loc[6 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        if (hangingMask & NODE_7)
          loc[7 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[7] + i]);
        else
          loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];
      }
      break;
    case 4:
      // 4,3 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0)
          loc[i] = 0.5 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[4] + i]);
        else
          loc[i] = glo[m_uiDof * idx[0] + i];

        if (hangingMask & NODE_1)
          loc[1 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[4] + i] +
                                  glo[m_uiDof * idx[5] + i]);
        else
          loc[1 * m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        if (hangingMask & NODE_2)
          loc[2 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[4] + i] +
                                  glo[m_uiDof * idx[6] + i]);
        else
          loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        if (hangingMask & NODE_5)
          loc[5 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[5] + i]);
        else
          loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        if (hangingMask & NODE_6)
          loc[6 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[6] + i]);
        else
          loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        if (hangingMask & NODE_7)
          loc[7 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[5] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];
      }
      break;
    case 5:
      // 5,2 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0)
          loc[i] = 0.25 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[4] + i] +
                           glo[m_uiDof * idx[5] + i]);
        else
          loc[i] = glo[m_uiDof * idx[0] + i];

        if (hangingMask & NODE_1)
          loc[m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[5] + i]);
        else
          loc[m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        if (hangingMask & NODE_3)
          loc[3 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[5] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        if (hangingMask & NODE_4)
          loc[4 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[5] + i]);
        else
          loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        if (hangingMask & NODE_6)
          loc[6 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[5] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        if (hangingMask & NODE_7)
          loc[7 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[5] + i] + glo[m_uiDof * idx[7] + i]);
        else
          loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];
      }
      break;
    case 6:
      // 6,1 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0)
          loc[i] = 0.25 * (glo[m_uiDof * idx[0] + i] + glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[4] + i] +
                           glo[m_uiDof * idx[5] + i]);
        else
          loc[i] = glo[m_uiDof * idx[0] + i];

        loc[m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        if (hangingMask & NODE_2)
          loc[2 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[6] + i]);
        else
          loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        if (hangingMask & NODE_3)
          loc[3 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        if (hangingMask & NODE_4)
          loc[4 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[6] + i]);
        else
          loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        if (hangingMask & NODE_5)
          loc[5 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[5] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        if (hangingMask & NODE_7)
          loc[7 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[6] + i] + glo[m_uiDof * idx[7] + i]);
        else
          loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];

      }
      break;
    case 7:
      // 7,0 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        loc[i] = glo[m_uiDof * idx[0] + i];

        if (hangingMask & NODE_1)
          loc[1 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[1] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[5] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[1 * m_uiDof + i] = glo[m_uiDof * idx[1] + i];

        if (hangingMask & NODE_2)
          loc[2 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[2] + i] + glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[2 * m_uiDof + i] = glo[m_uiDof * idx[2] + i];

        if (hangingMask & NODE_3)
          loc[3 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[3] + i] + glo[m_uiDof * idx[7] + i]);
        else
          loc[3 * m_uiDof + i] = glo[m_uiDof * idx[3] + i];

        if (hangingMask & NODE_4)
          loc[4 * m_uiDof + i] = 0.25 *
                                 (glo[m_uiDof * idx[4] + i] + glo[m_uiDof * idx[5] + i] + glo[m_uiDof * idx[6] + i] +
                                  glo[m_uiDof * idx[7] + i]);
        else
          loc[4 * m_uiDof + i] = glo[m_uiDof * idx[4] + i];

        if (hangingMask & NODE_5)
          loc[5 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[5] + i] + glo[m_uiDof * idx[7] + i]);
        else
          loc[5 * m_uiDof + i] = glo[m_uiDof * idx[5] + i];

        if (hangingMask & NODE_6)
          loc[6 * m_uiDof + i] = 0.5 * (glo[m_uiDof * idx[6] + i] + glo[m_uiDof * idx[7] + i]);
        else
          loc[6 * m_uiDof + i] = glo[m_uiDof * idx[6] + i];

        loc[7 * m_uiDof + i] = glo[m_uiDof * idx[7] + i];
      }
      break;
    default:
      std::cout << "in glo_to_loc: incorrect child num = " << chNum << std::endl;
      assert(false);
      break;
  } // switch


}

void interp_local_to_global(PetscScalar * /*__restrict*/ loc, PetscScalar *glo, ot::DA* da, int m_uiDof) {
  unsigned int idx[8];
  unsigned char hangingMask = da->getHangingNodeIndex(da->curr());
  unsigned int chNum = da->getChildNumber();
  da->getNodeIndices(idx);

  switch (chNum) {
    case 0:
      // 0,7 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        glo[m_uiDof * idx[0] + i] += loc[i];
        if (hangingMask & NODE_1) {
          glo[m_uiDof * idx[1] + i] += 0.5 * loc[m_uiDof + i];
          glo[m_uiDof * idx[0] + i] += 0.5 * loc[m_uiDof + i];
        } else {
          glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];
        }
        if (hangingMask & NODE_2) {
          glo[m_uiDof * idx[2] + i] += 0.5 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[0] + i] += 0.5 * loc[2 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];
        }
        if (hangingMask & NODE_3) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[3 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        }
        if (hangingMask & NODE_4) {
          glo[m_uiDof * idx[4] + i] += 0.5 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[0] + i] += 0.5 * loc[4 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];
        }
        if (hangingMask & NODE_5) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[5 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[6 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];
        }
        glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
      }
      break;
    case 1:
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          glo[m_uiDof * idx[0] + i] += 0.5 * loc[i];
          glo[m_uiDof * idx[1] + i] += 0.5 * loc[i];
        } else {
          glo[m_uiDof * idx[0] + i] += loc[i];
        }

        glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];

        if (hangingMask & NODE_2) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[2 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];
        }

        if (hangingMask & NODE_3) {
          glo[m_uiDof * idx[1] + i] += 0.5 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.5 * loc[3 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        }

        if (hangingMask & NODE_4) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[4 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];
        }

        if (hangingMask & NODE_5) {
          glo[m_uiDof * idx[1] + i] += 0.5 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.5 * loc[5 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];
        }

        glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];

        if (hangingMask & NODE_7) {
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[7 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 2:
      // 2,5 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          glo[m_uiDof * idx[0] + i] += 0.5 * loc[i];
          glo[m_uiDof * idx[2] + i] += 0.5 * loc[i];
        } else {
          glo[m_uiDof * idx[0] + i] += loc[i];
        }

        if (hangingMask & NODE_1) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[m_uiDof + i];
        } else {
          glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];
        }

        glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];

        if (hangingMask & NODE_3) {
          glo[m_uiDof * idx[2] + i] += 0.5 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.5 * loc[3 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        }

        if (hangingMask & NODE_4) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[4 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];
        }

        glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];

        if (hangingMask & NODE_6) {
          glo[m_uiDof * idx[2] + i] += 0.5 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.5 * loc[6 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];
        }

        if (hangingMask & NODE_7) {
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[7 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 3:
      // 3,4 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[i];
        } else {
          glo[m_uiDof * idx[0] + i] += loc[i];
        }

        if (hangingMask & NODE_1) {
          glo[m_uiDof * idx[1] + i] += 0.5 * loc[m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.5 * loc[m_uiDof + i];
        } else {
          glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];
        }

        if (hangingMask & NODE_2) {
          glo[m_uiDof * idx[2] + i] += 0.5 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.5 * loc[2 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];
        }

        glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];

        if (hangingMask & NODE_5) {
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[5 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[6 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];
        }
        if (hangingMask & NODE_7) {
          glo[m_uiDof * idx[3] + i] += 0.5 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.5 * loc[7 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 4:
      // 4,3 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          glo[m_uiDof * idx[0] + i] += 0.5 * loc[i];
          glo[m_uiDof * idx[4] + i] += 0.5 * loc[i];
        } else {
          glo[m_uiDof * idx[0] + i] += loc[i];
        }
        if (hangingMask & NODE_1) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[m_uiDof + i];
        } else {
          glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];
        }

        if (hangingMask & NODE_2) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[2 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];
        }

        glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];

        if (hangingMask & NODE_5) {
          glo[m_uiDof * idx[4] + i] += 0.5 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.5 * loc[5 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          glo[m_uiDof * idx[4] + i] += 0.5 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.5 * loc[6 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];
        }
        if (hangingMask & NODE_7) {
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[7 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 5:
      // 5,2 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[i];
        } else {
          glo[m_uiDof * idx[0] + i] += loc[i];
        }
        if (hangingMask & NODE_1) {
          glo[m_uiDof * idx[1] + i] += 0.5 * loc[m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.5 * loc[m_uiDof + i];
        } else {
          glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];
        }
        glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];
        if (hangingMask & NODE_3) {
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[3 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        }
        if (hangingMask & NODE_4) {
          glo[m_uiDof * idx[4] + i] += 0.5 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.5 * loc[4 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];
        }
        glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];
        if (hangingMask & NODE_6) {
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[6 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];
        }
        if (hangingMask & NODE_7) {
          glo[m_uiDof * idx[5] + i] += 0.5 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.5 * loc[7 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 6:
      // 6,1 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          glo[m_uiDof * idx[0] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[i];
        } else {
          glo[m_uiDof * idx[0] + i] += loc[i];
        }
        glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];
        if (hangingMask & NODE_2) {
          glo[m_uiDof * idx[2] + i] += 0.5 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.5 * loc[2 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];
        }
        if (hangingMask & NODE_3) {
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[3 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        }
        if (hangingMask & NODE_4) {
          glo[m_uiDof * idx[4] + i] += 0.5 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.5 * loc[4 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];
        }
        if (hangingMask & NODE_5) {
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[5 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];
        }
        glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];
        if (hangingMask & NODE_7) {
          glo[m_uiDof * idx[6] + i] += 0.5 * loc[7 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.5 * loc[7 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 7:
      // 7,0 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        glo[m_uiDof * idx[0] + i] += loc[i];
        if (hangingMask & NODE_1) {
          glo[m_uiDof * idx[1] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[m_uiDof + i];
        } else {
          glo[m_uiDof * idx[1] + i] += loc[m_uiDof + i];
        }
        if (hangingMask & NODE_2) {
          glo[m_uiDof * idx[2] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[3] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[2 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[2 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[2] + i] += loc[2 * m_uiDof + i];
        }
        if (hangingMask & NODE_3) {
          glo[m_uiDof * idx[3] + i] += 0.5 * loc[3 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.5 * loc[3 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[3] + i] += loc[3 * m_uiDof + i];
        }

        if (hangingMask & NODE_4) {
          glo[m_uiDof * idx[4] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[5] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[6] + i] += 0.25 * loc[4 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.25 * loc[4 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[4] + i] += loc[4 * m_uiDof + i];
        }
        if (hangingMask & NODE_5) {
          glo[m_uiDof * idx[5] + i] += 0.5 * loc[5 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.5 * loc[5 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[5] + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          glo[m_uiDof * idx[6] + i] += 0.5 * loc[6 * m_uiDof + i];
          glo[m_uiDof * idx[7] + i] += 0.5 * loc[6 * m_uiDof + i];
        } else {
          glo[m_uiDof * idx[6] + i] += loc[6 * m_uiDof + i];
        }
        glo[m_uiDof * idx[7] + i] += loc[7 * m_uiDof + i];
      }
      break;
    default:
      std::cout << "in loc_to_glo: incorrect child num = " << chNum << std::endl;
      assert(false);
      break;
  } // switch chNum
} // loc_to_glo



void
interp_local_to_global_still_local(PetscScalar * /*__restrict*/ loc, PetscScalar *loc_new, ot::DA* da, int m_uiDof) {
  //unsigned int idx[8];
  unsigned char hangingMask = da->getHangingNodeIndex(da->curr());
  unsigned int chNum = da->getChildNumber();
  //unsigned int idx[8];
  //da->getNodeIndices(idx);

  switch (chNum) {
    case 0:
      // 0,7 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        loc_new[m_uiDof * 0 + i] += loc[i];
        if (hangingMask & NODE_1) {
          loc_new[m_uiDof * 1 + i] += 0.5 * loc[m_uiDof + i];
          loc_new[m_uiDof * 0 + i] += 0.5 * loc[m_uiDof + i];
        } else {
          loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];
        }
        if (hangingMask & NODE_2) {
          loc_new[m_uiDof * 2 + i] += 0.5 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 0 + i] += 0.5 * loc[2 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];
        }
        if (hangingMask & NODE_3) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[3 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        }
        if (hangingMask & NODE_4) {
          loc_new[m_uiDof * 4 + i] += 0.5 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 0 + i] += 0.5 * loc[4 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];
        }
        if (hangingMask & NODE_5) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[5 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[6 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];
        }
        loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
      }
      break;
    case 1:
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          loc_new[m_uiDof * 0 + i] += 0.5 * loc[i];
          loc_new[m_uiDof * 1 + i] += 0.5 * loc[i];
        } else {
          loc_new[m_uiDof * 0 + i] += loc[i];
        }

        loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];

        if (hangingMask & NODE_2) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[2 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];
        }

        if (hangingMask & NODE_3) {
          loc_new[m_uiDof * 1 + i] += 0.5 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.5 * loc[3 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        }

        if (hangingMask & NODE_4) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[4 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];
        }

        if (hangingMask & NODE_5) {
          loc_new[m_uiDof * 1 + i] += 0.5 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.5 * loc[5 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];
        }

        loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];

        if (hangingMask & NODE_7) {
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[7 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 2:
      // 2,5 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          loc_new[m_uiDof * 0 + i] += 0.5 * loc[i];
          loc_new[m_uiDof * 2 + i] += 0.5 * loc[i];
        } else {
          loc_new[m_uiDof * 0 + i] += loc[i];
        }

        if (hangingMask & NODE_1) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[m_uiDof + i];
        } else {
          loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];
        }

        loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];

        if (hangingMask & NODE_3) {
          loc_new[m_uiDof * 2 + i] += 0.5 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.5 * loc[3 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        }

        if (hangingMask & NODE_4) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[4 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];
        }

        loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];

        if (hangingMask & NODE_6) {
          loc_new[m_uiDof * 2 + i] += 0.5 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.5 * loc[6 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];
        }

        if (hangingMask & NODE_7) {
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[7 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 3:
      // 3,4 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[i];
        } else {
          loc_new[m_uiDof * 0 + i] += loc[i];
        }

        if (hangingMask & NODE_1) {
          loc_new[m_uiDof * 1 + i] += 0.5 * loc[m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.5 * loc[m_uiDof + i];
        } else {
          loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];
        }

        if (hangingMask & NODE_2) {
          loc_new[m_uiDof * 2 + i] += 0.5 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.5 * loc[2 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];
        }

        loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];

        if (hangingMask & NODE_5) {
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[5 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[6 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];
        }
        if (hangingMask & NODE_7) {
          loc_new[m_uiDof * 3 + i] += 0.5 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.5 * loc[7 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 4:
      // 4,3 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          loc_new[m_uiDof * 0 + i] += 0.5 * loc[i];
          loc_new[m_uiDof * 4 + i] += 0.5 * loc[i];
        } else {
          loc_new[m_uiDof * 0 + i] += loc[i];
        }
        if (hangingMask & NODE_1) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[m_uiDof + i];
        } else {
          loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];
        }

        if (hangingMask & NODE_2) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[2 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];
        }

        loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];

        if (hangingMask & NODE_5) {
          loc_new[m_uiDof * 4 + i] += 0.5 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.5 * loc[5 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          loc_new[m_uiDof * 4 + i] += 0.5 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.5 * loc[6 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];
        }
        if (hangingMask & NODE_7) {
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[7 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 5:
      // 5,2 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[i];
        } else {
          loc_new[m_uiDof * 0 + i] += loc[i];
        }
        if (hangingMask & NODE_1) {
          loc_new[m_uiDof * 1 + i] += 0.5 * loc[m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.5 * loc[m_uiDof + i];
        } else {
          loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];
        }
        loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];
        if (hangingMask & NODE_3) {
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[3 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        }
        if (hangingMask & NODE_4) {
          loc_new[m_uiDof * 4 + i] += 0.5 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.5 * loc[4 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];
        }
        loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];
        if (hangingMask & NODE_6) {
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[6 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];
        }
        if (hangingMask & NODE_7) {
          loc_new[m_uiDof * 5 + i] += 0.5 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.5 * loc[7 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 6:
      // 6,1 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        if (hangingMask & NODE_0) {
          loc_new[m_uiDof * 0 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[i];
        } else {
          loc_new[m_uiDof * 0 + i] += loc[i];
        }
        loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];
        if (hangingMask & NODE_2) {
          loc_new[m_uiDof * 2 + i] += 0.5 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.5 * loc[2 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];
        }
        if (hangingMask & NODE_3) {
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[3 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        }
        if (hangingMask & NODE_4) {
          loc_new[m_uiDof * 4 + i] += 0.5 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.5 * loc[4 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];
        }
        if (hangingMask & NODE_5) {
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[5 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];
        }
        loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];
        if (hangingMask & NODE_7) {
          loc_new[m_uiDof * 6 + i] += 0.5 * loc[7 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.5 * loc[7 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
        }
      }
      break;
    case 7:
      // 7,0 are not hanging
      for (size_t i = 0; i < m_uiDof; i++) {
        loc_new[m_uiDof * 0 + i] += loc[i];
        if (hangingMask & NODE_1) {
          loc_new[m_uiDof * 1 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[m_uiDof + i];
        } else {
          loc_new[m_uiDof * 1 + i] += loc[m_uiDof + i];
        }
        if (hangingMask & NODE_2) {
          loc_new[m_uiDof * 2 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 3 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[2 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[2 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 2 + i] += loc[2 * m_uiDof + i];
        }
        if (hangingMask & NODE_3) {
          loc_new[m_uiDof * 3 + i] += 0.5 * loc[3 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.5 * loc[3 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 3 + i] += loc[3 * m_uiDof + i];
        }

        if (hangingMask & NODE_4) {
          loc_new[m_uiDof * 4 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 5 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 6 + i] += 0.25 * loc[4 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.25 * loc[4 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 4 + i] += loc[4 * m_uiDof + i];
        }
        if (hangingMask & NODE_5) {
          loc_new[m_uiDof * 5 + i] += 0.5 * loc[5 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.5 * loc[5 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 5 + i] += loc[5 * m_uiDof + i];
        }
        if (hangingMask & NODE_6) {
          loc_new[m_uiDof * 6 + i] += 0.5 * loc[6 * m_uiDof + i];
          loc_new[m_uiDof * 7 + i] += 0.5 * loc[6 * m_uiDof + i];
        } else {
          loc_new[m_uiDof * 6 + i] += loc[6 * m_uiDof + i];
        }
        loc_new[m_uiDof * 7 + i] += loc[7 * m_uiDof + i];
      }
      break;
    default:
      std::cout << "in loc_to_glo: incorrect child num = " << chNum << std::endl;
      assert(false);
      break;
  } // switch chNum
} // loc_to_glo

void interp_local_to_global_matrix(PetscScalar *Ke, std::vector<ot::MatRecord> &out, ot::DA *da, int m_uiDof) {
  unsigned int m = m_uiDof * 8;  // matrix size in one dimension

  unsigned int idx[8];
  da->getNodeIndices(idx);

  // Ke^T
  PetscScalar *Ke_trans = new PetscScalar[m * m];
  for (unsigned int i = 0; i < m; i++) {
    for (unsigned int j = 0; j < m; j++) {
      Ke_trans[i * m + j] = Ke[i + j * m];
    }
  }

  // get continuous columns of Ke^T (row major -> column major)
  PetscScalar *Ke_trans_cols = new PetscScalar[m * m];
  for (unsigned int i = 0; i < m; i++) {
    for (unsigned int j = 0; j < m; j++) {
      Ke_trans_cols[i * m + j] = Ke_trans[i + j * m];
    }
  }

  PetscScalar *Ke_temp = new PetscScalar[m * m];
  PetscScalar *interp_rhs = new PetscScalar[m];
  for (unsigned int i = 0; i < m; i++) {  // row
    // TODO change interp function to be = and not += so we dont have to zero out first
    for (unsigned int j = 0; j < m; j++) {
      interp_rhs[j] = 0.0;
    }

    // Qe^T*Ke^T (Ke_temp)
    interp_local_to_global_still_local(Ke_trans_cols + m * i, interp_rhs, da, m_uiDof);

    for (unsigned int j = 0; j < m; j++) {
      Ke_temp[i + j * m] = interp_rhs[j];
    }
  }

  // [Qe^T*Ke^T]^T
  PetscScalar *Ke_temp_trans = new PetscScalar[m * m];
  for (unsigned int i = 0; i < m; i++) {
    for (unsigned int j = 0; j < m; j++) {
      Ke_temp_trans[i * m + j] = Ke_temp[i + j * m];
    }
  }

  // get continuous columns of [Qe^T*Ke^T]^T (row major -> column major)
  PetscScalar *Ke_temp_trans_cols = new PetscScalar[m * m];
  for (unsigned int i = 0; i < m; i++) {
    for (unsigned int j = 0; j < m; j++) {
      Ke_temp_trans_cols[i * m + j] = Ke_temp_trans[i + j * m];
    }
  }

  PetscScalar *interp = new PetscScalar[m];
  for (unsigned int i = 0; i < m; i++) {  // column
    // TODO change interp function to be = and not += so we dont have to zero out first
    for (unsigned int j = 0; j < m; j++) {
      interp[j] = 0.0;
    }

    // map each column in Ke*Qe (Ke_temp) to get corresponding colume in Qe^T*Ke*Qe
    // interp_local_to_global_still_local is the operator of Qe^T
    interp_local_to_global_still_local(Ke_temp_trans_cols + m * i, interp, da, m_uiDof);

    // convert interp to m ot::MatRecords
    for (unsigned int j = 0; j < m; j++) {  // row
      ot::MatRecord mr;
      mr.rowIdx = idx[j / m_uiDof];
      mr.colIdx = idx[i / m_uiDof];
      mr.rowDim = j % m_uiDof;
      mr.colDim = i % m_uiDof;
      mr.val = interp[j];
      out.push_back(mr);
    }
  }

  delete[] Ke_trans;
  delete[] Ke_trans_cols;
  delete[] Ke_temp;
  delete[] Ke_temp_trans;
  delete[] Ke_temp_trans_cols;
  delete[] interp;
  delete[] interp_rhs;
}

void dendro_to_taly(void *dest, void *src, size_t bytes) {
  // maps morton (Dendro) indices to TalyFEM order
  // 2----3        3----2
  // |    |   ->   |    |
  // 0----1        0----1
  static const int morton_to_taly_map[8] = {
      0, 1, 3, 2, 4, 5, 7, 6
  };

  for (int i = 0; i < 8; i++) {
    memcpy(static_cast<char *>(dest) + bytes * morton_to_taly_map[i],
           static_cast<char *>(src) + bytes * i,
           bytes);
  }
}

// taly -> morton ordering
void taly_to_dendro(void *dest, void *src, size_t bytes) {
  static const int morton_to_taly_map[8] = {
      0, 1, 3, 2, 4, 5, 7, 6
  };

  for (int i = 0; i < 8; i++) {
    memcpy(static_cast<char *>(dest) + bytes * i,
           static_cast<char *>(src) + bytes * morton_to_taly_map[i],
           bytes);
  }
}

// builds directly as TalyFEM coordinates
// pt - "bottom left" of element (already corrected w/ xFac/yFac/zFac)
// h - width in each dimension (already corrected w/ xFac/yFac/zFac)
void build_taly_coordinates(double *coords /*out*/, const Point &pt, const Point &h) {
  const double &hx = h.x();
  const double &hy = h.y();
  const double &hz = h.z();

  Globals globals;
  if( globals.ndof() == 8) {
    coords[0] = pt.x();            coords[1]  = pt.y();          coords[2] = pt.z();
    coords[3] = coords[0] + hx;    coords[4]  = coords[1];       coords[5] = coords[2];
    coords[6] = coords[0] + hx;    coords[7]  = coords[1] + hy;  coords[8] = coords[2];
    coords[9] = coords[0];         coords[10] = coords[1]+ hy;   coords[11] = coords[2];
    coords[12] = coords[0];        coords[13] = coords[1];       coords[14] = coords[2] + hz;
    coords[15] = coords[0] + hx;   coords[16] = coords[1];       coords[17] = coords[2] + hz;
    coords[18] = coords[0] + hx;   coords[19] = coords[1] + hy;  coords[20] = coords[2] + hz;
    coords[21] = coords[0];        coords[22] = coords[1] + hy;  coords[23] = coords[2] + hz;
  }
  else if(globals.ndof() == 27){

    coords[24] = coords[0];      coords[25] = coords[1];       coords[26] = coords[2] + 0.5*hz;
    coords[27] = coords[3];      coords[28] = coords[4];       coords[29] = coords[5] + 0.5*hz;
    coords[30] = coords[6];      coords[31] = coords[7];       coords[32] = coords[8] + 0.5*hz;
    coords[33] = coords[9];      coords[34] = coords[10];      coords[35] = coords[11]+ 0.5*hz;

    coords[36] = coords[0] + 0.5*hx; coords[37] = coords[1];             coords[38] = coords[2];
    coords[39] = coords[3];          coords[40] = coords[4] + 0.5*hy;    coords[41] = coords[5];
    coords[42] = coords[6] - 0.5*hx; coords[43] = coords[7];             coords[44] = coords[8];
    coords[45] = coords[9];          coords[46] = coords[10] -0.5*hy;    coords[47] = coords[11];
    coords[48] = coords[0] + 0.5*hx; coords[49] = coords[1] + 0.5*hy;    coords[50] = coords[2];

    coords[51] = coords[12] + 0.5*hx; coords[52] = coords[13];           coords[53] = coords[14];
    coords[54] = coords[15];          coords[55] = coords[16] + 0.5*hy;  coords[56] = coords[17];
    coords[57] = coords[18] - 0.5*hx; coords[58] = coords[19];           coords[59] = coords[20];
    coords[60] = coords[21];          coords[61] = coords[22] -0.5*hy;   coords[62] = coords[23];
    coords[63] = coords[12] + 0.5*hx; coords[64] = coords[13] + 0.5*hy;  coords[65] = coords[14];

    coords[66] = coords[24] + 0.5*hx; coords[67] = coords[25];           coords[68] = coords[26];
    coords[69] = coords[27];          coords[70] = coords[28] + 0.5*hy;  coords[71] = coords[29];
    coords[72] = coords[30] - 0.5*hx; coords[73] = coords[31];           coords[74] = coords[32];
    coords[75] = coords[33];          coords[76] = coords[34] -0.5*hy;   coords[77] = coords[35];
    coords[78] = coords[24] + 0.5*hx; coords[79] = coords[25] + 0.5*hy;  coords[80] = coords[26];


  }
  else{
      assert(false);
  }
}



//template <typename T>
void dendro_to_taly_surface(double *dest, double *src, size_t bytes, int surf_id) {

  switch (surf_id)
  {
    case 3: {
        static const int morton_to_taly_map[4] = {0, 1, 3, 2};
        copy(dest, src, bytes, morton_to_taly_map);
        break;
    }
    case 2: {
        static const int morton_to_taly_map[4] = {2, 3, 7, 6};
        copy(dest, src, bytes, morton_to_taly_map);
        break;
    }
    case 1: {
        static const int morton_to_taly_map[4] = {1, 5, 7, 3};
        copy(dest, src, bytes, morton_to_taly_map);
        break;
    }
    case 0: {
        std::cout << "Something is wrong. The surf_id cannot be 0.\n";
        assert(false);
        break;
    }
    case -1: {
        static const int morton_to_taly_map[4] = {0, 2, 6, 4};
        copy(dest, src, bytes, morton_to_taly_map);
        break;
    }
    case -2: {
          static const int morton_to_taly_map[4] = {4, 5, 1 ,0};
          copy(dest, src, bytes, morton_to_taly_map);
          break;
    }
    case -3: {
         static const int morton_to_taly_map[4] = {6, 7, 5, 4};
         copy(dest, src, bytes, morton_to_taly_map);
         break;
    }
      default:{
          std::cout << "Something is wrong. The surf_id cannot be greater than 3 or less than 3.\n";
          assert(false);
      }



  }


}

void copy(double *dest, double *src, size_t bytes, const int * morton_to_taly_map) {
    for(int i = 0; i < 4; i++){
        dest[i] = src[morton_to_taly_map[i]];
    }


}


