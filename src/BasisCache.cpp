//
// Created by lofquist on 3/15/18.
//

#include <dendrite/BasisCache.h>
#include <dendrite/interp.h>

BasisCache::BasisCache() : cache_ {} {
    Globals globals;
    relOrder = globals.getrelOrder();
    bf = globals.getBasisFunction();
    N_GAUSS_POINTS = (bf + relOrder + 1)*(bf + relOrder +1 )*(bf + relOrder +1 );
    cache_.resize(MAX_LEVELS);
    positions_.resize(MAX_LEVELS);
    for (unsigned int i = 0; i < MAX_LEVELS; i++) {
        cache_[i].resize(N_GAUSS_POINTS);
        positions_[i].resize(N_GAUSS_POINTS);
    }
    for (unsigned int i = 0; i < MAX_LEVELS; i++) {
        for (unsigned int j = 0; j < N_GAUSS_POINTS; j++) {
            cache_[i][j] = new TALYFEMLIB::FEMElm(nullptr);
        }
    }
}

BasisCache::~BasisCache() {
    for (unsigned int i = 0; i < MAX_LEVELS; i++) {
        for (unsigned int j = 0; j < N_GAUSS_POINTS; j++) {

            delete cache_[i][j];
        }
    }
}

void BasisCache::init(const PetscScalar* problem_size, unsigned int max_depth, TALYFEMLIB::GRID* grid, unsigned int basis_flags, int rel_order) {
    assert(max_depth < MAX_LEVELS);
    assert (rel_order == 0);

    // calculate oct_to_phys_
    oct_to_phys_[0] = problem_size[0] / ((PetscScalar) (1 << (max_depth - 1)));
    oct_to_phys_[1] = problem_size[1] / ((PetscScalar) (1 << (max_depth - 1)));
    oct_to_phys_[2] = problem_size[2] / ((PetscScalar) (1 << (max_depth - 1)));


    for (unsigned int i = 1; i < max_depth; i++) {
        // fix up node positions
        double coords[8*3];
        Point origin(0.0, 0.0, 0.0);
        
        Point h(oct_to_phys_[0] * (1 << (max_depth - i)),
                oct_to_phys_[1] * (1 << (max_depth - i)),
                oct_to_phys_[2] * (1 << (max_depth - i)));
        build_taly_coordinates(coords, origin, h);


        assert(grid->n_nodes() == 8);
        for (unsigned int j = 0; j < 8; j++) {
            grid->GetNode(j)->location() = TALYFEMLIB::ZEROPTV(coords[j*3 + 0], coords[j*3+1], coords[j*3+2]);

        }

        for (unsigned int j = 0; j < N_GAUSS_POINTS; j++) {
            *cache_[i][j] = TALYFEMLIB::FEMElm(grid, basis_flags);

            cache_[i][j]->refill(0, rel_order);
            for (unsigned int k = 0; k <= j; k++)
                cache_[i][j]->next_itg_pt();
            positions_[i][j] = cache_[i][j]->position();
        }
    }

}

const TALYFEMLIB::FEMElm* BasisCache::get(unsigned char level, unsigned int gp_idx, const TALYFEMLIB::ZEROPTV& offset) {
    assert(level >= 0 && level < MAX_LEVELS);
    assert(gp_idx >= 0 && gp_idx < N_GAUSS_POINTS);

    TALYFEMLIB::FEMElm* fe = cache_[level][gp_idx];
    /*TALYFEMLIB::ZEROPTV p(offset.x() * oct_to_phys_[0],
                          offset.y() * oct_to_phys_[1],
                          offset.z() * oct_to_phys_[2]);*/
    fe->set_position(positions_[level][gp_idx] + offset);
    return fe;
}